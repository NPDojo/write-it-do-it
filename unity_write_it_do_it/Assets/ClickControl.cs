﻿using UnityEngine;
using System.Collections;
using WriteItDoIt;

public class ClickControl : MonoBehaviour {

	private TouchControl touchControl;
	private Vector3 originalPosition;
	private Quaternion originalRotation;
	private float shakeIntensity;
	private float shakeDecay;

	// Use this for initialization
	void Start () {
		touchControl = camera.GetComponent<TouchControl>();
	}
	
	// Update is called once per frame
	void Update () {
		handleMouseInput();
		handleKeyboardInput();
//		touchControl.boundCameraMovement();
	}

	private void handleMouseInput() {
		if (Input.GetMouseButtonDown(0) && Input.touchCount == 0) {
			touchControl.handleObjectSelection(Input.mousePosition);
		}
	}

	private void handleKeyboardInput() {
		if (Input.GetKey(KeyCode.A)) {
			gameObject.transform.Translate(Vector3.left * 5 * Time.deltaTime);
		}
		if (Input.GetKey(KeyCode.D)) {
			gameObject.transform.Translate(Vector3.right * 5 * Time.deltaTime);
		}
		if (Input.GetKey(KeyCode.W)) {
			gameObject.transform.Translate(Vector3.up * 5 * Time.deltaTime);
		}
		if (Input.GetKey(KeyCode.S)) {
			gameObject.transform.Translate(Vector3.down * 5 * Time.deltaTime);
		}
		if (Input.GetKey(KeyCode.Q)) {
			gameObject.transform.Translate(Vector3.back * 5 * Time.deltaTime);
		}
		if (Input.GetKey(KeyCode.E)) {
			gameObject.transform.Translate(Vector3.forward * 5 * Time.deltaTime);
		}
		if (Input.GetKey(KeyCode.Space)) {
			touchControl.ToggleAutoRotate();
		}
	}
}
