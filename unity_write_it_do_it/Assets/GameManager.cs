﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.IO;

// Enables gameplay flow between scenes by holding static information about the game being played
// We should always check that currentGame has the proper information before loading 3d-model scenes
namespace WriteItDoIt
{
	public static class GameManager
	{
		private static Game currentGame = new Game();

		//TODO when would we set this? Perhaps readonly singleton?
		public static Game CurrentGame {
			get {
				return currentGame;
			}
			set{
				currentGame = value;
			}
		}
	}
}

