using System;
using System.IO;

namespace WriteItDoIt
{
	public class ModelStream
	{

		//Protocol:
		//
		//Model:
		//	Pieces count (1)
		//		byte model.Pieces.Count
		//	Each individual piece serialized afterward

		private Stream stream;

		public ModelStream(Stream s)
		{
			this.stream = s;
		}

		public ModelStream(byte[] b)
		{
			this.stream = new MemoryStream(b);
		}

		public void WriteModel(Model m)
		{
			//write count
			stream.WriteByte((byte)m.Pieces.Count);

			PieceStream pstream = new PieceStream(stream);

			foreach (Piece p in m.Pieces) {
				pstream.WritePiece(p);
			}
		}

		//When calling this CHECK FOR EXCEPTIONS in the case of malformed/corrupted/incorrect data
		public Model ReadModel()
		{
			Model model = new Model();

			//number of pieces to follow
			int count = stream.ReadByte();

			if (count == -1) 
			{
				throw new ApplicationException("Stream has nothing to read.");
			}

			PieceStream ps = new PieceStream(stream);

			for (int i = 0; i < count; i++) {
				model.Add(ps.ReadPiece());
			}

			return model;
		}

		public int ReadPiecesCount()
		{
			return stream.ReadByte();
		}

		public void Reset()
		{
			stream.Seek (0, SeekOrigin.Begin);
		}

		public void Close()
		{
			stream.Close();
		}
	}
}

