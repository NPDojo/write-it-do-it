﻿using System;
using UnityEngine;
using System.Collections.Generic;

namespace WriteItDoIt
{
	public enum GameType {None, WIDI, WI, DI, View, Sandbox};

	public class Game
	{
		public delegate int ScoringStrategy(Model model, Model original);

		public string ModelName {
			get;
			set;
		}

		// Determines how to load a particular scene (DI vs. sandbox, timed vs. untimed, which scene next)
		public GameType Type {
			get;
			set;
		}

		public ModelStream LoadedModelStream {
			get;
			set;
		}

		public Model OriginalModel {
			get;
			set;
		}

		public Model CurrentModel {
			get;
			set;
		}

		public int OverlappingPieceCount {
			get;
			set;
		}

		// For WIDI game type, specifies which phase user is in
		public bool inDoItPhase {
			get;
			set;
		}

		// Indicates whether the user should be able to manipulate the model at this time
		public bool allowManipulation {
			get;
			set;
		}	

		// A flag that prevents piece auto-connection while true (be careful to set back to false)
		public bool pauseAutoConnect {
			get;
			set;
		}

		//Used to prevent user from doing anything besides fix intersecting piece(s)
		public bool IsPieceProblem {
			get {
				return this.OverlappingPieceCount > 0;
			}
		}

		private GameObject currentSelected;
		private Color currentSelectedOriginalColor;

		public GameObject CurrentSelected 
		{
			get
			{
				return currentSelected;
			}
			set 
			{
				//unselected color for previously selected object
				if (Utilities.IsConnectionPoint(currentSelected)) {
					currentSelected.renderer.material.color = Constants.ConnectionPointColorDefault;
				} else if (currentSelected != null) {
					currentSelected.renderer.material.color = currentSelectedOriginalColor;
				}

				//recolor newly selected object
				if (Utilities.IsConnectionPoint(value)) {
					value.renderer.material.color = Constants.ConnectionPointColorSelected;
				} else if (value != null) {
					currentSelectedOriginalColor = value.renderer.material.color;
					value.renderer.material.color = Constants.PieceColorSelected;
				}

				this.currentSelected = value;
			}
		}

		public ScoringStrategy DefaultScoringStrategy
		{
			get;
			private set;
		}

		public Game()
		{
			this.DefaultScoringStrategy = ScoringAlgorithmByPiece.GetModelScore;
			this.OriginalModel = null;
		}

		public int score(Model m, ScoringStrategy scoringStrategy = null) 
		{
			// Leaving this extra modular in case of additional algorithms

			//Optional scoringstrategy, otherwise use this game's default
			scoringStrategy = scoringStrategy ?? DefaultScoringStrategy;
			Model toBeScoredAgainst = OriginalModel ?? m;
			if (object.ReferenceEquals(toBeScoredAgainst, m)) {
				Debug.Log("No original model found; the score reported for " +
				          "this model represents the maximum possible score for this model.");
			}
			return scoringStrategy(m, toBeScoredAgainst);
		}		
	}
}

