﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Threading;

public class DecodeQrSceneStartScript : MonoBehaviour
{

    private WebCamTexture cameraTexture;
    private RawImage uiImage;
    private QR qr;

    private bool[] oldAutorotateSettings = new bool[4];
    private ScreenOrientation oldScreenOrientation;
    private int cameraTextureWidth;
    private int cameraTextureHeight;

    // Use this for initialization
    void Start()
    {
        SaveAndChangeOrientation();
        Thread.Sleep(200);

        cameraTexture = new WebCamTexture(WebCamTexture.devices[0].name);
        this.cameraTextureWidth = cameraTexture.width;
        this.cameraTextureHeight = cameraTexture.height;
        ResizeRequestedResolution();
        cameraTexture.Play();
        Debug.Log("Initial camera texture is " + this.cameraTextureWidth.ToString() + " by " + this.cameraTextureHeight.ToString());
        Debug.Log("Initial camera texture requested resolution is " + this.cameraTexture.requestedWidth.ToString() + " by " + this.cameraTexture.requestedHeight.ToString());

        uiImage = GameObject.Find("cameraImage").GetComponent<RawImage>();
        uiImage.texture = cameraTexture;
        uiImage.rectTransform.rotation = uiImage.rectTransform.rotation * Quaternion.Euler(0f, 0f, 270f);

        ResizeRawImage();

        qr = QR.GetInstance();
        qr.StartThread(cameraTexture.width, cameraTexture.height);
    }

    private void SaveAndChangeOrientation()
    {
        oldAutorotateSettings[0] = Screen.autorotateToPortrait;
        oldAutorotateSettings[1] = Screen.autorotateToLandscapeLeft;
        oldAutorotateSettings[2] = Screen.autorotateToPortraitUpsideDown;
        oldAutorotateSettings[3] = Screen.autorotateToLandscapeRight;
        oldScreenOrientation = Screen.orientation;

        Screen.autorotateToPortrait = false;
        Screen.autorotateToLandscapeLeft = false;
        Screen.autorotateToPortraitUpsideDown = false;
        Screen.autorotateToLandscapeRight = false;
        Screen.orientation = ScreenOrientation.Portrait;
    }

    private void RestoreOrientationSettings()
    {
        Screen.autorotateToPortrait = oldAutorotateSettings[0];
        Screen.autorotateToLandscapeLeft = oldAutorotateSettings[1];
        Screen.autorotateToPortraitUpsideDown = oldAutorotateSettings[2];
        Screen.autorotateToLandscapeRight = oldAutorotateSettings[3];
        Screen.orientation = oldScreenOrientation;
    }

    private void ResizeRawImage()
    {
        // because of screen rotation unity sometimes returns the wrong value so you have to do this
        int shorterSideDimention = (Screen.height < Screen.width) ? Screen.height : Screen.width;

        // Set the size of the image to the width of the screen, adjusting the height porportionately
        uiImage.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, (float)cameraTexture.width * ((float)shorterSideDimention / (float)cameraTexture.height));
        uiImage.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, shorterSideDimention);
    }

    private void ResizeRequestedResolution()
    {
        cameraTexture.requestedWidth = cameraTexture.width;
        cameraTexture.requestedHeight = cameraTexture.height;

        // Request at least 1000pix resolution in one dimention.
        // Roughly, requesting more will cause performance issues and requesting less will look bad.
        while (cameraTexture.requestedHeight < 1000 && cameraTexture.requestedWidth < 1000)
        {
            cameraTexture.requestedWidth = cameraTexture.requestedWidth + cameraTexture.width;
            cameraTexture.requestedHeight = cameraTexture.requestedHeight + cameraTexture.height;
        }
    }

    void Update()
    {
        if (qr.ReadyForNewData)
        {
            qr.SetCameraData(cameraTexture.GetPixels32());
        }

        if (qr.SuccessfullyScanned)
        {
            RestoreOrientationSettings();
            qr.FunctionToCallOnExit();
        }

        // trying to mitigate unity bug of not returning correct camera aspect ratio at first
        if (cameraTextureHeight != cameraTexture.height || cameraTextureWidth != cameraTexture.width)
        {
            this.cameraTextureWidth = cameraTexture.width;
            this.cameraTextureHeight = cameraTexture.height;
            cameraTexture.Stop();
            ResizeRequestedResolution();
            ResizeRawImage();
            cameraTexture.Play();
            Debug.Log("Camera texture resized to " + this.cameraTextureWidth.ToString() + " by " + this.cameraTextureHeight.ToString());
            Debug.Log("Camera texture requested resolution resized to " + this.cameraTexture.requestedWidth.ToString() + " by " + this.cameraTexture.requestedHeight.ToString());
			qr.Update(cameraTextureWidth, cameraTextureHeight);
		}
    }

    public void CancelButtonPressed()
    {
        qr.TryStopThread();
        cameraTexture.Stop();
        RestoreOrientationSettings();
        qr.AbortThread();
        qr.FunctionToCallOnExit();
    }

    void OnDestroy()
    {
        qr.AbortThread();
        cameraTexture.Stop();
    }

    void OnApplicationQuit()
    {
        qr.TryStopThread();
    }
}
