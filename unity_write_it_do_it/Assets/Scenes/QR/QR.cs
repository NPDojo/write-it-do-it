﻿using UnityEngine;
using System.Collections;
using ZXing;
using System.Threading;
using System.Collections.Generic;
using AOT;
using ZXing.QrCode;
using System;

public class QR  {

    public static QR GetInstance()
    {
        if (instance == null)
        {
            lock (threadLock)
            {
                if (instance == null)
                {
                    instance = new QR();
                }
            }
        }

        return instance;
    }

    public static Texture2D Encode(string text)
    {
        BarcodeWriter writer = new BarcodeWriter { Format = BarcodeFormat.QR_CODE, Options = new QrCodeEncodingOptions { Height = 256, Width = 256 } };
        Texture2D t2d = new Texture2D(256, 256, TextureFormat.ARGB32, false);
        t2d.SetPixels32(writer.Write(text));
        t2d.Apply();

        return ScaleUpQrTexture(1024, t2d);
    }

    // Use the following two functions from other scenes to launch the QR scene or read the result.

    // Set before loading the QrDecode scene. The function must call LoadLevel.
    public delegate void FunctionToCallOnExitDelegate();

    public void LanunchQrReader(FunctionToCallOnExitDelegate func)
    {
        this.FunctionToCallOnExit = func;
        Application.LoadLevel("decodeQrScene");
    }

    // returns null if scanning was cancelled
    public string GetQrReaderResult()
    {
        if (this.SuccessfullyScanned)
        {
            // set successfullyscanned to false so the same result isn't used again
            this.SuccessfullyScanned = false;
            return this.LastResult;
        }
        else
        {
            return null;
        }
    }

	public void Update (int width, int height)
	{
		this.width = width;
		this.height = height;
	}



    // Use the following methods from the QR scene

    // When true, call SetCameraData
    public bool ReadyForNewData { get; private set; }

    // when true, call functionToCallOnExit from qr scene
    public bool SuccessfullyScanned { get; private set; }

    public FunctionToCallOnExitDelegate FunctionToCallOnExit { get; private set; }

    // the width and height must be the same of the array that you will send in setCameraData
    // thread will exit when it seccessfully scans or when trystopthread or abort thread are called
    public void StartThread(int width, int height)
    {
        if (this.thread != null)
        {
            this.AbortThread();
            while (this.thread.ThreadState != ThreadState.Stopped)
            {
                Thread.Sleep(100);
            }
        }

        this.stopThread = false;
        this.dataChanged = false;
        this.ReadyForNewData = true;
        this.SuccessfullyScanned = false;

        this.width = width;
        this.height = height;

        this.thread = new Thread(DecodeQR);
        this.thread.Start();
    }

    // if you want to cancel call this first
    public void TryStopThread()
    {
        this.stopThread = true;
    }

    // then call this to force the cancel and thread stop
    public void AbortThread()
    {
        this.stopThread = true;
        this.thread.Abort();
    }

    // call when ready for new data is true
    public void SetCameraData(Color32[] data)
    {
        this.ReadyForNewData = false;
        this.cameraData = data;
        this.dataChanged = true;
    }

    private static QR instance;
    private static readonly object threadLock = new object();
    private string LastResult;
    private bool stopThread;
    private bool dataChanged;
    private int width;
    private int height;
    private Color32[] cameraData;
    private Thread thread;

    private QR()
    {

    }

    private void DecodeQR()
    {
		ZXing.BarcodeReader barcodeReader = new BarcodeReader { AutoRotate = false, TryHarder = false, PossibleFormats = new List<BarcodeFormat> { BarcodeFormat.QR_CODE}};

        while (true)
        {
            if (this.stopThread)
            {
                break;
            }

            while (!this.dataChanged)
            {
                Thread.Sleep(50);
            }

            this.dataChanged = false;

            var result = barcodeReader.Decode(this.cameraData, this.width, this.height);

            if (result != null)
            {
                this.LastResult = result.Text;
                this.SuccessfullyScanned = true;
                break;
            }

            Thread.Sleep(300);

            this.ReadyForNewData = true;
        }
    }
	
	[MonoPInvokeCallback(typeof(QR.FunctionToCallOnExitDelegate))]
	public static void LoadSandboxScene()
	{
		Application.LoadLevel("sandboxScene");
	}

	public void LoadQRButton(){
		QR.GetInstance().LanunchQrReader(LoadSandboxScene);
	}

    private static Texture2D ScaleUpQrTexture(int size, Texture2D input)
    {
        if (size < 256 || !Mathf.IsPowerOfTwo(size)) throw new ArgumentException("size parameter invalid");
        if (input.width != 256 || input.height != 256) throw new ArgumentException("texture is not 256 in width and height");

        int timesBigger = size / 256;
        Texture2D result = new Texture2D(size, size, TextureFormat.ARGB32, false);

        for (int row = 0; row < size; row++)
            for (int column = 0; column < size; column++)
                result.SetPixel(row, column, input.GetPixel(row / timesBigger, column / timesBigger));

        result.filterMode = FilterMode.Point;
        result.Apply();
        return result;
    }
}
