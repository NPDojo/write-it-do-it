﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using AOT;

public class testQrSceneStartScript : MonoBehaviour {

    [MonoPInvokeCallback(typeof(QR.FunctionToCallOnExitDelegate))]
    static void LoadThisScene()
    {
		Application.LoadLevel("testQrScene");
    }

	// Use this for initialization
	void Start () {

        string result = QR.GetInstance().GetQrReaderResult();
        if (result != null)
        {
            GameObject.Find("ResultText").GetComponent<Text>().text = result;
        }
        else
        {
			QR.GetInstance().LanunchQrReader(LoadThisScene);
        }
	}

	public void LoadQRButton(){
		//QR.GetInstance().LanunchQrReader(LoadSandboxScene);
	}
}
