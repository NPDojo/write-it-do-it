﻿using UnityEngine;
using System.Collections;

namespace WriteItDoIt
{
	public class SelectedPiecePanel : MonoBehaviour {
		GameObject panel;
		private GameObject currentSelection = null;

		// Use this for initialization
		void Start () {
			panel = GameObject.Find ("Selected Piece Panel").gameObject;
			panel.SetActive (false);
		}
		
		// Update is called once per frame
		void Update () {
			// If the current selection has changed since the last frame
			if (currentSelection != GameManager.CurrentGame.CurrentSelected)
			{
				currentSelection = GameManager.CurrentGame.CurrentSelected;
				if (currentSelection != null
				    && !Utilities.IsConnectionPoint(currentSelection)
				    && !currentSelection.GetComponent<PieceBehavior>().Piece.IsDisplayPiece){
					panel.gameObject.SetActive(true);
				}
				else {
					panel.gameObject.SetActive(false);
				}
			}
		}
	}
}