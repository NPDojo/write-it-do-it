using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System;
using System.IO;
using UnityEngine.UI;
using ZXing;
using ZXing.QrCode;

namespace WriteItDoIt
{
	public class DisplayCase : MonoBehaviour
	{
		// The piece in the display case window
		public static Piece Piece {
			get;
			set;
		}

		Color color;
		private static float posLerpCoefficient = 0.07f;
		private static float rotLerpCoefficient = 0.01f;
		private static float maxLerpDuration = 0.3f;
		private float time;
		private bool waitingForSecondSelection;
		private bool timed;
		private static SortedDictionary<string, int> toolboxCounts;
		private static Dictionary<string, GameObject> toolboxButtons;
		private static Color defaultColor;
		private static Color unusableColor = Color.gray;
		private GameObject ScreenTitle;
		
		public GameObject PieceButtonPrefab;
		public GameObject PanelPrefab;
		public Transform PieceDropdownPanel;
		public Transform ColorDropdownPanel;
		public Transform AddPieceButton;
		public Transform Timer;
		public Transform TimeUpPanel;
		public Transform ResultsPanel;
		public Transform InitializingPanel;
		public Transform DiscoFromButton;

		// Use this for initialization
		void Start()
		{
			ScreenTitle = GameObject.FindGameObjectWithTag (Constants.ScreenTitleTag);
			Piece = null;
			color = Constants.pieceColors [0];
			defaultColor = PieceButtonPrefab.GetComponent<Button> ().image.color;
			adjustForGameType(GameManager.CurrentGame.Type);
		}

		void Update()
		{
			if (timed) {
				// Update the timer
				time -= Time.deltaTime;
				string timeString = Mathf.Floor (time / 60f).ToString () + ":"
					+ (Mathf.Floor (time % 60f) < 10 ? "0" : "") + Mathf.Floor (time % 60f).ToString ();
				Timer.GetComponent<Text> ().text = timeString;
				if (time < 0f) timeUp ();
			}
		}

		// Takes down the "initializing" panel after waiting long enough to ensure automatic connections are complete
		IEnumerator RemoveInitializingPanel() {
			yield return new WaitForSeconds (Constants.autoConnectTime + 0.1f);
			GameType t = GameManager.CurrentGame.Type;
			if ((t == GameType.DI || t == GameType.WIDI && GameManager.CurrentGame.inDoItPhase) 
			    && GameManager.CurrentGame.LoadedModelStream != null) resetCurrentModel ();
			InitializingPanel.gameObject.SetActive (false);
		}

		void adjustForGameType(GameType t) {
			// Generate the original model from a model stream to actualize connections for OriginalModel object
			// Depending on game type, these generated pieces may be wiped away immediately afterward
			ModelStream ms = GameManager.CurrentGame.LoadedModelStream;
			GameObject sb = GameObject.Find ("SaveButton");
			GameObject lb = GameObject.Find ("LoadButton");
			GameObject db = GameObject.Find ("DoneButton");
			lb.SetActive (false);	
			LoadSceneScript ls = GameObject.Find("Home Button").GetComponent<LoadSceneScript>();

			resetCurrentModel ();
			if (ms != null) {
				InitializingPanel.gameObject.SetActive (true);
				GameManager.CurrentGame.OriginalModel = ms.ReadModel ();
				GameManager.CurrentGame.CurrentModel = GameManager.CurrentGame.OriginalModel;
				Debug.Log (GameManager.CurrentGame.OriginalModel.ToString ());
				Debug.Log (GameManager.CurrentGame.CurrentModel.ToString ());
				ms.Reset ();
			}
			switch (t) {
			case GameType.View: {
				AddPieceButton.gameObject.SetActive (false);
				ScreenTitle.GetComponent<Text>().text = "Model Viewer";
				GameManager.CurrentGame.allowManipulation = false;
				sb.SetActive (false);
				db.SetActive (false);
				break;
			}
			case GameType.Sandbox: {
				DynamicPieceTypeDropdown();
				DynamicColorDropdown();
				db.SetActive (false);
				ScreenTitle.GetComponent<Text>().text = "Sandbox";
				GameManager.CurrentGame.allowManipulation = true;
				break;
			}
			case GameType.WI: {
				AddPieceButton.gameObject.SetActive (false);
				setTimer (PlayerPrefs.GetFloat (Constants.PrefWITime));
				ScreenTitle.GetComponent<Text>().text = "Write It";
				GameManager.CurrentGame.allowManipulation = false;
				sb.SetActive (false);
				TimeUpPanel.FindChild("InfoText").GetComponent<Text>().text = "Thanks for playing!";
				TimeUpPanel.FindChild("Button").GetChild (0).GetComponent<Text>().text = "Main Menu";
				Button b = TimeUpPanel.FindChild("Button").GetComponent<Button>();
				b.onClick.AddListener(
					() => 
					{ls.LeaveModelingScene();}
				);
				break;
			}
			case GameType.WIDI: {
				sb.SetActive (false);
				if (GameManager.CurrentGame.inDoItPhase) {
					DynamicReconstructionPieceDropdown(); // Populate the piece list with the pieces of the model
					ColorDropdownPanel.parent.gameObject.SetActive(false); // Remove color button

					setTimer (PlayerPrefs.GetFloat (Constants.PrefDITime));
					GameManager.CurrentGame.allowManipulation = true;
					TimeUpPanel.FindChild("InfoText").GetComponent<Text>().text = "Press below to see your score!";
					TimeUpPanel.FindChild("Button").GetChild (0).GetComponent<Text>().text = "Score";
					Button b = TimeUpPanel.FindChild("Button").GetComponent<Button>();
					b.onClick.AddListener(
						() => 
						{ResultsPanel.gameObject.SetActive(true);
						ResultsScreen();}
					);
				} else { // Write-It phase
					AddPieceButton.gameObject.SetActive (false);
					setTimer (PlayerPrefs.GetFloat (Constants.PrefWITime));
					GameManager.CurrentGame.allowManipulation = false;
					TimeUpPanel.FindChild("InfoText").GetComponent<Text>().text = "Pass the device and instructions to the builder.";
					TimeUpPanel.FindChild("Button").GetChild (0).GetComponent<Text>().text = "Begin \"Do It\"!";
					Button b = TimeUpPanel.FindChild("Button").GetComponent<Button>();
					b.onClick.AddListener(
						() => 
						{ls.LeaveModelingScene();}
					);
				}
				ScreenTitle.GetComponent<Text>().text = "Write It Do It";
				break;
			}
			case GameType.DI: {
				sb.SetActive (false);
				DynamicReconstructionPieceDropdown(); // Populate the piece list with the pieces of the model
				ColorDropdownPanel.parent.gameObject.SetActive(false); // Remove color button
				setTimer (PlayerPrefs.GetFloat (Constants.PrefDITime));
				ScreenTitle.GetComponent<Text>().text = "Do It";
				GameManager.CurrentGame.allowManipulation = true;
				TimeUpPanel.FindChild("InfoText").GetComponent<Text>().text = "Press below to see your score!";
				TimeUpPanel.FindChild("Button").GetChild (0).GetComponent<Text>().text = "Score";
				Button b = TimeUpPanel.FindChild("Button").GetComponent<Button>();
				b.onClick.RemoveAllListeners();
				b.onClick.AddListener(
					() => 
					{ResultsPanel.gameObject.SetActive(true);
					ResultsScreen();}
				);
				break;
			}
			default: {
				Debug.Log ("Error: loaded sandboxScene without gametype set, assuming sandbox mode (if scene loaded in isolation for debug, ignore)");
				DynamicPieceTypeDropdown();
				DynamicColorDropdown();
				db.SetActive (false);
				ScreenTitle.GetComponent<Text>().text = "Sandbox";
				GameManager.CurrentGame.allowManipulation = true;
				break;
			}
			}
			StartCoroutine ("RemoveInitializingPanel");
		}

		// Resets the current model, both in terms of game objects and the CurrentModel object
		// Intended for use after loading the original model
		public static void resetCurrentModel() {
			GameObject modelObj = GameObject.FindGameObjectWithTag (Constants.ModelTag);
			// Destroy all of the pieces that currently exist in the model
			if (modelObj != null) {
				foreach (Transform str in modelObj.transform) {
					GameObject.Destroy (str.gameObject);
				}
			}
			GameManager.CurrentGame.CurrentModel = new Model ();
			GameManager.CurrentGame.OverlappingPieceCount = 0;
		}

		// Activates a timer for the current modeling scene
		void setTimer(float time) {
			timed = true;
			this.time = time;
			Timer.gameObject.SetActive (true);
		}

		// Stops all further manipulation and brings up a panel for the user to pass the device, score, etc.
		void timeUp() {
			timed = false;
			GameManager.CurrentGame.allowManipulation = false;
			TimeUpPanel.gameObject.SetActive (true);
		}

		// Creates a toolbox-style dropdown for selecting pieces
		void DynamicReconstructionPieceDropdown()
		{
			if (GameManager.CurrentGame.OriginalModel == null) {
				Debug.Log ("No OriginalModel; toolbox construction aborted");
				return;
			}

			// Stretch out the piece button
			PieceDropdownPanel.transform.parent.GetComponent<RectTransform> ().Translate (
				new Vector3(GameObject.Find ("Canvas").GetComponent<Canvas> ().scaleFactor * 65,0,0));

			Vector2 scale = PieceDropdownPanel.transform.parent.GetComponent<RectTransform> ().sizeDelta;
			scale.x *= 2;
			PieceDropdownPanel.transform.parent.GetComponent<RectTransform> ().sizeDelta = scale;

			// Stretch out the panel the same way, to stretch the generated buttons
			scale = PieceDropdownPanel.transform.GetComponent<RectTransform> ().sizeDelta;
			scale.x *= 2;
			PieceDropdownPanel.transform.GetComponent<RectTransform> ().sizeDelta = scale;

			toolboxButtons = new Dictionary<string, GameObject> ();
			toolboxCounts = new SortedDictionary<string, int> (); // TODO add fancy comparator to sort by piece type first, then color

			// These two just hold types and colors for the buttons so that we don't have to derive them from the color-name string
			Dictionary<string, Type> toolboxPieceTypes = new Dictionary<string, Type>();
			Dictionary<string, int> toolboxPieceColors = new Dictionary<string, int>();

			foreach (Piece p in GameManager.CurrentGame.OriginalModel.Pieces) {
				if (!toolboxCounts.ContainsKey (p.ColorAndName())) toolboxCounts.Add (p.ColorAndName(), 0);
				toolboxCounts[p.ColorAndName()]++;
				if (!toolboxPieceTypes.ContainsKey (p.ColorAndName())) toolboxPieceTypes.Add (p.ColorAndName(), p.GetType());
				if (!toolboxPieceColors.ContainsKey (p.ColorAndName())) toolboxPieceColors.Add (p.ColorAndName(), p.ColorIndex);
			}

			int index = 0;
			// Create first page
			GameObject panel = (GameObject)Instantiate(PanelPrefab);
			panel.transform.SetParent(PieceDropdownPanel, false);
			GameObject firstPage = panel;
			foreach (string pieceString in toolboxCounts.Keys) {
				if (index >= Constants.DropDownPageSize && index % Constants.DropDownPageSize == 0) { // if it's time for a new page
					GameObject oldPanel = panel;
					// Create new page panel
					panel = (GameObject)Instantiate(PanelPrefab);
					panel.transform.SetParent(PieceDropdownPanel, false);
					panel.SetActive(false); // Every panel but the first should start out inactive

					// Create next-page button for previous panel
					GameObject nextPanel = panel;
					GameObject pageButton = (GameObject)Instantiate(PieceButtonPrefab);
					pageButton.GetComponentInChildren<Text>().text = "Page " + index / Constants.DropDownPageSize + " (Next)";
					pageButton.GetComponent<Button>().onClick.AddListener(
						() => {
						oldPanel.SetActive(false);
						nextPanel.SetActive(true);}
					);
					pageButton.transform.SetParent(oldPanel.transform, false);
				}
				// Make the new button
				GameObject butt = (GameObject)Instantiate(PieceButtonPrefab);
				butt.GetComponentInChildren<Text>().text = pieceString + " x" + toolboxCounts[pieceString];
				toolboxButtons.Add (pieceString, butt);

				string s = pieceString;
				butt.GetComponent<Button>().onClick.AddListener(
					() => {
					if (toolboxCounts[s] > 0) {
						changePieceType (toolboxPieceTypes[s]);
						changePieceColor (Constants.pieceColors[toolboxPieceColors[s]]);
						hidePanel();}
					}
				);
				butt.transform.SetParent(panel.transform, false);
				index++;
			}
			if (panel != firstPage) { // if there is more than one page
				// Make a button in the last page leading back to the first
				GameObject pageButton = (GameObject)Instantiate(PieceButtonPrefab);
				pageButton.GetComponentInChildren<Text>().text = "Page " + (index / Constants.DropDownPageSize + 1) + " (First)";
				pageButton.GetComponent<Button>().onClick.AddListener(
					() => {
					panel.SetActive(false);
					firstPage.SetActive(true);}
				);
				pageButton.transform.SetParent(panel.transform, false);
			}
		}

		// For use in toolbox-style reconstruction mode
		// Reduces the inventory of the specified piece/color
		public static void takeFromToolbox(string colorAndName) {
			if(GameManager.CurrentGame.Type == GameType.DI || GameManager.CurrentGame.Type == GameType.WIDI && GameManager.CurrentGame.inDoItPhase)
			{
				toolboxCounts [colorAndName]--;
				toolboxButtons[colorAndName].transform.GetChild(0).GetComponent<Text>().text = colorAndName + " x" + toolboxCounts[colorAndName];
				if (toolboxCounts [colorAndName] <= 0) {
					toolboxButtons[colorAndName].GetComponent<Button>().image.color = unusableColor;
				}
				// Destroy the display piece
				Destroy(Piece.GameObject.transform.parent.gameObject);
				Destroy(Piece.GameObject);
				Piece = null;
			}
		}

		// For use in toolbox-style reconstruction mode
		// Increases the inventory of the specified piece/color
		public static void returnToToolbox(string colorAndName) {
			if(GameManager.CurrentGame.Type == GameType.DI || GameManager.CurrentGame.Type == GameType.WIDI && GameManager.CurrentGame.inDoItPhase)
			{
				if (toolboxCounts [colorAndName] <= 0) {
					toolboxButtons[colorAndName].GetComponent<Button>().image.color = defaultColor;
				}
				toolboxCounts [colorAndName]++;
				toolboxButtons[colorAndName].transform.GetChild (0).GetComponent<Text>().text = colorAndName + " x" + toolboxCounts[colorAndName];
			}
		}
		
		// Creates a paged dropdown list with a button for each type of piece
		void DynamicPieceTypeDropdown()
		{
			IEnumerable types = typeof(Piece).Assembly.GetTypes()
				.Where(type => type.IsSubclassOf(typeof(Piece)) 
				       && type != typeof(MalePiece) && type != typeof(FemalePiece));

			Utilities.DynamicDropdown(PieceDropdownPanel, types, 
			                (object item) =>
			                {
								Type t = (Type)item;
								return t.Name.Substring(0, t.Name.LastIndexOf("Piece")) 
									+ (t.IsSubclassOf(typeof(MalePiece)) ? " (M)" : " (F)");
							},
			                (object item) => 
			                {
								return () => 
								{
									changePieceType((Type)item);
									hidePanel();	
								};
							});
		}

		// Creates a paged dropdown list with a button for each color
		void DynamicColorDropdown()
		{
			Utilities.DynamicDropdown(ColorDropdownPanel, Constants.pieceColors,
                (object item) =>
                {
					Color c = (Color)item;
					
					for (int i = 0; i < Constants.pieceColors.Length; i++)
					{
						if (c == Constants.pieceColors[i])
						{
							return Constants.pieceColorNames[i];
						}
					}
					return null;
				},
				(object item) =>
				{
					return () => 
					{
						changePieceColor((Color)item);
						hidePanel();
					};
				});
		}

		void hidePanel()
		{
			PieceDropdownPanel.gameObject.SetActive(false);
			ColorDropdownPanel.gameObject.SetActive(false);
		}

		// Destroys the current display case piece and makes a new piece of the given type
		void changePieceType(Type type)
		{
			if (Piece != null) {
				Destroy(Piece.GameObject.transform.parent.gameObject); // This piece's corresponding parent structure
				Destroy(Piece.GameObject);
			}

			Piece = (Piece)Activator.CreateInstance(Type.GetType("WriteItDoIt." + type.Name), new System.Object[] {color});
			Piece.IsDisplayPiece = true;

			// Rotates such that the first connection point is facing the camera
			Quaternion displayRotation = Quaternion.FromToRotation (Piece.ConnectionPoints[0].Normal, Vector3.back);
			
			Piece.GameObject.transform.rotation = displayRotation;
			// Rotate a bit to see depth of piece better
			Piece.GameObject.transform.Rotate (new Vector3 (45f, 45f, 0f)); 
			Piece.GameObject.transform.parent.SetParent (gameObject.transform, true);
			Piece.GameObject.transform.parent.transform.localPosition = Vector3.zero;
		}

		// Change the color of the display piece
		void changePieceColor(Color col)
		{
			if (Piece != null) {
				Piece.ChangeColor(col);
				color = col;
			}
		}

		IEnumerator LerpRotation(Quaternion newRotation)
		{    
			float t = 0.0f;
			Quaternion startingRot = Piece.GameObject.transform.rotation;

			float lerpSpeed =
				Mathf.Min(rotLerpCoefficient * (startingRot.eulerAngles - newRotation.eulerAngles).magnitude,
				          maxLerpDuration);

			while (t < 1.0f) {
				t += Time.deltaTime * (Time.timeScale / lerpSpeed);
				
				Piece.GameObject.transform.rotation = Quaternion.Lerp(startingRot, newRotation, t);
				yield return 0;
			}    
		}

		// Handles when the user chooses to complete the round before the timer is up
		public void EarlyDoneButton(){
			timed = false;
			TimeUpPanel.FindChild("TimeUpText").GetComponent<Text>().text = "Done!";
			GameObject sp = GameObject.Find ("SubmitPanel");
			sp.SetActive (false);
			GameManager.CurrentGame.allowManipulation = false;
			TimeUpPanel.gameObject.SetActive (true);
		}

		// No longer in use; model loading is done from gallery
		public void LoadQRButton(){
			QR.GetInstance().LanunchQrReader(QR.LoadSandboxScene);
		}

		// No longer in use; model exporting is done from gallery
		public void GenerateQRButton()
        {
            RawImage image = GameObject.Find("QRCode").GetComponent<RawImage>();
            image.texture = QR.Encode(Utilities.SerializeModelToString (GameManager.CurrentGame.CurrentModel));
			image.uvRect = new Rect (0, 0, 1, 1);
            image.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, image.rectTransform.rect.height);
            image.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, image.rectTransform.rect.height);
		}

		// No longer in use; model loading is done from gallery
        public void LoadStringButton()
        {
			string modelString = GameObject.Find("ModelStringInput").GetComponent<InputField>().text;
			DisplayCase.resetCurrentModel();
			GameManager.CurrentGame.CurrentModel = Utilities.DeserializeModelFromString(modelString);
			GameObject lsp = GameObject.Find ("LoadStringPanel");
			lsp.SetActive (false);
			GameObject lp = GameObject.Find ("LoadPanel");
			lp.SetActive (false);
		}

		// No longer in use; model exporting is done from gallery
		public void GenerateStringButton(){
			string modelString = Utilities.SerializeModelToString (GameManager.CurrentGame.CurrentModel);
			GameObject.Find ("ModelStringText").GetComponent<ConstantInputField> ().constantValue = modelString;
		}
		
		public void ResultsScreen(){
			String timeRemaining = Timer.GetComponent<Text> ().text;
			GameObject.Find("TimeText").GetComponent<Text>().text = "Time remaining: " + timeRemaining;
			int score = GameManager.CurrentGame.score (GameManager.CurrentGame.CurrentModel);
			int maxScore = GameManager.CurrentGame.score (GameManager.CurrentGame.OriginalModel);
			GameObject.Find("ScoreText").GetComponent<Text>().text = "Score: " + score + "/" + maxScore;
			PersistenceBehavior.SaveScore (score, maxScore);
		}

		public void DisconnectFromButton()
		{
			GameObject sel = GameManager.CurrentGame.CurrentSelected;
			if (!waitingForSecondSelection) { // Disregard repeated button presses
				StartCoroutine(WaitForSecondSelection(sel));
				waitingForSecondSelection = true;
			}
		}

		private IEnumerator WaitForSecondSelection(GameObject firstPiece){
			// Change color of button to reflect that it's in use
			DiscoFromButton.GetComponent<Button> ().image.color = unusableColor;

			GameObject currentSelected = GameManager.CurrentGame.CurrentSelected;
			while (firstPiece.Equals(currentSelected))
			{
				yield return 0;
				currentSelected = GameManager.CurrentGame.CurrentSelected;
			}
			// See if the user selected a piece (or a connection point attached to a piece)
			Piece thisPiece = firstPiece.GetComponent<PieceBehavior>().Piece, otherPiece = null;
			PieceBehavior otherPieceBehavior = null;
			ConnectionPointBehavior otherPointBehavior = null;
			if (currentSelected != null) {
				otherPieceBehavior = currentSelected.GetComponent<PieceBehavior>();
				otherPointBehavior = currentSelected.GetComponent<ConnectionPointBehavior>();
			}
			if (otherPointBehavior != null) otherPiece = otherPointBehavior.Piece;
			if (otherPieceBehavior != null) otherPiece = otherPieceBehavior.Piece;
			// Disconnect the two pieces
			if (otherPiece != null && thisPiece != otherPiece) {
				thisPiece.DisconnectFrom(otherPiece);
			} else {
				StartCoroutine(GameObject.Find ("Main Camera").GetComponent<TouchControl>().ShakeCamera());
			}
			GameManager.CurrentGame.CurrentSelected = null;
			waitingForSecondSelection = false;

			// Change button color back to normal
			DiscoFromButton.GetComponent<Button> ().image.color = defaultColor;
		}

		// Disconnects everything from the currently selected piece
		public void DisconnectAllButton()
		{
			GameObject sel = GameManager.CurrentGame.CurrentSelected;
			if (sel != null && sel.CompareTag(Constants.PieceTag)) {
				Piece.DisconnectAll(sel.GetComponent<PieceBehavior>().Piece);
			}
			GameManager.CurrentGame.CurrentSelected = null;
		}

		// Deletes the currently selected piece
		public void DeleteButton()
		{
			GameObject sel = GameManager.CurrentGame.CurrentSelected;
			if (sel != null && sel.CompareTag(Constants.PieceTag)) {
				Piece p = sel.GetComponent<PieceBehavior>().Piece;
				if(GameManager.CurrentGame.Type == GameType.DI || GameManager.CurrentGame.Type == GameType.WIDI && GameManager.CurrentGame.inDoItPhase) 
					returnToToolbox (p.ColorAndName());
				Piece.DisconnectAll(p, false);
				GameObject parent = sel.transform.parent.gameObject;
				GameManager.CurrentGame.CurrentModel.Remove (p);
				Action postLerpDestroy = delegate 
				{
					GameObject.Destroy (sel);
					GameObject.Destroy (parent);
				};
				p.PieceBehavior.LerpTo (sel.transform.rotation, new Vector3(30f,Constants.floorHeight-30f,0f),
				                        0.1f, postLerpDestroy);
			}
		}

		// Moves all the pieces in the workspace so that the average piece (x,z) is (0,0) and minimum y is 0
		public void RecenterButton()
		{
			GameObject model = GameObject.FindWithTag(Constants.ModelTag);
			if (model != null) {
				// Calculate the average position of each piece
				// Note: the y component is not the average, but the min Y
				float x = 0f, minY = float.MaxValue, z = 0f, pieceCount = 0f;
				foreach (Transform str in model.transform) {
					foreach (Transform p in str) {
						x += p.position.x;
						minY = Mathf.Min(minY, p.position.y);
						z += p.position.z;
						pieceCount += 1;
					}
				}
				Vector3 avgPosition = new Vector3(x / pieceCount, minY, z / pieceCount);
				// Slide the model so that the average piece x/z location is the origin and minY is zero
				StartCoroutine(LerpTranslate(model, -avgPosition));
			}
		}

		private IEnumerator LerpTranslate(GameObject obj, Vector3 translateVector)
		{    
			float t = 0.0f;
			Vector3 startingPos = obj.transform.position;
			Vector3 newPos = startingPos + translateVector;
			
			float lerpSpeed = Mathf.Min(posLerpCoefficient * translateVector.magnitude,
							          	maxLerpDuration);
			
			while (t < 1.0f) {
				t += Time.deltaTime * (Time.timeScale / lerpSpeed);
				
				obj.transform.position = Vector3.Lerp(startingPos, newPos, t);
				yield return 0;
			}    
		}

		// Deselect the current selection if it is part of the display case piece
		public void DeselectDisplay()
		{
			GameObject sel = GameManager.CurrentGame.CurrentSelected;
			if (sel) {
				if (Utilities.IsConnectionPoint(sel))
					sel = sel.gameObject.transform.parent.gameObject;
				if (sel.GetComponent<PieceBehavior>().Piece.IsDisplayPiece)
					GameManager.CurrentGame.CurrentSelected = null;
			}
		}

		public void rotateR()
		{
			if (Piece != null) {
				Piece.GameObject.transform.RotateAround(Piece.GameObject.transform.position, Vector3.up, -45);
			}
		}
		
		public void rotateL()
		{
			if (Piece != null) {
				Piece.GameObject.transform.RotateAround(Piece.GameObject.transform.position, Vector3.up, 45);
			}
		}
		
		public void rotateF()
		{
			if (Piece != null) {
				Piece.GameObject.transform.RotateAround(Piece.GameObject.transform.position, Vector3.right, 45);
			}
		}
		
		public void rotateB()
		{
			if (Piece != null) {
				Piece.GameObject.transform.RotateAround(Piece.GameObject.transform.position, Vector3.right, -45);
			}
		}

		public void scoreModel()
		{
			Game game = GameManager.CurrentGame;
			Debug.Log("Current Model: " + game.CurrentModel.ToString());
			Debug.Log("Original Model: " + (game.OriginalModel != null ? game.OriginalModel.ToString() : "none!"));
//			Debug.Log("Score with self: " + game.score(game.CurrentModel));
			Utilities.Alert("Current score: " + game.score(game.CurrentModel)
			                + (game.OriginalModel == null ? " (score is with self)" : ""));
		}

		public void saveModel()
		{
			string filePath = Constants.fileDirectory + "/" + "testModel";
			ModelStream modelStream = new ModelStream(new FileStream(filePath, FileMode.OpenOrCreate));
			modelStream.WriteModel(GameManager.CurrentGame.CurrentModel);
		}


		// No longer in use; model loading is done from gallery
		public void loadModel()
		{
			if (Directory.GetFiles(Constants.fileDirectory).Count() > 0) {
				string filePath = Directory.GetFiles(Constants.fileDirectory) [0];
				ModelStream modelStream = new ModelStream(new FileStream(filePath, FileMode.Open));
				GameManager.CurrentGame.OriginalModel = modelStream.ReadModel();
			} else {
				Debug.Log("No available models to load");
			}
		}
	}
}
