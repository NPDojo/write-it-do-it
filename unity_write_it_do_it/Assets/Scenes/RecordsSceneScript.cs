﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace WriteItDoIt
{
	public class RecordsSceneScript : MonoBehaviour {

		public Transform recordsPanel;
		public Transform showingPageText;

		private int currentPage = 0;

		// Use this for initialization
		void Start () {
			Utilities.DynamicRecordDropdown (recordsPanel, PersistenceBehavior.ReadScoreLog());
			UpdateShowingText ();
		}
		
		// Update is called once per frame
		void Update () {
		
		}

		private void UpdateShowingText() {
			showingPageText.GetComponent<Text> ().text = "Page " + (currentPage + 1).ToString ()
				+ " of " + recordsPanel.childCount.ToString ();
		}

		public void PreviousButton() {
			recordsPanel.GetChild (currentPage).gameObject.SetActive (false);
			currentPage = (currentPage - 1 + recordsPanel.childCount) % recordsPanel.childCount;
			recordsPanel.GetChild (currentPage).gameObject.SetActive (true);
			UpdateShowingText ();
		}

		public void NextButton() {
			recordsPanel.GetChild (currentPage).gameObject.SetActive (false);
			currentPage = (currentPage + 1) % recordsPanel.childCount;
			recordsPanel.GetChild (currentPage).gameObject.SetActive (true);
			UpdateShowingText ();
		}

		public void ResetRecordsButton() {
			PersistenceBehavior.ClearScoreLog ();
			GameObject.Find("Home Button").GetComponent<LoadSceneScript>().LoadRecordsScene();
		}
	}
}