using UnityEngine;
using System.Collections;
using System.IO;
using System.Linq;
using WriteItDoIt;

public class LoadSceneScript : MonoBehaviour {

	// For the Do It part of WIDI mode
	private void ReconstructModelScene()
	{
		GameManager.CurrentGame.inDoItPhase = true;
		LoadModelingScene ();
	}

	public void LoadModelingScene()
	{
		if (GameManager.CurrentGame.Type == GameType.None) {
			Debug.Log ("GameType unspecified; scene not loaded");
			Utilities.Alert ("Please select a game type.");
		} else if (GameManager.CurrentGame.LoadedModelStream == null && GameManager.CurrentGame.Type != GameType.Sandbox) {
			Debug.Log ("No model selected; scene not loaded.");
			Utilities.Alert ("Please select a model");
		} else {
			Application.LoadLevel ("sandboxScene");
			Debug.Log ("loaded sandboxScene with GameType " + GameManager.CurrentGame.Type);
		}
	}

	public void LeaveModelingScene()
	{
		Debug.Log ("leaving sandboxScene");
		if (GameManager.CurrentGame.Type == GameType.View) {
			LoadGalleryScene ();
			//TODO initialize currentGame?
		} else if (GameManager.CurrentGame.Type == GameType.Sandbox) {
			LoadMainMenuScene(); // save?
		} else if (GameManager.CurrentGame.Type == GameType.WI) {
			LoadMainMenuScene();
		} else if (GameManager.CurrentGame.Type == GameType.WIDI) {
			if (GameManager.CurrentGame.inDoItPhase) LoadMainMenuScene (); // score?
			else ReconstructModelScene();
		} else if (GameManager.CurrentGame.Type == GameType.DI) {
			LoadMainMenuScene();
		}
	}

	public void ViewerButton()
	{
		GameManager.CurrentGame.Type = GameType.View;
		LoadModelingScene ();
	}

	public void SandboxButton()
	{
		GameManager.CurrentGame.Type = GameType.Sandbox;
		LoadModelingScene ();
	}

	public void LoadMainMenuScene()
	{
		ModelStream ms = GameManager.CurrentGame.LoadedModelStream;
		if (ms != null) ms.Close ();
		GameManager.CurrentGame = new Game (); // Reinitialize the current game
		Application.LoadLevel ("mainMenuScene");
		Debug.Log ("loaded mainMenuScene");
	}

	public void LoadGalleryScene()
	{
		Application.LoadLevel ("galleryScene");
		Debug.Log ("loaded galleryScene");
	}

	public void LoadRecordsScene()
	{
		Application.LoadLevel ("recordsScene");
		Debug.Log ("loaded recordsScene");
	}

	public void LoadPlayScene()
	{
		Application.LoadLevel ("playScene");
		Debug.Log ("loaded playScene");
	}

	public void Quit() {
		Debug.Log ("Quitting");
		Application.Quit();
	}

	public void About() {
		Debug.Log ("About");
		// Load a Science Olympiad page
		Application.OpenURL ("http://soinc.org/write_do_b");
	}

	public void SetTypeWI() {
		GameManager.CurrentGame.Type = GameType.WI;
	}

	public void SetTypeWIDI() {
		GameManager.CurrentGame.Type = GameType.WIDI;
	}

	public void SetTypeDI() {
		GameManager.CurrentGame.Type = GameType.DI;
	}

}
