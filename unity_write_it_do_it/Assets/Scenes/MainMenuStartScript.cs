﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using WriteItDoIt;

public class MainMenuStartScript : MonoBehaviour {

	public Transform WITimeField;
	public Transform DITimeField;

	// Use this for initialization
	void Start () {
		// Locks orientation
		Screen.autorotateToPortrait = false;
		Screen.autorotateToPortraitUpsideDown = false;
		Screen.autorotateToLandscapeRight = true;
		Screen.autorotateToLandscapeLeft = true;

		initializePreferences ();
		initializeSettings ();

		GameManager.CurrentGame = new Game();
	}

	// Gives default values to application preferences if they are undefined
	private void initializePreferences() {
		if (!PlayerPrefs.HasKey (Constants.PrefWITime)) {
			PlayerPrefs.SetFloat(Constants.PrefWITime, Constants.defaultWITime);
		}
		if (!PlayerPrefs.HasKey (Constants.PrefDITime)) {
			PlayerPrefs.SetFloat(Constants.PrefDITime, Constants.defaultDITime);
		}
	}

	public void restoreDefaults()
	{
		PlayerPrefs.SetFloat(Constants.PrefWITime, Constants.defaultWITime);
		PlayerPrefs.SetFloat(Constants.PrefDITime, Constants.defaultDITime);
		initializeSettings();
	}

	// Puts the current values as placeholders in the relevant fields in the settings panel
	public void initializeSettings() {
		// Set the placeholder text to reflect the current value
		string WITimeString = (PlayerPrefs.GetFloat (Constants.PrefWITime) / 60f).ToString () + " minutes";
		WITimeField.GetChild (WITimeField.childCount-2).GetComponent<Text> ().text = WITimeString;
		// Reset any current input in the field
		WITimeField.GetComponent<InputField> ().text = "";

		string DITimeString = (PlayerPrefs.GetFloat (Constants.PrefDITime) / 60f).ToString () + " minutes";
		DITimeField.GetChild (DITimeField.childCount-2).GetComponent<Text> ().text = DITimeString;
		DITimeField.GetComponent<InputField> ().text = "";
	}

	// Attempt to save the values in each field in the settings panel
	public void saveSettings() {
		string WITimeString = WITimeField.GetComponent<InputField> ().text;
		if (!WITimeString.Equals ("")) { // if modified
			PlayerPrefs.SetFloat (Constants.PrefWITime, Mathf.Max (float.Parse(WITimeString) * 60f, 60f));
		}
		string DITimeString = DITimeField.GetComponent<InputField> ().text;
		if (!DITimeString.Equals ("")) { // if modified
			PlayerPrefs.SetFloat (Constants.PrefDITime, Mathf.Max (float.Parse(DITimeString) * 60f, 60f));
		}
	}
	
	public void deleteModelsButton() {
		PersistenceBehavior.DeleteUserModels ();
	}

	public void deleteRecordsButton() {
		PersistenceBehavior.ClearScoreLog ();
	}
}
