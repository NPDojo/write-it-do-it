using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System;
using System.IO;


namespace WriteItDoIt
{
	public class StartScript : MonoBehaviour {

		// Use this for initialization
		void Start () 
		{
			CreatePlane ();
			AddLights ();
		}

		public static void CreatePlane()
		{
			float radius = 10f;
			float depth = .5f;
			float lipWidth = 1f;
			float anglePerSlice = .05f;
			
			List<Vector3> vertices = new List<Vector3> ();
			// center top
			vertices.Add (new Vector3 (0f, depth/2.0f, 0f));
			// center bottom
			vertices.Add (new Vector3 (0f, -depth/2.0f, 0f));
			
			List<int> triangles = new List<int> ();
			
			float angle = 0.0f;
			int count = 0;
			while (angle < 2*Mathf.PI) {
				// top face
				vertices.Add (new Vector3(radius*Mathf.Sin(angle),depth/2.0f,radius*Mathf.Cos(angle)));
				// bottom face
				vertices.Add (new Vector3((radius+lipWidth)*Mathf.Sin(angle),-depth/2.0f,(radius+lipWidth)*Mathf.Cos(angle)));
				// top triangle
				triangles.Add(0); triangles.Add(4+(count*2)); triangles.Add(2+(count*2));
				// edge triangels
				triangles.Add(2+(2*count)); triangles.Add(5+(count*2)); triangles.Add(3+(count*2));
				triangles.Add(4+(2*count)); triangles.Add(5+(count*2)); triangles.Add(2+(count*2));
				// bottom trangle
				triangles.Add(1); triangles.Add(3+(count*2)); triangles.Add(5+(count*2));
				
				angle += anglePerSlice;
				count++;
			}
			
			Vector3[] vertexArray = vertices.ToArray ();
			int[] triangleArray = triangles.ToArray ();
			
			for (int index = 0; index < triangles.Count; index++) {
				if (triangleArray[index] >= vertices.Count)
				{
					triangleArray[index] = (triangleArray[index] % 2) + 2;
				}
			}
			
			GameObject gameObject = null;
			
			gameObject = new GameObject("Plane");
			gameObject.AddComponent<MeshFilter>();
			gameObject.AddComponent<MeshRenderer>();
			gameObject.GetComponent<MeshRenderer>().material.SetColor("_Color",new Color(0.3f, 0.3f, 0.3f));
			
			Mesh mesh = new Mesh();
			
			mesh.vertices = vertexArray;
			mesh.triangles = triangleArray;
			Vector2[] uvs = new Vector2[vertexArray.Length];
			int i = 0;
			while (i < uvs.Length) {
				uvs[i] = new Vector2(vertices[i].x, vertices[i].z);
				i++;
			}

			mesh.uv = uvs;
			mesh.RecalculateNormals();
			
			gameObject.GetComponent<MeshFilter>().mesh = mesh;
			
			gameObject.transform.Translate (new Vector3 (0, Constants.floorHeight, 0), Space.World);			
		}

		private void AddLights()
		{
			GameObject parent = new GameObject ("Generated Lighting");
			for (int x = -40; x < 40; x+=3) {
				for (int z = -40; z < 40; z+=3) {
					GameObject light = new GameObject("Light");
					light.transform.SetParent (parent.transform);
					light.AddComponent<Light>();
					light.GetComponent<Light>().type = LightType.Spot;
					light.GetComponent<Light>().range = 30;
					light.GetComponent<Light>().intensity = .06f;
					light.GetComponent<Light>().spotAngle = 70f;
					light.transform.Translate(new Vector3(x, Mathf.Sqrt(x*x + z*z), z));
					light.GetComponent<Light>().transform.LookAt(new Vector3(UnityEngine.Random.Range(0f,10f),UnityEngine.Random.Range(0f,10f),UnityEngine.Random.Range(0f,10f)));
				}
			}


		}
	}
}
