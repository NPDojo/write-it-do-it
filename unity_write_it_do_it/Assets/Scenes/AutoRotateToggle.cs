using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using WriteItDoIt;

public class AutoRotateToggle : MonoBehaviour {
	private GameObject cam;
	private TouchControl touchControl;
	Text buttonText;

	// Use this for initialization
	void Start () {
		cam = GameObject.Find ("Main Camera");
		touchControl = cam.GetComponent<TouchControl>();
		buttonText = transform.FindChild("Text").GetComponent<Text>();
	}

	void Update () {
		if (touchControl.autoRotate) {
			buttonText.text = "Pause";
		} else {
			buttonText.text = "Auto-rotate";
		}
	}
}
