﻿using UnityEngine;
using System.Collections;

public class ButtonTapDetector : MonoBehaviour
{

	// Use this for initialization
	void Start()
	{
	
	}
	
	// Update is called once per frame
	void Update()
	{
		if (Input.touchCount > 0) {
			Touch touch = Input.GetTouch(0);
			Ray ray = Camera.main.ScreenPointToRay(touch.position);
			RaycastHit hit = new RaycastHit();
		
			if (Physics.Raycast(ray, out hit, Mathf.Infinity)) {
				if (hit.collider.gameObject == this.gameObject) {
					switch (touch.phase) {
						case TouchPhase.Began://if the touch begins
							Debug.Log("Button was tapped");
							break;
					}
				}
			}
		}
	}
}
