using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using WriteItDoIt;

public class TouchControl : MonoBehaviour
{
	public bool autoRotate {
		get;
		set;
	}
	
	public bool bound;
	public Texture2D lineTexture;
	public int autoRotateSpeed = 20;
	public bool isConnecting = false;
	public Vector2 startConnectingDragPosition;
	private static GameObject selectedPiece = null;
	private static Color selectedPieceOriginalColor;
	private static Vector3 initialPosition;
	private static Quaternion initialRotation;
	private static float floor = Constants.floorHeight + 0.5f;
	private float zoomSpeed = 0.05f;
	private float zoomMin = 3.0f;
	private float panSpeed = 0.07f;
	private float posLerpCoefficient = 0.07f;
	private float rotLerpCoefficient = 0.01f;
	private float maxLerpDuration = 0.7f;
	private float orbitDirectionLockThreshold = 2f;
	private float touchDuration = 0;
	private Touch touch;
	private float zoomLockThreshold = 2f;
	private Vector2 touchDeltaSinceLastRotation;
	private Vector3 originalPosition;
	private Quaternion originalRotation;
	private float shakeIntensity;
	private float shakeDecay;
	private float camRotationMax = 80f;
	private float lineWidth = 4;
	private float circleRadius = 50;
	private float cpRotationThreshold = 100;
	private bool isRotatingPiece = false;
	private Transform rotatingStr, rotatingPiece, rotatingConPoint;

	// Use this for initialization
	void Start()
	{
		if (Screen.dpi != 0) {
			// Fix user interaction constants for screens of varying pixel density
			float dpiScalingFactor = Screen.dpi / 150f;
			zoomSpeed /= dpiScalingFactor;
			panSpeed /= dpiScalingFactor;
			orbitDirectionLockThreshold /= dpiScalingFactor;
			zoomLockThreshold /= dpiScalingFactor;
			lineWidth *= dpiScalingFactor;
			circleRadius *= dpiScalingFactor;
			cpRotationThreshold /= dpiScalingFactor;
		}
		autoRotate = false;	
		camera.transform.LookAt(Vector3.zero);
		initialPosition = camera.transform.position;
		initialRotation = camera.transform.rotation;
		lineTexture = Resources.Load<Texture2D>("lineTexture");
	}

	// Update is called once per frame
	void Update()
	{
		selectedPiece = GameManager.CurrentGame.CurrentSelected;
		
		// There's a touch or more
		if (Input.touchCount == 1) {
			//if there is exactly one touch:
			//double-tap to start autorotate,
			//if no piece is selectd drag to orbit,
			//if a piece is selected drag to rotate
			handleOneFinger();
		} else if (Input.touchCount == 2) {
			//if there are exactly two touches, pinch to zoom in and out, drag to orbit
			handleTwoFingers();
		} else {
			// There are either no touches or there are too many to nicely handle,
			// so reset touchDuration and adjust for non-rotating state
			touchDuration = 0.0f;
			if (isRotatingPiece == true) {
				isRotatingPiece = false;
				// Resume auto-connection of pieces
				GameManager.CurrentGame.pauseAutoConnect = false;
				// Temporarily reparent the piece to be a child of the connection point
				rotatingPiece.SetParent(rotatingStr);
				rotatingConPoint.SetParent(rotatingPiece);
				rotatingConPoint = null;
				rotatingPiece = null;
				rotatingStr = null;
			}
		}
		
		if (autoRotate) {
			gameObject.transform.RotateAround(Vector3.zero, Vector3.up, autoRotateSpeed * Time.deltaTime);
		}

		boundCameraMovement();

		//keep camera pointed at origin
		camera.transform.LookAt(Vector3.zero);
	}
	
	void OnGUI()
	{
		if (isConnecting) {
			DrawCircle(startConnectingDragPosition, lineTexture);
			if (touch.position != startConnectingDragPosition) {
				DrawLine(startConnectingDragPosition, touch.position, lineTexture, 4);
			}
		}
	}
		
	private void handleOneFinger()
	{
		touchDuration += Time.deltaTime;
		touch = Input.GetTouch(0);
		if (touch.phase == TouchPhase.Began) {
			touchDeltaSinceLastRotation = Vector2.zero;
		}
		if (!isConnecting && touch.phase == TouchPhase.Moved) {
			// If the user moves the finger without holding it in place first, orbit the camera
			touchDeltaSinceLastRotation += touch.deltaPosition;
			autoRotate = false;
			if (selectedPiece == null || Utilities.IsConnectionPoint(selectedPiece)) {
				//If no piece is selected or the selected piece is a connection point, drag to orbit
				autoRotate = false;
				handleDragToOrbit(touch.deltaPosition, touch.deltaTime);
			} else if (!Utilities.IsConnectionPoint(selectedPiece)) {
				// Rotate the selected piece freely only if it's not a connection point and it's
				// not connected to another piece.  Otherwise, if it's connected in exactly one
				// place, rotate it about that connection point.  If it's connected in more than
				// one place, do not let the user manipulate the piece
				handlePieceManipulation();
			}
		} else if (touch.phase == TouchPhase.Ended && touchDuration < 0.2f) {
			//making sure to only check each touch once && it was a short touch/tap and not a dragging.
			StartCoroutine(singleOrDouble());
		} else if (!isConnecting && touch.phase == TouchPhase.Stationary && touchDuration > 0.25f) {
			handleTouchDragConnectionBegin();
		} else if (isConnecting && touch.phase == TouchPhase.Ended &&
			touch.position != startConnectingDragPosition) {
			handleTouchDragConnectionEnd();
		}
	}

	private void handleTwoFingers()
	{
		autoRotate = false;
		
		// Store both touches and their center.
		Touch touchZero = Input.GetTouch(0);
		Touch touchOne = Input.GetTouch(1);
		Vector2 centerPos = (touchZero.position + touchOne.position) / 2;
		
		// Find the position in the previous frame of each touch and their center.
		Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
		Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;
		Vector2 centerPrevPos = (touchOnePrevPos + touchZeroPrevPos) / 2;
		
		// Find the delta position of the center from last frame
		Vector2 centerDeltaPos = centerPos - centerPrevPos;
		
		// Find the magnitude of the vector (the distance) between the touches in each frame.
		float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;
		float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
		
		// Find the difference in the distances between each frame.
		float deltaMagnitudeDiff = touchDeltaMag - prevTouchDeltaMag;
		
		// Move the camera forward or backward if the user is pinching,
		// and rotate around the origin if the user is dragging with 2 fingers
		if (Mathf.Abs(deltaMagnitudeDiff) > zoomLockThreshold) {
			camera.transform.Translate(0, 0, zoomSpeed * deltaMagnitudeDiff);
			camera.transform.Translate(0, 0, zoomSpeed * deltaMagnitudeDiff * Time.deltaTime / ((touchZero.deltaTime + touchOne.deltaTime) / 2));
		}
		
		handleDragToOrbit(centerDeltaPos, (touchZero.deltaTime + touchOne.deltaTime) / 2f);
	}

	private void handleDragToOrbit(Vector2 deltaPosition, float deltaTime)
	{
		if (Mathf.Abs(deltaPosition.x) > orbitDirectionLockThreshold) {
			camera.transform.RotateAround(Vector3.zero, 
			                              new Vector3(0, deltaPosition.x, 0), 
			                              3f * panSpeed * deltaPosition.magnitude * Time.deltaTime / deltaTime);
		}
		
		if (Mathf.Abs(deltaPosition.y) > orbitDirectionLockThreshold) {
			camera.transform.RotateAround(Vector3.zero, 
			                              camera.transform.right * -1 * deltaPosition.y,
                                          3f * panSpeed * deltaPosition.magnitude * Time.deltaTime / deltaTime);
		}
	}

	private void handlePieceManipulation()
	{
		if (!GameManager.CurrentGame.allowManipulation) {
			return;
		}

		List<Connection> allConnections = selectedPiece.GetComponent<PieceBehavior>().Piece.Connections;
		int numConnections = allConnections.Count;
//		Debug.Log("This " + selectedPiece.GetComponent<PieceBehavior>().Piece.Name + 
//				  " has " + allConnections.Count + " connections");
		if (selectedPiece.GetComponent<PieceBehavior>().Piece.IsDisplayPiece) {
			// If the selected piece is the display piece, rotate it freely
			handleFreeObjectRotation();
		} else if (numConnections == 1) {
			// If the piece is connected to exactly one other piece,
			// rotate it about that connection point
			handleConnectedObjectRotation(allConnections [0]);
		} else if (numConnections > 1) {
			// User can't manipulate a piece with more than 1 connection
			StartCoroutine(ShakeCamera());
		}
	}

	private void handleFreeObjectRotation()
	{
//		touchDuration += Time.deltaTime;
		touch = Input.GetTouch(0);
		if (touch.phase == TouchPhase.Moved) {
			if (Mathf.Abs(touch.deltaPosition.x) > orbitDirectionLockThreshold) {
//				selectedPiece.transform.Rotate(camera.transform.up, -2f * panSpeed * touch.deltaPosition.x, Space.World);
				selectedPiece.transform.Rotate(camera.transform.up, 
				                               -2f * panSpeed * touch.deltaPosition.x * Time.deltaTime / touch.deltaTime, 
				                               Space.World);
			}
			
			if (Mathf.Abs(touch.deltaPosition.y) > orbitDirectionLockThreshold) {
//				selectedPiece.transform.Rotate(camera.transform.right, 2f * panSpeed * touch.deltaPosition.y, Space.World);
				selectedPiece.transform.Rotate(camera.transform.right, 
				                               2f * panSpeed * touch.deltaPosition.y * Time.deltaTime / touch.deltaTime, 
				                               Space.World);
			}
		}
	}

	private void handleConnectedObjectRotation(Connection connection)
	{
		if (Mathf.Abs(touchDeltaSinceLastRotation.x) > cpRotationThreshold) {
			// Perform bookkeeping for start of rotation
			if (isRotatingPiece == false) {
				isRotatingPiece = true;
				// Pause auto-connection of pieces
				GameManager.CurrentGame.pauseAutoConnect = true;
				// Temporarily reparent the piece to be a child of the connection point
				rotatingConPoint = connection.ParentConnectionPoint.GameObject.transform;
				rotatingPiece = rotatingConPoint.parent;
				rotatingStr = rotatingPiece.parent;

				rotatingConPoint.SetParent(rotatingStr);
				rotatingPiece.SetParent(rotatingConPoint);
			}
//			Vector3 rotationAxis = connection.ParentConnectionPoint.Normal * touchDeltaSinceLastRotation.x;
			Vector3 rotationAxis = Vector3.forward * touchDeltaSinceLastRotation.x;
			rotationAxis.Normalize();
			StartCoroutine(LerpRotateOnAxis(rotatingConPoint.gameObject, rotationAxis, -45));
			touchDeltaSinceLastRotation = Vector2.zero;
		}
	}

	private void handleTouchDragConnectionBegin()
	{
		if (!GameManager.CurrentGame.allowManipulation) {
			return;
		}
		
		GameObject selectedObject = Utilities.GameObjectAtDisplayCase(touch.position) ?? 
			Utilities.GameObjectAt(touch.position);
		if (selectedObject != null &&
			(Utilities.IsConnectionPoint(selectedObject) || 
			GameManager.CurrentGame.CurrentModel.Pieces.Count == 0)) {
			isConnecting = true;
			startConnectingDragPosition = touch.position;
			handleObjectSelection(touch.position);
		}
	}

	private void handleTouchDragConnectionEnd()
	{
		if (!GameManager.CurrentGame.allowManipulation) {
			return;
		}

		isConnecting = false;
		handleObjectSelection(touch.position);
	}
	
	public void boundCameraMovement()
	{
		if (bound) {
			// Bound camera's height
			Vector3 cameraPos = camera.transform.position;
			cameraPos.Set(cameraPos.x, Mathf.Max(floor, cameraPos.y), cameraPos.z);
			camera.transform.position = cameraPos;

			// Bound camera's pitch
			float cameraPitch = camera.transform.rotation.eulerAngles.x;
			if (cameraPitch > 180) {
				cameraPitch -= 360;
			}
			
			if (cameraPitch >= camRotationMax) {
				camera.transform.RotateAround(Vector3.zero, camera.transform.right,
				                              camRotationMax - cameraPitch);
			} else if (cameraPitch <= -camRotationMax) {
				camera.transform.RotateAround(Vector3.zero, camera.transform.right,
				                              cameraPitch + camRotationMax);
			}

			// Bound camera's distance from the origin
			if (camera.transform.position.magnitude <= zoomMin) {
				camera.transform.Translate(0, 0, camera.transform.position.magnitude - zoomMin);
			}
		}
	}

	public void ToggleAutoRotate()
	{
		autoRotate = !autoRotate;
		if (autoRotate) {
			Refocus();
		}
	}
	
	public void Refocus()
	{
		StartCoroutine(LerpToPosition(initialPosition, initialRotation));
	}

	public void handleObjectSelection(Vector2 position)
	{
		if (!GameManager.CurrentGame.allowManipulation) {
			return;
		}

		//if nothing clicked or tapped in display case, try main screen
		GameObject go = Utilities.GameObjectAtDisplayCase(position) ?? Utilities.GameObjectAt(position);
		
		if (go != null) {
			if (object.ReferenceEquals(GameManager.CurrentGame.CurrentSelected, go)) {
				//Unselect if reselected
				GameManager.CurrentGame.CurrentSelected = null;
			} else if (!Utilities.IsConnectionPoint(GameManager.CurrentGame.CurrentSelected)) {
				GameManager.CurrentGame.CurrentSelected = go;
			} else if (Utilities.IsConnectionPoint(GameManager.CurrentGame.CurrentSelected) 
				&& Utilities.IsConnectionPoint(go)) {
				ConnectionPointBehavior cpb = 
					GameManager.CurrentGame.CurrentSelected.GetComponent<ConnectionPointBehavior>();
				ConnectionPointBehavior cpb2 = go.GetComponent<ConnectionPointBehavior>();
				
				Piece parent = cpb2.Piece;
				Piece child = cpb.Piece;
				ConnectionPoint parentC = cpb2.ConnectionPoint;
				ConnectionPoint childC = cpb.ConnectionPoint;
				
				//Can't connect same gender pieces
				if (parent.IsMale != child.IsMale) {
					//Swap, so that the display piece is connected to the existing piece (not vice versa)
					if (parent.IsDisplayPiece) {
						Utilities.Swap(ref parent, ref child);
						Utilities.Swap(ref parentC, ref childC);
					}
					
					//Can't connect to itself
					if (!object.ReferenceEquals(child, parent)) {
						bool shouldCopy = child.IsDisplayPiece;
						//Create a copy of the display piece
						if (shouldCopy) {
							int connectionPointIndex = child.ConnectionPoints.IndexOf(childC);
							child = child.Copy();
							childC = child.ConnectionPoints [connectionPointIndex];
							GameManager.CurrentGame.CurrentModel.Add(child);
							if (GameManager.CurrentGame.Type == GameType.DI || GameManager.CurrentGame.Type == GameType.WIDI && GameManager.CurrentGame.inDoItPhase) 
								DisplayCase.takeFromToolbox(child.ColorAndName());
						}

						// ConnectTo checks if pieces are already connected, etc. and handles it
						child.ConnectTo(childC, parent, parentC, !shouldCopy);
						//deselect on connect
						GameManager.CurrentGame.CurrentSelected = null;
						
						Debug.Log("Connected pieces.");
					} else {
						GameManager.CurrentGame.CurrentSelected = null;
						StartCoroutine(ShakeCamera());
					}
				} else {
					GameManager.CurrentGame.CurrentSelected = null;
					StartCoroutine(ShakeCamera());
				}
			} else {
				GameManager.CurrentGame.CurrentSelected = null;
				Debug.Log("Something strange in handleObjectSelection");
			}
			// Add the first piece to the model if there are no pieces currently in the workspace
		} else if (GameManager.CurrentGame.CurrentModel.Pieces.Count == 0 && GameManager.CurrentGame.CurrentSelected != null) {
			Piece newPiece = null;
			if (Utilities.IsConnectionPoint(GameManager.CurrentGame.CurrentSelected)) {
				newPiece = GameManager.CurrentGame.CurrentSelected.GetComponent<ConnectionPointBehavior>().Piece.Copy();
			} else if (GameManager.CurrentGame.CurrentSelected.GetComponent<PieceBehavior>() != null) {
				newPiece = GameManager.CurrentGame.CurrentSelected.GetComponent<PieceBehavior>().Piece.Copy();
			}
			if (newPiece != null) {
				newPiece.GameObject.transform.position = Vector3.zero;
				GameManager.CurrentGame.CurrentModel.Add(newPiece);
				DisplayCase.takeFromToolbox(newPiece.ColorAndName());
			}
			GameManager.CurrentGame.CurrentSelected = null;
		} else if (EventSystem.current.currentSelectedGameObject != null) {
			// Do nothing because the user tapped or clicked on a gui object
			return;
		} else {
			GameManager.CurrentGame.CurrentSelected = null;
		}
	}

	public void DrawLine(Vector2 lineStart, Vector2 lineEnd, Texture2D texture, int thickness)
	{
		Matrix4x4 matrixBackup = GUI.matrix;
		
		lineStart.y = Screen.height - lineStart.y;
		lineEnd.y = Screen.height - lineEnd.y;
		Vector2 lineVector = lineEnd - lineStart;
		float angle = Mathf.Rad2Deg * Mathf.Atan(lineVector.y / lineVector.x);
		if (lineVector.x < 0) {
			angle += 180;
		}
		
		if (thickness < 1) {
			thickness = 1;
		}
		
		// The center of the line will always be at the center
		// regardless of the thickness.
		int thicknessOffset = (int)Mathf.Ceil(thickness / 2);
		
		Rect drawingRect = new Rect(lineStart.x,
		                            lineStart.y - thicknessOffset,
		                            Vector2.Distance(lineStart, lineEnd),
		                            (float)thickness);
		GUIUtility.RotateAroundPivot(angle,
		                             lineStart);
		GUI.BeginGroup(drawingRect);
		{
			int drawingRectWidth = Mathf.RoundToInt(drawingRect.width);
			int drawingRectHeight = Mathf.RoundToInt(drawingRect.height);
			
			for (int y = 0; y < drawingRectHeight; y += texture.height) {
				for (int x = 0; x < drawingRectWidth; x += texture.width) {
					GUI.DrawTexture(new Rect(x,
					                         y,
					                         texture.width,
					                         texture.height),
					                texture);
				}
			}
		}
		GUI.EndGroup();
		
		GUI.matrix = matrixBackup;
	}

	private void DrawCircle(Vector2 center, Texture2D texture)
	{
		int sides = 36;
		List<Vector2> circleVertices = new List<Vector2>();
		for (int i = 0; i < sides; i++) {
			float angle = (360.0f / (float)sides) * (float)i;
			Vector2 edgePoint = new Vector2(Mathf.Cos(Mathf.Deg2Rad * angle), 
			                                Mathf.Sin(Mathf.Deg2Rad * angle));
			circleVertices.Add(center + ((float)circleRadius * edgePoint));
		}
		for (int i = 0; i < sides; i++) {
			DrawLine(circleVertices [i], circleVertices [(i + 1) % sides], texture, 4);
		}
	}

	private IEnumerator singleOrDouble()
	{
		yield return new WaitForSeconds(0.3f);
		if (touch.tapCount == 1) {
			//			Debug.Log("Single Tap");
			handleObjectSelection(touch.position);
		} else if (touch.tapCount == 2) {
			//			Debug.Log("Double Tap");
			/*
			if (Utilities.GameObjectAt(touch.position) != null) {
				ToggleAutoRotate();
			} else {
				ToggleAutoRotate();
			}
			*/
			//this coroutine has been called twice. We should stop the next one here otherwise we get two double tap
			StopCoroutine(singleOrDouble());
		}
	}
	
	private IEnumerator LerpToPosition(Vector3 newPosition, Quaternion newRotation)
	{    
		float t = 0.0f;
		Vector3 startingPos = transform.position;
		Quaternion startingRot = transform.rotation;
		// if the transform is close to the new location or oriented similarly, make the transition quicker
		float posLerpSpeed =
			Mathf.Min(posLerpCoefficient * (transform.position - newPosition).magnitude, maxLerpDuration);
		float rotLerpSpeed =
			Mathf.Min(rotLerpCoefficient * (transform.rotation.eulerAngles - newRotation.eulerAngles).magnitude,
			          maxLerpDuration);
		float lerpSpeed = Mathf.Max(posLerpSpeed, rotLerpSpeed);
		
		while (t < 1.0f) {
			t += Time.deltaTime * (Time.timeScale / lerpSpeed);
			
			transform.position = Vector3.Lerp(startingPos, newPosition, t);
			transform.rotation = Quaternion.Lerp(startingRot, newRotation, t);
			yield return 0;
		}    
	}

	// Rotates in Local Space!!!
	private IEnumerator LerpRotateOnAxis(GameObject obj, Vector3 axis, float angle)
	{
		float t = 0.0f;
		
		Quaternion startingRot = obj.transform.rotation;
		Quaternion targetRot = startingRot * Quaternion.AngleAxis(angle, axis);

		float lerpSpeed =
			Mathf.Min(rotLerpCoefficient * Mathf.Abs(angle) * 0.1f,
			          maxLerpDuration);
		
		while (t < 1.0f) {
			t += Time.deltaTime * (Time.timeScale / lerpSpeed);
			
			obj.transform.rotation = Quaternion.Lerp(startingRot, targetRot, t);

			yield return 0;
		}    
	}

	private IEnumerator LerpRotateOnAxisAboutPoint(GameObject obj, Vector3 axis, Vector3 center, int angle)
	{
		float t = 0.0f;
		
		Quaternion startingRot = obj.transform.rotation;
		Quaternion targetRot = startingRot * Quaternion.AngleAxis(angle, axis);

		Vector3 startingPos = obj.transform.position;
		Vector3 targetPos = targetRot * (startingPos - center) + center;

		Debug.Log("Rotating about " + center + " on axis " + axis);

		float lerpSpeed =
			Mathf.Min(rotLerpCoefficient * Mathf.Abs(angle) * 0.1f,
			          maxLerpDuration);
		
		while (t < 1.0f) {
			t += Time.deltaTime * (Time.timeScale / lerpSpeed);

			obj.transform.rotation = Quaternion.Lerp(startingRot, targetRot, t);
			obj.transform.position = Vector3.Lerp(startingPos, targetPos, t);

			yield return 0;
		}
	}
	
	// Used to indicate an illegal move by the user
	public IEnumerator ShakeCamera()
	{
		originalPosition = transform.position;
		originalRotation = transform.rotation;
		
		shakeIntensity = 0.1f;
		shakeDecay = 0.025f;

		while (shakeIntensity > 0) {
			transform.position = originalPosition + Random.insideUnitSphere * shakeIntensity;
			transform.rotation = new Quaternion(originalRotation.x + Random.Range(-shakeIntensity, shakeIntensity) * .2f,
			                                    originalRotation.y + Random.Range(-shakeIntensity, shakeIntensity) * .2f,
			                                    originalRotation.z + Random.Range(-shakeIntensity, shakeIntensity) * .2f,
			                                    originalRotation.w + Random.Range(-shakeIntensity, shakeIntensity) * .2f);
			
			shakeIntensity -= shakeDecay;
			yield return 0;
		}
		transform.position = originalPosition;
		transform.rotation = originalRotation;

		yield return 0;
	}	
}
