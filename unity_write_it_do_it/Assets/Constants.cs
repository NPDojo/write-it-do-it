using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace WriteItDoIt
{
	public class Constants : MonoBehaviour {

		// *** Spatial dimensions
		public static readonly float lengthUnit = .75f;
		public static readonly float rodWidthFactor = 0.3f;
		public static readonly float rodLengthFactor = 3.0f;
		public static readonly float floorHeight = -3f;

		// *** Colors
		public static readonly Color scolyGreen = new Color (133f/255f, 195f/255f, 76f/255f); // Used for UI elements
		public static readonly Color scolyBlue = new Color (30f/255f, 123f/255f, 184f/255f); // Used for UI elements
		public static readonly Color purple = new Color(163f / 255f, 90f / 255f, 184f / 255f);
		public static readonly Color yellow = new Color(230f / 255f, 230f / 255f, 0f);
		public static readonly Color gray = new Color(.95f,.95f,.95f);
		public static readonly Color orange = new Color(250/255f,104f/255f,1f/255f);

		public static readonly Color[] pieceColors = {scolyGreen, scolyBlue, purple, yellow, gray, orange};
		public static readonly string[] pieceColorNames = {"Green", "Blue", "Purple", "Yellow", "Gray", "Orange"};
		public static readonly Color ConnectionPointColorDefault = new Color(1f, 1f, 1f, 0.7f); // was Color.gray
		public static readonly Color ConnectionPointColorSelected = new Color(0f, 1f, 1f, 0.7f);  // was Color.cyan
		public static readonly Color PieceColorSelected = Constants.ConnectionPointColorSelected; //new Color(255f/255f, 255f/255f, 20f/255f); // Yellow
		public static readonly Color OverlappingPieceColor = Color.red;

		// *** Times
		public static readonly float defaultWITime = 25f * 60f;
		public static readonly float defaultDITime = 20f * 60f;
		public static readonly float ConnectingLerpTime = 1f;
		public static readonly float autoConnectTime = 1f;

		// *** Tags
		public static readonly string PieceTag = "Piece";
		public static readonly string ConnectionPointTag = "ConnectionPoint";
		public static readonly string StructureTag = "Structure";
		public static readonly string ModelTag = "Model";
		public static readonly string ErrorTag = "ErrorMessage";
		public static readonly string ScreenTitleTag = "ScreenTitle";

		// *** Preferences
		public static readonly string PrefWITime = "WITime";
		public static readonly string PrefDITime = "DITime";
		
		// *** Other
		public static readonly int DropDownPageSize = 5; // Number of items in dynamic dropdown page

		// *** Files and/or serialization
		public static readonly string resultsLogName = "widiUserScores.log";
		public static readonly int SerializedPieceSize = 30; //30 bytes
		
		// Application.persistentDataPath can't be called from initializing thread; the wrapper works around this
		private static string fileDir = null;
		public static string fileDirectory
		{
			get {if(fileDir==null) fileDir = Application.persistentDataPath; return fileDir;}
			// No set; it's a constant
		}

		// The TextAsset objects representing the built in models
		private static UnityEngine.Object[] builtins = null;
		public static UnityEngine.Object[] builtinModels
		{
			get {
				if (builtins == null) {
					builtins = Resources.LoadAll ("DefaultModels");
				}
				return builtins;
			}
		}

		public static readonly Type[] PieceTypeMap;
		//Static block equivelant. Basically only executed on class-load
		static Constants() 
		{
			PieceTypeMap = new Type[] {
				typeof(L45RodPiece),
				typeof(LPlusRodPiece),
				typeof(LRodPiece),
				typeof(OctahedronPiece),
				typeof(PyramidPiece),
				typeof(SmallCubePiece),
				typeof(SpokePiece),
				typeof(StarRodPiece),
				typeof(StraightRodPiece),
				typeof(TRodPiece),
				typeof(X90RodPiece),
				typeof(XPlusRodPiece),
				typeof(XRodPiece),
				typeof(YRodPiece)
			};
		}
	}
}