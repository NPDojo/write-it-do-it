﻿using UnityEngine;
using System.Collections;
using WriteItDoIt;

public class CameraAutofocus : MonoBehaviour {
	float cameraDistance = 5.0f;
	float distanceToObject;
	Piece piece;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		GameObject displayCase = GameObject.Find("Display Case");
		if (displayCase.activeSelf){
			RaycastHit hit;
			if (Physics.Raycast(transform.position, Vector3.forward, out hit, Mathf.Infinity)) {
				distanceToObject = hit.distance;
			}
			//DisplayCase dcScript = displayCase.GetComponent<DisplayCase>();
			piece = DisplayCase.Piece;
			float distanceDifference = distanceToObject - cameraDistance;
			if(piece != null && Mathf.Abs( distanceDifference) > 0.1f) {
				//Debug.Log(distanceToObject + " " + transform.position.z);
				transform.position =  new Vector3(transform.position.x, transform.position.y, transform.position.z + (distanceDifference));
				//Debug.Log(transform.position.z);				
			}
		}
	}
}
