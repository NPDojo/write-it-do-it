﻿using UnityEngine;
using System.Collections;

public class CameraTestMovement : MonoBehaviour
{
	public bool AutoRotate {
		get;
		private set;
	}
	
	// Control whether the camera is bound above some y position
	public bool bound;
	public float floor;
	
	public static bool didTapButton;
	
	public int autoRotateSpeed = 20;
	private static Vector3 initialPosition;
	private static Quaternion initialRotation;
	private float zoomSpeed = 0.05f;
	private float panSpeed = 0.07f;
	private float posLerpCoefficient = 0.07f;
	private float rotLerpCoefficient = 0.01f;
	private float maxLerpDuration = 0.7f;
	private float orbitDirectionLockThreshold = 2f;
	private float touchDuration;
	private Touch touch;
	private float zoomLockThreshold = 2f;
	
	// Use this for initialization
	void Start()
	{
		AutoRotate = false;	
		initialPosition = gameObject.transform.position;
		initialRotation = gameObject.transform.rotation;
	}
	
	// Update is called once per frame
	void Update()
	{
		//keep camera pointed at origin
		camera.transform.LookAt (Vector3.zero);

		// Touch input control
		if (Input.touchCount == 1) { //if there is exactly one touch, drag to orbit, double-tap to start autorotate
			touchDuration += Time.deltaTime;
			touch = Input.GetTouch(0);
			if (touch.phase == TouchPhase.Moved) {
				/*AutoRotate = false;
				if (Mathf.Abs(touch.deltaPosition.x) > orbitDirectionLockThreshold) {
					camera.transform.RotateAround(Vector3.zero, 
					                              new Vector3(0, touch.deltaPosition.x, 0), 
					                              3f * panSpeed * touch.deltaPosition.magnitude);
				}
				if (Mathf.Abs(touch.deltaPosition.y) > orbitDirectionLockThreshold) {
					camera.transform.RotateAround(Vector3.zero,
					                              camera.transform.right * -1 *touch.deltaPosition.y,
					                              3f * panSpeed * touch.deltaPosition.magnitude);
				}*/
			} else if (touch.phase == TouchPhase.Ended && touchDuration < 0.2f) {
				//making sure it only check the touch once && it was a short touch/tap and not a dragging.
				StartCoroutine("singleOrDouble");
			}
		} else if (Input.touchCount == 2) {
			//if there are exactly two touches, pinch to zoom in and out, drag to orbit
			
			AutoRotate = false;
			
			// Store both touches and their center.
			Touch touchZero = Input.GetTouch(0);
			Touch touchOne = Input.GetTouch(1);
			Vector2 centerPos = (touchOne.position + touchZero.position) / 2;
			
			// Find the position in the previous frame of each touch and their center.
			Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
			Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;
			Vector2 centerPrevPos = (touchOnePrevPos + touchZeroPrevPos) / 2;
			
			// Find the magnitude of the vector (the distance) between the touches in each frame.
			float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
			float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;
			
			// Find the difference in the distances between each frame.
			float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;
			
			// Move the camera forward or backward if the user is pinching,
			// and rotate around the origin if the user is dragging with 2 fingers
			if (Mathf.Abs(deltaMagnitudeDiff) > zoomLockThreshold) {
				camera.transform.Translate(0, 0, -1f * zoomSpeed * deltaMagnitudeDiff);
			}
			if (Mathf.Abs(centerPos.x - centerPrevPos.x) > orbitDirectionLockThreshold) {
				camera.transform.RotateAround(Vector3.zero, 
				                              new Vector3(0, centerPos.x - centerPrevPos.x, 0), 
				                              3f * panSpeed * (centerPos - centerPrevPos).magnitude);
			}
			if (Mathf.Abs(centerPos.y - centerPrevPos.y) > orbitDirectionLockThreshold) {
				camera.transform.RotateAround(Vector3.zero,
				                              camera.transform.right * (centerPrevPos.y - centerPos.y),
				                              3f * panSpeed * (centerPos - centerPrevPos).magnitude);
			}
		} else {
			touchDuration = 0.0f;
		}
		
		// Keyboard input control
		if (Input.GetKeyDown(KeyCode.Space)) {
			//AutoRotate = !AutoRotate;
			if (AutoRotate) {	
				//gameObject.transform.position = initialPosition;
				//gameObject.transform.rotation = initialRotation;
			}
		} else if (Input.anyKeyDown) {
			//AutoRotate = false;
		}
		
		if (Input.GetKey(KeyCode.A)) {
			gameObject.transform.Translate(Vector3.left * 5 * Time.deltaTime);
		}
		if (Input.GetKey(KeyCode.D)) {
			gameObject.transform.Translate(Vector3.right * 5 * Time.deltaTime);
		}
		if (Input.GetKey(KeyCode.W)) {
			gameObject.transform.Translate(Vector3.up * 5 * Time.deltaTime);
		}
		if (Input.GetKey(KeyCode.S)) {
			gameObject.transform.Translate(Vector3.down * 5 * Time.deltaTime);
		}
		if (Input.GetKey(KeyCode.Q)) {
			gameObject.transform.Translate(Vector3.back * 5 * Time.deltaTime);
		}
		if (Input.GetKey(KeyCode.E)) {
			gameObject.transform.Translate(Vector3.forward * 5 * Time.deltaTime);
		}	
		if (Input.GetKey(KeyCode.UpArrow)) {
			gameObject.transform.Rotate(Vector3.left * 20 * Time.deltaTime);
		}
		if (Input.GetKey(KeyCode.DownArrow)) {
			gameObject.transform.Rotate(Vector3.right * 20 * Time.deltaTime);
		}
		if (Input.GetKey(KeyCode.LeftArrow)) {
			gameObject.transform.Rotate(Vector3.down * 20 * Time.deltaTime, Space.World);
		}
		if (Input.GetKey(KeyCode.RightArrow)) {
			gameObject.transform.Rotate(Vector3.up * 20 * Time.deltaTime, Space.World);
		}
		
		//Debug.Log (AutoRotate);
		if (AutoRotate) {
			gameObject.transform.RotateAround(Vector3.zero, Vector3.up, autoRotateSpeed * Time.deltaTime);
		}
		
		// Ensure camera does not move below floor
		if (bound) {
			Vector3 pos = gameObject.transform.position;
			pos.Set (pos.x, Mathf.Max (floor, pos.y), pos.z);
			gameObject.transform.position = pos;
		}
	}
	
	IEnumerator singleOrDouble()
	{
		yield return new WaitForSeconds(0.3f);
		if (touch.tapCount == 1 || didTapButton) {
			Debug.Log("Single Tap");
		} else if (touch.tapCount == 2) {
			//this coroutine has been called twice. We should stop the next one here otherwise we get two double tap
			StopCoroutine("singleOrDouble");
			ToggleAutoRotate();
			Debug.Log("Double Tap");
		}
	}
	
	public void ToggleAutoRotate()
	{
		AutoRotate = !AutoRotate;
		if (AutoRotate) {
			Refocus();
		}
	}
	
	public void Refocus()
	{
		StartCoroutine(LerpToPosition(initialPosition, initialRotation));
	}
	
	IEnumerator LerpToPosition(Vector3 newPosition, Quaternion newRotation)
	{    
		float t = 0.0f;
		Vector3 startingPos = transform.position;
		Quaternion startingRot = transform.rotation;
		// if the transform is close to the initial location or oriented similarly, make the transition quicker
		float posLerpSpeed =
			Mathf.Min(posLerpCoefficient * (transform.position - newPosition).magnitude, maxLerpDuration);
		float rotLerpSpeed =
			Mathf.Min(rotLerpCoefficient * (transform.rotation.eulerAngles - newRotation.eulerAngles).magnitude,
			          maxLerpDuration);
		float lerpSpeed = Mathf.Max(posLerpSpeed, rotLerpSpeed);
		
		while (t < 1.0f) {
			t += Time.deltaTime * (Time.timeScale / lerpSpeed);
			
			transform.position = Vector3.Lerp(startingPos, newPosition, t);
			transform.rotation = Quaternion.Lerp(startingRot, newRotation, t);
			yield return 0;
		}    
	}
}
