﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

// For use with generated error messages
public class Error : MonoBehaviour {

	public void SetMessage(string m) {
		gameObject.transform.GetChild (0).GetChild (0).GetChild (0).gameObject.GetComponent<Text> ().text = m;
	}

	public void Deactivate() {
		gameObject.transform.GetChild (0).gameObject.SetActive (false);
	}
}
