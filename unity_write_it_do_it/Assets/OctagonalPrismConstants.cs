﻿using UnityEngine;
using System.Collections;
using WriteItDoIt;

public static class OctagonalPrismConstants : object {

	private static float xySizeFactor = .2f;
	private static float zSizeFactor = 0.1f;
	private static float femaleBorderWidth = .05f;
	private static float pieceWidth = Constants.lengthUnit * xySizeFactor;
	public static float pieceThickness = Constants.lengthUnit * zSizeFactor;
	private static float halfSideLength = pieceWidth * Mathf.Tan ((22.5f*Mathf.PI)/180f);
	private static float femalePieceWidth = pieceWidth + femaleBorderWidth;
	private static float femaleHalfSideLength = femalePieceWidth * Mathf.Tan (Mathf.PI / 8f);

	// front face
	private static Vector3 vxA = new Vector3(halfSideLength, pieceWidth, -pieceThickness);
	private static Vector3 vxB = new Vector3(pieceWidth, halfSideLength, -pieceThickness);
	private static Vector3 vxC = new Vector3(pieceWidth, -halfSideLength, -pieceThickness);
	private static Vector3 vxD = new Vector3(halfSideLength, -pieceWidth, -pieceThickness);
	private static Vector3 vxE = new Vector3(-halfSideLength, -pieceWidth, -pieceThickness);
	private static Vector3 vxF = new Vector3(-pieceWidth, -halfSideLength, -pieceThickness);
	private static Vector3 vxG = new Vector3(-pieceWidth, halfSideLength, -pieceThickness);
	private static Vector3 vxH = new Vector3(-halfSideLength, pieceWidth, -pieceThickness);
	
	// back face
	private static Vector3 vxI = new Vector3(halfSideLength, pieceWidth, pieceThickness);
	private static Vector3 vxJ = new Vector3(pieceWidth, halfSideLength, pieceThickness);
	private static Vector3 vxK = new Vector3(pieceWidth, -halfSideLength, pieceThickness);
	private static Vector3 vxL = new Vector3(halfSideLength, -pieceWidth, pieceThickness);
	private static Vector3 vxM = new Vector3(-halfSideLength, -pieceWidth, pieceThickness);
	private static Vector3 vxN = new Vector3(-pieceWidth, -halfSideLength, pieceThickness);
	private static Vector3 vxO = new Vector3(-pieceWidth, halfSideLength, pieceThickness);
	private static Vector3 vxP = new Vector3(-halfSideLength, pieceWidth, pieceThickness);

	// female outside front
	private static Vector3 vxAf = new Vector3(femaleHalfSideLength, femalePieceWidth, -pieceThickness);
	private static Vector3 vxBf = new Vector3(femalePieceWidth, femaleHalfSideLength, -pieceThickness);
	private static Vector3 vxCf = new Vector3(femalePieceWidth, -femaleHalfSideLength, -pieceThickness);
	private static Vector3 vxDf = new Vector3(femaleHalfSideLength, -femalePieceWidth, -pieceThickness);
	private static Vector3 vxEf = new Vector3(-femaleHalfSideLength, -femalePieceWidth, -pieceThickness);
	private static Vector3 vxFf = new Vector3(-femalePieceWidth, -femaleHalfSideLength, -pieceThickness);
	private static Vector3 vxGf = new Vector3(-femalePieceWidth, femaleHalfSideLength, -pieceThickness);
	private static Vector3 vxHf = new Vector3(-femaleHalfSideLength, femalePieceWidth, -pieceThickness);
	
	// female outside back
	private static Vector3 vxIf = new Vector3(femaleHalfSideLength, femalePieceWidth, pieceThickness);
	private static Vector3 vxJf = new Vector3(femalePieceWidth, femaleHalfSideLength, pieceThickness);
	private static Vector3 vxKf = new Vector3(femalePieceWidth, -femaleHalfSideLength, pieceThickness);
	private static Vector3 vxLf = new Vector3(femaleHalfSideLength, -femalePieceWidth, pieceThickness);
	private static Vector3 vxMf = new Vector3(-femaleHalfSideLength, -femalePieceWidth, pieceThickness);
	private static Vector3 vxNf = new Vector3(-femalePieceWidth, -femaleHalfSideLength, pieceThickness);
	private static Vector3 vxOf = new Vector3(-femalePieceWidth, femaleHalfSideLength, pieceThickness);
	private static Vector3 vxPf = new Vector3(-femaleHalfSideLength, femalePieceWidth, pieceThickness);
	
	public static Vector3[] maleVertices = 
	{
		vxA, vxB, vxC, vxD, vxE, vxF, vxG, vxH, // front face
		vxP, vxO, vxN, vxM, vxL, vxK, vxJ, vxI, // back face
		vxA, vxH, vxP, vxI, // 12:00
		vxB, vxA, vxI, vxJ, // 1:30
		vxC, vxB, vxJ, vxK, // 3:00
		vxD, vxC, vxK, vxL, // 4:30
		vxE, vxD, vxL, vxM, // 6:00
		vxF, vxE, vxM, vxN, // 7:30
		vxG, vxF, vxN, vxO, // 9:00
		vxH, vxG, vxO, vxP, // 10:30
	};
	
	public static int[] maleTriangles =
	{
		// front face
		0, 1, 2,
		0, 2, 3,
		0, 3, 4,
		0, 4, 5,
		0, 5, 6,
		0, 6, 7,
		
		// back face
		8, 9, 10,
		8, 10, 11,
		8, 11, 12,
		8, 12, 13,
		8, 13, 14,
		8, 14, 15,
		
		// 12:00
		16, 17, 18,
		16, 18, 19,
		
		// 1:30
		20, 21, 22,
		20, 22, 23,
		
		// 3:00
		24, 25, 26,
		24, 26, 27,
		
		// 4:30
		28, 29, 30,
		28, 30, 31,
		
		// 6:00
		32, 33, 34,
		32, 34, 35,
		
		// 7:30
		36, 37, 38,
		36, 38, 39,
		
		// 9:00
		40, 41, 42,
		40, 42, 43,
		
		// 10:30
		44, 45, 46,
		44, 46, 47
	};

	public static Vector3[] femaleVertices = new Vector3[]
	{
		vxA, vxB, vxC, vxD, vxE, vxF, vxG, vxH, vxAf, vxBf, vxCf, vxDf, vxEf, vxFf, vxGf, vxHf, // front face
		vxP, vxO, vxN, vxM, vxL, vxK, vxJ, vxI, vxPf, vxOf, vxNf, vxMf, vxLf, vxKf, vxJf, vxIf, // back face

		vxA, vxH, vxP, vxI, // 12:00 inside
		vxB, vxA, vxI, vxJ, // 1:30
		vxC, vxB, vxJ, vxK, // 3:00
		vxD, vxC, vxK, vxL, // 4:30
		vxE, vxD, vxL, vxM, // 6:00
		vxF, vxE, vxM, vxN, // 7:30
		vxG, vxF, vxN, vxO, // 9:00
		vxH, vxG, vxO, vxP, // 10:30

		vxAf, vxHf, vxPf, vxIf, // 12:00 outside
		vxBf, vxAf, vxIf, vxJf, // 1:30
		vxCf, vxBf, vxJf, vxKf, // 3:00
		vxDf, vxCf, vxKf, vxLf, // 4:30
		vxEf, vxDf, vxLf, vxMf, // 6:00
		vxFf, vxEf, vxMf, vxNf, // 7:30
		vxGf, vxFf, vxNf, vxOf, // 9:00
		vxHf, vxGf, vxOf, vxPf, // 10:30
	};
	
	public static int[] femaleTriangles = new int[]
	{
		// front face
		15, 8, 7,
		7, 8, 0,
		8, 9, 0,
		0, 9, 1,
		1, 9, 2,
		2, 9, 10,
		2, 10, 3,
		3, 10, 11,
		4, 3, 11,
		4, 11, 12,
		5, 4, 12,
		5, 12, 13,
		14, 6, 5,
		14, 5, 13,
		15, 7, 6,
		15, 6, 14,

		// back face
		31, 24, 23,
		23, 24, 16,
		30, 31, 22,
		22, 31, 23,
		29, 30, 21,
		21, 30, 22,
		28, 29, 20,
		20, 29, 21,
		27, 28, 19,
		19, 28, 20,
		26, 27, 18,
		18, 27, 19,
		25, 26, 17,
		17, 26, 18,
		24, 25, 16,
		16, 25, 17,

		// insides
		32, 34, 33,
		48, 50, 49,
		32, 35, 34,
		48, 51, 50,
		36, 38, 37,
		52, 54, 53,
		36, 39, 38,
		52, 55, 54,
		40, 42, 41,
		56, 58, 57,
		40, 43, 42,
		56, 59, 58,
		44, 46, 45,
		60, 62, 61,
		44, 47, 46,
		60, 63, 62,

		// outsides
		64, 65, 66,
		80, 81, 82,
		64, 66, 67,
		80, 82, 83,
		68, 69, 70,
		84, 85, 86,
		68, 70, 71,
		84, 86, 87,
		72, 73, 74,
		88, 89, 90,
		72, 74, 75,
		88, 90, 91,
		76, 77, 78,
		92, 93, 94,
		76, 78, 79,
		92, 94, 95
	};

}
