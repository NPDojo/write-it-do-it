﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/* This is a MonoBehaviour (component) extending InputField
 * It is intended for use in Text Fields that aren't meant to be editable, but instead,
 * can be easily copied from (like fields containing generated model strings).
 * To that end, it reverts any edits made to the text field to a constant value, and
 * exposes the protected "Select All" functionality from InputField
 */
public class ConstantInputField : InputField {

	private string val;

	public string constantValue {
		get { return val; }
		set {
			val = value;
			RevertValue ();
		}
	}

	public new void Start() {
		gameObject.GetComponent<InputField> ().onEndEdit.AddListener (delegate{RevertValue();});
	}

	public void RevertValue() {
		gameObject.GetComponent<InputField> ().text = constantValue;
	}

	public void CallSelectAll() {
		gameObject.GetComponent<InputField> ().Select ();
		SelectAll ();
	}
}
