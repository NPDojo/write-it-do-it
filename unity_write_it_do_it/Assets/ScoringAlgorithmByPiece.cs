﻿using UnityEngine;
using System;
using System.Collections.Generic;

namespace WriteItDoIt
{
	public class ScoringAlgorithmByPiece
	{
		private static readonly int MAX_CONNECTION_SCORE = 2;

		private static int GetPieceScore(Piece toCheck, Piece correct)
		{
			int score = 0;

			// If the color matches, add one point
			if (toCheck.Color == correct.Color) {
//				Debug.Log("color point");
				score++;
			}
			
			// If the piece type matches, add one point and check the total number of connections,
			// adding one point per correct unconnected connection point.  Then check the magnitude
			// of the average of the connections' 'normal' vectors as described below
			if (toCheck.Name == correct.Name) {
//				Debug.Log("name point");
				score ++;
				if (toCheck.Connections.Count == correct.Connections.Count) {
					score += toCheck.ConnectionPoints.Count - toCheck.Connections.Count;
					
					// If the length of the average of the connections' 'normal' vectors matches that
					// of the correct piece, add one point
					// *This doesn't use the vector itself because the piece could be oriented differently
					//  in the user's model, even though the relative orientations of their connections
					//  are correct
					if (toCheck.Connections.Count > 0) {
						Vector3 toCheckNormalAvg = Vector3.zero;
						Vector3 correctNormalAvg = Vector3.zero;
						foreach (Connection c in toCheck.Connections) {
							toCheckNormalAvg += c.ParentConnectionPoint.Normal;
						}
						foreach (Connection c in correct.Connections) {
							correctNormalAvg += c.ParentConnectionPoint.Normal;
						}
						toCheckNormalAvg /= toCheck.Connections.Count;
						correctNormalAvg /= correct.Connections.Count;
						if (Vector3.Magnitude(toCheckNormalAvg) == Vector3.Magnitude(correctNormalAvg)) {
//							Debug.Log("orientation point");
							score++;
						}
					}
				}
			}
			
			// Find the best-matching connection points and add up to 2 points each (one for
			// each piece involved in the connection being correct)
			foreach (Connection connToCheck in toCheck.Connections) {
				int highScore = 0;
				Connection highScoreConnection = null;
				foreach (Connection correctConn in correct.Connections) {
					int thisScore = 0;
					if (!correctConn.alreadyScored) {
						thisScore = GetConnectionScore(connToCheck, correctConn);
						if (thisScore > highScore) {
							highScore = thisScore;
							highScoreConnection = correctConn;
						}
					}
				}
				score += highScore;
				if (highScoreConnection != null) {
					highScoreConnection.alreadyScored = true;
				}
			}
			foreach (Connection c in correct.Connections) {
				c.alreadyScored = false;
			}
			Debug.Log("Score for " + toCheck.ColorAndName() + 
				" with " + correct.ColorAndName() + " is " + score);
			return score;
		}

		// TODO: make this method work. I don't know what's wrong with it, but for some
		// reason it's not always returning the correct value. 3 AM me is at a loss as
		// to what the problem is. The commented-out first part of GetModelScore()
		// is the only place this method is called from. Limited testing shows the
		// version with this call commented out works, though theoretically it shouldn't.
		// -Alan
		private static bool IsPerfectPieceMatch(Piece toCheck, Piece correct)
		{
			bool isPerfectMatchThusFar = (toCheck.ColorAndName() == correct.ColorAndName());
			if (isPerfectMatchThusFar) {
				Debug.Log("color&name match");
				if (toCheck.Connections.Count == correct.Connections.Count) {
					Debug.Log("connection count match");
					if (toCheck.Connections.Count > 0) {
						Vector3 toCheckNormalAvg = Vector3.zero;
						Vector3 correctNormalAvg = Vector3.zero;
						foreach (Connection c in toCheck.Connections) {
							toCheckNormalAvg += c.ParentConnectionPoint.Normal;
						}
						foreach (Connection c in correct.Connections) {
							correctNormalAvg += c.ParentConnectionPoint.Normal;
						}
						toCheckNormalAvg /= toCheck.Connections.Count;
						correctNormalAvg /= correct.Connections.Count;
						if (Vector3.Magnitude(toCheckNormalAvg) == Vector3.Magnitude(correctNormalAvg)) {
							Debug.Log("normal average match");
							isPerfectMatchThusFar = true;
						} else {
							return false;
						}
						foreach (Connection connToCheck in toCheck.Connections) {
							int highScore = 0;
							Connection highScoreConnection = null;
							foreach (Connection correctConn in correct.Connections) {
								int thisScore = 0;
								if (!correctConn.alreadyScored) {
									thisScore = GetConnectionScore(connToCheck, correctConn);
									if (thisScore > highScore) {
										highScore = thisScore;
										highScoreConnection = correctConn;
									}
								}
							}
							if (highScore == MAX_CONNECTION_SCORE) {
								Debug.Log("connection match");
								isPerfectMatchThusFar = true;
							} else {
								return false;
							}
							if (highScoreConnection != null) {
								highScoreConnection.alreadyScored = true;
							}
						}
						foreach (Connection c in correct.Connections) {
							c.alreadyScored = false;
						}
					}
				} else {
					return false;
				}
			}
			if (isPerfectMatchThusFar) {
				Debug.Log(toCheck.ColorAndName() + " is a perfect match with " + correct.ColorAndName());
			}
			return isPerfectMatchThusFar;
		}

		private static int GetConnectionScore(Connection toCheck, Connection correct)
		{
			int score = 0;
			if (toCheck.ParentPiece.ColorAndName() == correct.ParentPiece.ColorAndName() ||
			    toCheck.ParentPiece.ColorAndName() == correct.OtherPiece.ColorAndName()) {
				// pieceA is correct
				score++;
			}
			if (toCheck.OtherPiece.ColorAndName() == correct.OtherPiece.ColorAndName() ||
			    toCheck.OtherPiece.ColorAndName() == correct.ParentPiece.ColorAndName()) {
				// pieceB is correct
				score++;
			}
			return score;
		}
		
		public static int GetModelScore(Model modelToCheck, Model modelCorrect)
		{
			int score = 0;
			
//			foreach (Piece pieceToCheck in modelToCheck.Pieces) {
//				foreach (Piece correctPiece in modelCorrect.Pieces) {
//					if (!correctPiece.alreadyScored && IsPerfectPieceMatch(pieceToCheck, correctPiece)) {
//						score += GetPieceScore(pieceToCheck, correctPiece);
//						pieceToCheck.alreadyScored = true;
//						correctPiece.alreadyScored = true;
//					}
//				}
//				if (!pieceToCheck.alreadyScored) {
//					Debug.Log("unmatched piece: " + pieceToCheck.ConnInfo());
//				}
//			}
//			
//			int numUnmatched = 0;
//			foreach (Piece p in modelToCheck.Pieces) {
//				if (!p.alreadyScored) {
//					numUnmatched++;
//				}
//			}
//			Debug.Log(numUnmatched + " unmatched Pieces");

			foreach (Piece pieceToCheck in modelToCheck.Pieces) {
				if (!pieceToCheck.alreadyScored) {
					int highScore = 0;
					Piece highScorePiece = null;
					foreach (Piece correctPiece in modelCorrect.Pieces) {
						int thisScore = 0;
						if (!correctPiece.alreadyScored) {
							thisScore = GetPieceScore(pieceToCheck, correctPiece);
							if (thisScore > highScore) {
								Debug.Log("thisScore > highScore");
								highScore = thisScore;
								highScorePiece = correctPiece;
							}
						}
					}
					score += highScore;
					if (highScorePiece != null) {
						Debug.Log("high score for " + pieceToCheck.ColorAndName() + " is " + highScore +
						          " (" + highScorePiece.ColorAndName() + ")");
						highScorePiece.alreadyScored = true;
					}
				}
			}
			foreach (Piece p in modelToCheck.Pieces) {
				p.alreadyScored = false;
			}
			foreach (Piece p in modelCorrect.Pieces) {
				p.alreadyScored = false;
			}
			return score;
		}

		public static int getMaxScore(Model modelToScore)
		{
			return GetModelScore(modelToScore, modelToScore);
		}
	}
}