using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace WriteItDoIt
{
	public class StarRodPiece : MalePiece {
		
		private static float pieceLength = Constants.lengthUnit * Constants.rodLengthFactor;
		private static float pieceWidth = Constants.lengthUnit * Constants.rodWidthFactor;
		
		// front inner face
		private static Vector3 vxA = new Vector3 (-pieceWidth, pieceLength, -pieceWidth);
		private static Vector3 vxB = new Vector3 (pieceWidth, pieceLength, -pieceWidth);
		private static Vector3 vxC = new Vector3 (pieceWidth, pieceWidth, -pieceWidth);
		private static Vector3 vxD = new Vector3 (pieceLength, pieceWidth, -pieceWidth);
		private static Vector3 vxE = new Vector3 (pieceLength, -pieceWidth, -pieceWidth);
		private static Vector3 vxF = new Vector3 (pieceWidth, -pieceWidth, -pieceWidth);
		private static Vector3 vxG = new Vector3 (pieceWidth, -pieceLength, -pieceWidth);
		private static Vector3 vxH = new Vector3 (-pieceWidth, -pieceLength, -pieceWidth);
		private static Vector3 vxI = new Vector3 (-pieceWidth, -pieceWidth, -pieceWidth);
		private static Vector3 vxJ = new Vector3 (-pieceLength, -pieceWidth, -pieceWidth);
		private static Vector3 vxK = new Vector3 (-pieceLength, pieceWidth, -pieceWidth);
		private static Vector3 vxL = new Vector3 (-pieceWidth, pieceWidth, -pieceWidth);
		
		// back inner face
		private static Vector3 vxM = new Vector3 (-pieceWidth, pieceWidth, pieceWidth);
		private static Vector3 vxN = new Vector3 (-pieceLength, pieceWidth, pieceWidth);
		private static Vector3 vxO = new Vector3 (-pieceLength, -pieceWidth, pieceWidth);
		private static Vector3 vxP = new Vector3 (-pieceWidth, -pieceWidth, pieceWidth);
		private static Vector3 vxQ = new Vector3 (-pieceWidth, -pieceLength, pieceWidth);
		private static Vector3 vxR = new Vector3 (pieceWidth, -pieceLength, pieceWidth);
		private static Vector3 vxS = new Vector3 (pieceWidth, -pieceWidth, pieceWidth);
		private static Vector3 vxT = new Vector3 (pieceLength, -pieceWidth, pieceWidth);
		private static Vector3 vxU = new Vector3 (pieceLength, pieceWidth, pieceWidth);
		private static Vector3 vxV = new Vector3 (pieceWidth, pieceWidth, pieceWidth);
		private static Vector3 vxW = new Vector3 (pieceWidth, pieceLength, pieceWidth);
		private static Vector3 vxX = new Vector3 (-pieceWidth, pieceLength, pieceWidth);

		// front connection face
		private static Vector3 vxY = new Vector3 (-pieceWidth, pieceWidth, -pieceLength);
		private static Vector3 vxZ = new Vector3 (pieceWidth, pieceWidth, -pieceLength);
		private static Vector3 vxAA = new Vector3 (pieceWidth, -pieceWidth, -pieceLength);
		private static Vector3 vxAB = new Vector3 (-pieceWidth, -pieceWidth, -pieceLength);

		// back connection face
		private static Vector3 vxAC = new Vector3 (pieceWidth, pieceWidth, pieceLength);
		private static Vector3 vxAD = new Vector3 (-pieceWidth, pieceWidth, pieceLength);
		private static Vector3 vxAE = new Vector3 (-pieceWidth, -pieceWidth, pieceLength);
		private static Vector3 vxAF = new Vector3 (pieceWidth, -pieceWidth, pieceLength);
		
		// connection points
		private static Vector3 cpA = new Vector3 (0, pieceLength, 0); // top
		private static Vector3 cpB = new Vector3 (pieceLength, 0, 0); // right
		private static Vector3 cpC = new Vector3 (0, -pieceLength, 0); // bottom
		private static Vector3 cpD = new Vector3 (-pieceLength, 0, 0); // left
		private static Vector3 cpE = new Vector3 (0, 0, -pieceLength); // front
		private static Vector3 cpF = new Vector3 (0, 0, pieceLength); // back
		
		static Vector3[] vertices = 
		{
			vxA, vxB, vxC, vxD, vxE, vxF, vxG, vxH, vxI, vxJ, vxK, vxL, // front inner face; 0-11
			vxM, vxN, vxO, vxP, vxQ, vxR, vxS, vxT, vxU, vxV, vxW, vxX, // back inner face; 12-23
			vxL, vxK, vxN, vxM, vxAD, vxAC, vxV, vxU, vxD, vxC, vxZ, vxY, // top inner face; 24-35
			vxV, vxAC, vxAF, vxS, vxR, vxG, vxF, vxAA, vxZ, vxC, vxB, vxW, // right inner face; 36-47
			vxL, vxY, vxAB, vxI, vxH, vxQ, vxP, vxAE, vxAD, vxM, vxX, vxA, // left inner face; 48-59
			vxI, vxAB, vxAA, vxF, vxE, vxT, vxS, vxAF, vxAE, vxP, vxO, vxJ, // bottom inner face; 60-71
			vxA, vxX, vxW, vxB, // top conn face; 72-75
			vxD, vxU, vxT, vxE, // right conn face; 76-79
			vxH, vxG, vxR, vxQ, // bottom conn face; 80-83
			vxK, vxJ, vxO, vxN, // left conn face; 84-87
			vxY, vxZ, vxAA, vxAB, // front conn face; 88-91
			vxAC, vxAD, vxAE, vxAF, // back conn face; 92-95

			cpA, cpB, cpC, cpD, cpE, cpF // connection points; 96-101
		};
		
		static int[] triangles =
		{
			// front inner face
			0, 1, 2,
			0, 2, 11,
			2, 3, 4,
			2, 4, 5,
			5, 6, 7, 
			5, 7, 8,
			8, 9, 10,
			8, 10, 11,
			11, 2, 5,
			11, 5, 8,
			
			// back inner face
			12, 13, 14,
			12, 14, 15,
			15, 16, 17,
			15, 17, 18,
			18, 19, 20,
			18, 20, 21,
			21, 22, 23,
			21, 23, 12,
			12, 15, 18,
			12, 18, 21,
			
			// top inner face
			24, 25, 26,
			24, 26, 27,
			27, 28, 29,
			27, 29, 30,
			30, 31, 32,
			30, 32, 33,
			33, 34, 35,
			33, 35, 12,
			24, 27, 30,
			24, 30, 33,
			
			// right inner face
			36, 37, 38,
			36, 38, 39,
			39, 40, 41,
			39, 41, 42,
			42, 43, 44,
			42, 44, 45,
			45, 46, 47,
			45, 47, 36,
			36, 39, 42,
			36, 42, 45,
			
			// left inner face
			48, 49, 50,
			48, 50, 51,
			51, 52, 53,
			51, 53, 54,
			54, 55, 56,
			54, 56, 57,
			57, 58, 59,
			57, 59, 48,
			48, 51, 54,
			48, 54, 57,
			
			// bottom inner face
			60, 61, 62,
			60, 62, 63,
			63, 64, 65,
			63, 65, 66,
			66, 67, 68,
			66, 68, 69,
			69, 70, 71,
			69, 71, 60,
			60, 63, 66,
			60, 66, 69,
			
			// top conn face
			72, 73, 74,
			72, 74, 75,
			
			// right conn face
			76, 77, 78,
			76, 78, 79,
			
			// left conn face
			80, 81, 82,
			80, 82, 83,
			
			// bottom conn face
			84, 85, 86,
			84, 86, 87,

			// front conn face
			88, 89, 90,
			88, 90, 91,

			// back conn face
			92, 93, 94,
			92, 94, 95
		};
		
		static List<ConnectionPoint> connectionPoints = new List<ConnectionPoint>()
		{
			new ConnectionPoint(96,new Vector3(0,0,1),new Vector3(0,1,0)),
			new ConnectionPoint(97,new Vector3(0,1,0),new Vector3(1,0,0)),
			new ConnectionPoint(98,new Vector3(0,0,1),new Vector3(0,-1,0)),
			new ConnectionPoint(99,new Vector3(0,1,0),new Vector3(-1,0,0)),
			new ConnectionPoint(100,new Vector3(0,1,0),new Vector3(0,0,-1)),
			new ConnectionPoint(101,new Vector3(0,1,0),new Vector3(0,0,1))
		};
		
		public StarRodPiece(Color color) : base("StarRod", vertices, triangles, connectionPoints, color)
		{
		}

		public override Piece Copy()
		{
			return new StarRodPiece(this.Color);
		}
	}
}

