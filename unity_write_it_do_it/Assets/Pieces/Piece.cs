//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.1
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

namespace WriteItDoIt
{
	public abstract class Piece
	{
		private static float maxLerpDuration = 0.7f;
		//private static float posLerpCoefficient = 0.07f;

		public Boolean alreadyScored = false;

		//Now wraps colorindex for serialization purposes
		public Color Color 
		{
			get
			{
				return Constants.pieceColors[ColorIndex];
			}
			protected set
			{
				this.ColorIndex = Utilities.IndexForPieceColor(value);
			}
		}

		public int ColorIndex {
			get;
			private set;
		}

		public List<Connection> Connections 
		{
			get;
			protected set;
		}

		public Vector3[] Vertices 
		{
			get;
			private set;
		}

		public int[] Triangles 
		{
			get;
			private set;
		}

		public abstract bool IsMale 
		{
			get;
		}

		public List<ConnectionPoint> ConnectionPoints 
		{
			get;
			private set;
		}

		public GameObject GameObject
		{
			get;
			private set;
		}

		public string Name
		{
			get;
			private set;
		}

		public PieceBehavior PieceBehavior 
		{
			get;
			private set;
		}

		public bool IsDisplayPiece 
		{
			get;
			set;
		}

		private static GameObject modelObj = null;

		public Piece(string name, Vector3[] vertices, int[] triangles, 
		             List<ConnectionPoint> connectionPoints, Color color)
		{
			this.Name = name;
			this.Vertices = vertices;
			this.Triangles = triangles;
			this.ConnectionPoints = new List<ConnectionPoint> ();
			this.Color = color;
			this.Connections = new List<Connection>();
			this.GameObject = GameObjectFactory.CreateGameObject(name, 
			                                                     vertices, 
			                                                     triangles, 
			                                                     connectionPoints, 
			                                                     color);
			GameObject parent = new GameObject(Constants.StructureTag);
			parent.tag = Constants.StructureTag;
			if (modelObj == null) {
				modelObj = new GameObject(Constants.ModelTag);
				modelObj.tag = Constants.ModelTag;
			}
			parent.transform.SetParent(modelObj.transform, true);
			this.GameObject.transform.SetParent(parent.transform, true);
			this.PieceBehavior = this.GameObject.AddComponent<PieceBehavior>();
			this.PieceBehavior.Piece = this;
			this.GameObject.tag = Constants.PieceTag;

			// Replace each static connection point with a piece-specific instance, and attach
			for (int i = 0; i < connectionPoints.Count; i++) {
				ConnectionPoint c = connectionPoints[i];
				this.ConnectionPoints.Add (new ConnectionPoint(c.VertexIndex, c.Up, c.Normal));
				AttachConnectionPoint(this.ConnectionPoints[i]);
			}
		}

		private void AttachConnectionPoint(ConnectionPoint c)
		{
			c.Piece = this;
			GameObject gob = c.build ();
			gob.transform.parent = this.GameObject.transform;
			gob.transform.rotation = Quaternion.LookRotation(c.Normal);
			Vector3 cpPos = VertexForConnectionPoint(c);

			gob.transform.localPosition = cpPos;
		}

		public void ConnectTo(ConnectionPoint c1, Piece p, ConnectionPoint c2, Boolean lerp = true)
		{
			Connect (this, c1, p, c2, lerp);
		}

		public static void Connect(Piece p1, 
		                           ConnectionPoint c1, 
		                           Piece p2, 
		                           ConnectionPoint c2, 
		                           Boolean lerp = true)
		{
			Transform str1 = p1.GameObject.transform.parent;
			Transform str2 = p2.GameObject.transform.parent;

			// If they are part of the same structure, disconnect p1 from that structure
			if (str1 == str2) {
				List<Piece> adjacentPieces = Utilities.adjacentPieces(p1);
				//Debug.Log (adjacentPieces.Count + " adjacent pieces");
				Piece pieceToDisconnect = null;
				int i = 0;
				Piece adj = null;
				while (pieceToDisconnect == null) {
					//Debug.Log (i + " " + adjacentPieces[i].Name);
					adj = adjacentPieces[i];
					if (Utilities.reachablePieces(adj, p1).Contains (p2))
						pieceToDisconnect = adj;
					i++;
				}
				//Debug.Log ("Disconnecting " + p1.Name + " and" + adj.Name);
				Disconnect(p1,adj, false);
				str1 = p1.GameObject.transform.parent;
				str2 = p2.GameObject.transform.parent;
				//Debug.Log ("str1 = " + str1.gameObject.name + ", str2 = " + str2.gameObject.name);
			}

			// Temporarily make p1 the parent to its structure
			p1.GameObject.transform.SetParent (null, true);
			str1.SetParent (p1.GameObject.transform, true);
			
			//We need to transform the connection point normals and ups
			Vector3 transNorm1 = p1.GameObject.transform.rotation * c1.Normal;
			Vector3 transNorm2 = p2.GameObject.transform.rotation * -c2.Normal;

			Quaternion targetRotation = Quaternion.FromToRotation(transNorm1, transNorm2) * 
				p1.GameObject.transform.rotation;

			Vector3 transUp1 = targetRotation * c1.Up;
			Vector3 transUp2 = p2.GameObject.transform.rotation * c2.Up;
			float upDiffAngle = Vector3.Angle(transUp1, transUp2);
			
			targetRotation *= Quaternion.AngleAxis(-upDiffAngle, c1.Normal);

			//We want distance from center of this piece to c1, but we need that
			//magnitude in the direction of c2's normal
			// Note: the 0.01f is one of those "stupid solutions"; the goal is to create
			// a small space between connected pieces to discourage collision/trigger detection
			float distanceToPoint = p1.VertexForConnectionPoint(c1).magnitude + 0.01f;
			Vector3 magV1 = new Vector3(distanceToPoint, distanceToPoint, distanceToPoint);

			Vector3 cpLoc = targetRotation * p1.VertexForConnectionPoint(c1);
			Vector3 offset = Vector3.Normalize(-transNorm2) * 0.01f;

			Vector3 destination = p2.GameObject.transform.position
//				+ Vector3.Scale (magV1, Vector3.Normalize (-transNorm2))
				+ offset
				+ p2.GameObject.transform.rotation * p2.VertexForConnectionPoint(c2)
				- cpLoc;


			Action postLerpReparent = delegate 
			{
				// Reform structure hierarchy by transferring all of structure1's children to structure2
				str1.SetParent (modelObj.transform);
				p1.GameObject.transform.SetParent (str1, true);
				Utilities.mergeStructures (p1, p2);
			};

			if (lerp) { //Lerp it for good looks
				p1.PieceBehavior.LerpTo(targetRotation, 
				                        destination,
				                        Constants.ConnectingLerpTime,
				                        postLerpReparent);
			} else { // Connect instantly instead
				p1.GameObject.transform.rotation = targetRotation;
				p1.GameObject.transform.position = destination;
				postLerpReparent(); //we need to reparent even if we don't lerp
			}

			AddConnections (p1, c1, p2, c2);
		}
		
		public void DisconnectFrom(Piece p, bool offset = true)
		{
			// Identify the substructure that p belongs to, excluding this and 
			// anything only reachable from this
			List<Piece> substructure = Utilities.reachablePieces (p, this);
			List<Piece> piecesToDisconnect = new List<Piece> ();
			// Identify pieces which this is connected to from that substructure
			foreach (Piece adj in Utilities.adjacentPieces(this)) {
				if (substructure.Contains (adj)) {
					piecesToDisconnect.Add (adj);
				}
			}
			
			if (piecesToDisconnect.Count == 0) return; // i.e., the pieces weren't actually connected

			foreach (Piece other in piecesToDisconnect) {
				//Remove connection from both pieces
				RemoveConnections(this, other);
				RemoveConnections(other, this);
			}

			// Create new parent structure
			GameObject newSt = new GameObject (Constants.StructureTag);
			newSt.tag = Constants.StructureTag;
			newSt.transform.SetParent (modelObj.transform, true);
			foreach (Piece piece in substructure) {
				piece.GameObject.transform.SetParent(newSt.transform, true);
			}

			// Move the disconnected structure away
			if (offset) 
			{
				Transform str = p.GameObject.transform.parent;
				p.GameObject.transform.SetParent (null, true);
				str.SetParent (p.GameObject.transform, true);

				Vector3 newPos = Vector3.MoveTowards(p.GameObject.transform.position, 
					                    			 this.GameObject.transform.position, -1);
				Action postLerpReparent = delegate {
					str.SetParent (modelObj.transform, true);
					p.GameObject.transform.SetParent (str, true);
				};
				p.GameObject.GetComponent<PieceBehavior>().LerpTo(p.GameObject.transform.rotation,
				                                                     newPos,
				                                                     maxLerpDuration,
				                                                     postLerpReparent);
			}
		}

		public static void Disconnect(Piece p1, Piece p2, bool offset = true) {
			p1.DisconnectFrom (p2, offset);
		}

		public static void DisconnectAll(Piece p, bool offset = true) {
			while (p.Connections.Count > 0) p.DisconnectFrom (p.Connections[0].OtherPiece, offset);
		}

		/* An old, less-lazy implementation of disconnectAll; 
		 * disconnected pieces would offset improperly
		 * 
		public static void DisconnectAll(Piece p, bool offset = true) {
			if (p.Connections.Count == 0) return;

			// Give the piece a new structure parent
			GameObject newStructure = new GameObject (Constants.StructureTag);
			newStructure.tag = Constants.StructureTag;
			newStructure.transform.SetParent (modelObj.transform, true);
			p.GameObject.transform.SetParent (newStructure.transform, true);

			// Update the piece data structure's connection list
			List<Connection> deadcons = new List<Connection>();
			List<Piece> disconnectedPieces = new List<Piece> ();
			foreach (Connection c in p.Connections) 
			{
				deadcons.Add(c);
			}
			foreach (Connection c in deadcons) 
			{
				RemoveConnections(c.OtherPiece, p);
				RemoveConnections(p, c.OtherPiece);
				p.Connections.Remove(c);
				disconnectedPieces.Add (c.OtherPiece);
			}

			// A duplicate list, for use later when moving disconnected pieces away from p
			Piece[] disconnectedPiecesCopy = new Piece[disconnectedPieces.Count];
			disconnectedPieces.CopyTo (disconnectedPiecesCopy);

			// Create a list of now-separated substructures
			List<List<Piece>> substructures = new List<List<Piece>> ();
			while (disconnectedPieces.Count > 0) {
				List<Piece> substructure = Utilities.reachablePieces (disconnectedPieces[0]);
				substructures.Add (substructure);
				foreach(Piece piece in substructure) {
					if(disconnectedPieces.Contains(piece)) {
						disconnectedPieces.Remove (piece);
					}
				}
			}
			// Sort list, smallest substructure first (this way the largest 
			// substructure isn't reparented)
			substructures.Sort ((a,b) => a.Count - b.Count);
			// If there's more than one substructure, reparent substructures until there's one left
			while(substructures.Count > 1) {
				GameObject newSt = new GameObject (Constants.StructureTag);
				newSt.tag = Constants.StructureTag;
				newSt.transform.SetParent (modelObj.transform, true);
				foreach(Piece piece in substructures[0]) {
					piece.GameObject.transform.SetParent (newSt.transform, true);
				}
				substructures.RemoveAt (0);
			}

			// Move every substructure away from p
			if (offset) 
			{
				foreach (Piece other in disconnectedPiecesCopy) 
				{
					Transform str = other.GameObject.transform.parent;
					Piece o = other; // Necessary for proper execution of delegate
					other.GameObject.transform.SetParent(null, true);
					str.SetParent(other.GameObject.transform, true);

					Vector3 newPos = Vector3.MoveTowards(other.GameObject.transform.position, 
					                                     p.GameObject.transform.position, -1);
					Action postLerpReparent = delegate {
						str.SetParent(modelObj.transform, true);
						o.GameObject.transform.SetParent(str, true);
					};
					other.PieceBehavior.LerpTo(other.GameObject.transform.rotation,
					                           newPos,
					                           maxLerpDuration,
					                           postLerpReparent);
				}
			}
		}
		*/

		// Adds the specified connection to both pieces' lists and marks connection points as in use
		public static void AddConnections(Piece p1, ConnectionPoint c1, Piece p2, ConnectionPoint c2) {
			Vector3 c1Up = p1.GameObject.transform.rotation * c1.Up;
			Vector3 c2Up = p2.GameObject.transform.rotation * c2.Up;
			
			p1.Connections.Add(new Connection(p1, c1, p2, c2, Vector3.Angle(c1Up, c2Up)));
			p2.Connections.Add(new Connection(p2, c2, p1, c1, Vector3.Angle(c2Up, c1Up)));

			c1.isUsed = true;
			c2.isUsed = true;
		}

		// Removes connections between these two pieces from their lists and marks 
		// connection points as vacant
		private static void RemoveConnections(Piece p1, Piece p2)
		{
			List<Connection> deadcons = new List<Connection>();
			foreach (Connection c in p1.Connections) 
			{
				if (c.OtherPiece == p2)
				{
					deadcons.Add(c);
				}
			}
			foreach (Connection c in deadcons) 
			{
				p1.Connections.Remove(c);
				c.ParentConnectionPoint.isUsed = false;
			}
			// Repeat for the other piece
			deadcons = new List<Connection>();
			foreach (Connection c in p2.Connections) 
			{
				if (c.OtherPiece == p1)
				{
					deadcons.Add(c);
				}
			}
			foreach (Connection c in deadcons) 
			{
				p2.Connections.Remove(c);
				c.ParentConnectionPoint.isUsed = false;
			}
		}

		public Vector3 VertexForConnectionPoint(ConnectionPoint c)
		{
			return Vertices [c.VertexIndex];
		}

		public void ChangeColor (Color newColor)
		{
			this.Color = newColor;
			this.GameObject.renderer.material.color = this.Color;
		}

		public override string ToString()
		{
			return this.Name + " " + (this.IsMale ? "(M)" : "(F)");
		}
		
		public string ConnInfo()
		{
			return this.ColorAndName() + ": " + this.Connections.Count + " connections";
		}

		public string ColorAndName()
		{
			return Constants.pieceColorNames [this.ColorIndex] + " " + this.ToString ();
		}

		public abstract Piece Copy();
	}
}

