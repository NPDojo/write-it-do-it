using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace WriteItDoIt
{
	public class XPlusRodPiece : MalePiece {
		
		private static float pieceLength = Constants.lengthUnit * Constants.rodLengthFactor;
		private static float pieceWidth = Constants.lengthUnit * Constants.rodWidthFactor;
		
		// front face
		private static Vector3 vxA = new Vector3 (-pieceWidth, pieceLength, -pieceWidth);
		private static Vector3 vxB = new Vector3 (pieceWidth, pieceLength, -pieceWidth);
		private static Vector3 vxC = new Vector3 (pieceWidth, pieceWidth, -pieceWidth);
		private static Vector3 vxD = new Vector3 (pieceLength, pieceWidth, -pieceWidth);
		private static Vector3 vxE = new Vector3 (pieceLength, -pieceWidth, -pieceWidth);
		private static Vector3 vxF = new Vector3 (pieceWidth, -pieceWidth, -pieceWidth);
		private static Vector3 vxG = new Vector3 (pieceWidth, -pieceLength, -pieceWidth);
		private static Vector3 vxH = new Vector3 (-pieceWidth, -pieceLength, -pieceWidth);
		private static Vector3 vxI = new Vector3 (-pieceWidth, -pieceWidth, -pieceWidth);
		private static Vector3 vxJ = new Vector3 (-pieceLength, -pieceWidth, -pieceWidth);
		private static Vector3 vxK = new Vector3 (-pieceLength, pieceWidth, -pieceWidth);
		private static Vector3 vxL = new Vector3 (-pieceWidth, pieceWidth, -pieceWidth);

		// back face
		private static Vector3 vxM = new Vector3 (-pieceWidth, pieceWidth, pieceWidth);
		private static Vector3 vxN = new Vector3 (-pieceLength, pieceWidth, pieceWidth);
		private static Vector3 vxO = new Vector3 (-pieceLength, -pieceWidth, pieceWidth);
		private static Vector3 vxP = new Vector3 (-pieceWidth, -pieceWidth, pieceWidth);
		private static Vector3 vxQ = new Vector3 (-pieceWidth, -pieceLength, pieceWidth);
		private static Vector3 vxR = new Vector3 (pieceWidth, -pieceLength, pieceWidth);
		private static Vector3 vxS = new Vector3 (pieceWidth, -pieceWidth, pieceWidth);
		private static Vector3 vxT = new Vector3 (pieceLength, -pieceWidth, pieceWidth);
		private static Vector3 vxU = new Vector3 (pieceLength, pieceWidth, pieceWidth);
		private static Vector3 vxV = new Vector3 (pieceWidth, pieceWidth, pieceWidth);
		private static Vector3 vxW = new Vector3 (pieceWidth, pieceLength, pieceWidth);
		private static Vector3 vxX = new Vector3 (-pieceWidth, pieceLength, pieceWidth);

		// front arm
		private static Vector3 vxY = new Vector3 (-pieceWidth, pieceWidth, -pieceLength);
		private static Vector3 vxZ = new Vector3 (pieceWidth, pieceWidth, -pieceLength);
		private static Vector3 vxAA = new Vector3 (pieceWidth, -pieceWidth, -pieceLength);
		private static Vector3 vxAB = new Vector3 (-pieceWidth, -pieceWidth, -pieceLength);

		// connection points
		private static Vector3 cpA = new Vector3 (0, pieceLength, 0); // top
		private static Vector3 cpB = new Vector3 (pieceLength, 0, 0); // right
		private static Vector3 cpC = new Vector3 (0, -pieceLength, 0); // bottom
		private static Vector3 cpD = new Vector3 (-pieceLength, 0, 0); // left
		private static Vector3 cpE = new Vector3 (0, 0, -pieceLength); // front
		
		static Vector3[] vertices = 
		{
			vxA, vxB, vxC, vxD, vxE, vxF, vxG, vxH, vxI, vxJ, vxK, vxL, // front face
			vxM, vxN, vxO, vxP, vxQ, vxR, vxS, vxT, vxU, vxV, vxW, vxX, // back face
			vxA, vxX, vxW, vxB, // top face
			vxD, vxU, vxT, vxE, // right face
			vxH, vxG, vxR, vxQ, // bottom face
			vxK, vxJ, vxO, vxN, // left face
			vxD, vxK, vxN, vxU, // top inner face
			vxB, vxW, vxR, vxG, // right inner face
			vxA, vxH, vxQ, vxX, // left inner face
			vxJ, vxE, vxT, vxO, // bottom inner face
			vxY, vxZ, vxAA, vxAB, // front conn face
			vxL, vxC, vxZ, vxY, // front top face
			vxC, vxF, vxAA, vxZ, // front right face
			vxF, vxI, vxAB, vxAA, // front bottom face
			vxI, vxL, vxY, vxAB, // front left face

			cpA, cpB, cpC, cpD, cpE // connection points
		};
		
		static int[] triangles =
		{
			// front face
			0, 1, 2,
			0, 2, 11,
			2, 3, 4,
			2, 4, 5,
			5, 6, 7, 
			5, 7, 8,
			8, 9, 10,
			8, 10, 11,
			11, 2, 5,
			5, 8, 11,
			
			// back face
			12, 13, 14,
			12, 14, 15,
			15, 16, 17,
			15, 17, 18,
			18, 19, 20,
			18, 20, 21,
			21, 22, 23,
			21, 23, 12,
			12, 15, 18,
			12, 18, 21,
			
			// top face
			24, 25, 26,
			24, 26, 27,

			// right face
			28, 29, 30,
			28, 30, 31,

			// bottom face
			32, 33, 34,
			32, 34, 35,

			// left face
			36, 37, 38,
			36, 38, 39,

			// top inner face
			40, 41, 42,
			40, 42, 43,

			// right inner face
			44, 45, 46,
			44, 46, 47,

			// left inner face
			48, 49, 50,
			48, 50, 51,

			// bottom inner face
			52, 53, 54,
			52, 54, 55,

			// front conn face
			56, 57, 58,
			56, 58, 59,

			// front top face
			60, 61, 62,
			60, 62, 63,

			// front right face
			64, 65, 66,
			64, 66, 67,

			// front bottom face
			68, 69, 70,
			68, 70, 71,

			// front left face
			72, 73, 74,
			72, 74, 75
		};
		
		static List<ConnectionPoint> connectionPoints = new List<ConnectionPoint>()
		{
			new ConnectionPoint(76,new Vector3(0,0,1),new Vector3(0,1,0)),
			new ConnectionPoint(77,new Vector3(0,1,0),new Vector3(1,0,0)),
			new ConnectionPoint(78,new Vector3(0,0,1),new Vector3(0,-1,0)),
			new ConnectionPoint(79,new Vector3(0,1,0),new Vector3(-1,0,0)),
			new ConnectionPoint(80,new Vector3(0,1,0),new Vector3(0,0,-1))
		};
		
		public XPlusRodPiece(Color color) : base("XPlusRod", vertices, triangles, connectionPoints, color)
		{
		}

		public override Piece Copy()
		{
			return new XPlusRodPiece(this.Color);
		}
	}
}
