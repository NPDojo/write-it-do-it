﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace WriteItDoIt
{
	public class TRodPiece : MalePiece {
		
		private static float pieceLength = Constants.lengthUnit * Constants.rodLengthFactor;
		private static float pieceWidth = Constants.lengthUnit * Constants.rodWidthFactor;

		// front face
		private static Vector3 vxA = new Vector3 (-pieceWidth, pieceLength, -pieceWidth);
		private static Vector3 vxB = new Vector3 (pieceWidth, pieceLength, -pieceWidth);
		private static Vector3 vxC = new Vector3 (pieceWidth, -pieceLength, -pieceWidth);
		private static Vector3 vxD = new Vector3 (-pieceWidth, -pieceLength, -pieceWidth);
		private static Vector3 vxE = new Vector3 (-pieceWidth, -pieceWidth, -pieceWidth);
		private static Vector3 vxF = new Vector3 (-pieceLength, -pieceWidth, -pieceWidth);
		private static Vector3 vxG = new Vector3 (-pieceLength, pieceWidth, -pieceWidth);
		private static Vector3 vxH = new Vector3 (-pieceWidth, pieceWidth, -pieceWidth);

		// back face
		private static Vector3 vxI = new Vector3 (pieceWidth, pieceLength, pieceWidth);
		private static Vector3 vxJ = new Vector3 (-pieceWidth, pieceLength, pieceWidth);
		private static Vector3 vxK = new Vector3 (-pieceWidth, pieceWidth, pieceWidth);
		private static Vector3 vxL = new Vector3 (-pieceLength, pieceWidth, pieceWidth);
		private static Vector3 vxM = new Vector3 (-pieceLength, -pieceWidth, pieceWidth);
		private static Vector3 vxN = new Vector3 (-pieceWidth, -pieceWidth, pieceWidth);
		private static Vector3 vxO = new Vector3 (-pieceWidth, -pieceLength, pieceWidth);
		private static Vector3 vxP = new Vector3 (pieceWidth, -pieceLength, pieceWidth);

		// connection points
		private static Vector3 cpA = new Vector3 (0, pieceLength, 0); // top
		private static Vector3 cpB = new Vector3 (-pieceLength, 0, 0); // left
		private static Vector3 cpC = new Vector3 (0, -pieceLength, 0); // bottom
		
		static Vector3[] vertices = 
		{
			vxA, vxB, vxC, vxD, vxE, vxF, vxG, vxH, // front face
			vxI, vxJ, vxK, vxL, vxM, vxN, vxO, vxP, // back face
			vxA, vxJ, vxI, vxB, // top face
			vxC, vxP, vxO, vxD, // bottom face
			vxF, vxM, vxL, vxG, // left face
			vxB, vxI, vxP, vxC, // right face
			vxF, vxE, vxN, vxM, // bottom inner face
			vxL, vxK, vxH, vxG, // top inner face
			vxA, vxH, vxK, vxJ, // left top inner face
			vxE, vxD, vxO, vxN, // left bottom inner face

			cpA, cpB, cpC // connection points
		};
		
		static int[] triangles =
		{
			// front face
			0, 1, 7,
			1, 2, 7,
			7, 2, 4,
			2, 3, 4,
			4, 5, 6,
			4, 6, 7,

			// back face
			8, 9, 10,
			8, 10, 13,
			8, 13, 15,
			10, 11, 12,
			10, 12, 13,
			13, 14, 15,

			// top face
			16, 17, 18,
			16, 18, 19,
			
			// bottom face
			20, 21, 22,
			20, 22, 23,
			
			// left face
			24, 25, 26,
			24, 26, 27,
			
			// right face
			28, 29, 30,
			28, 30, 31,
			
			// bottom inner face
			32, 33, 34,
			32, 34, 35,
			
			// top inner face
			36, 37, 38,
			36, 38, 39,
			
			// left top inner face
			40, 41, 42,
			40, 42, 43,
			
			// left bottom inner face
			44, 45, 46,
			44, 46, 47
		};
		
		static List<ConnectionPoint> connectionPoints = new List<ConnectionPoint>()
		{
			new ConnectionPoint(48,new Vector3(0,0,1),new Vector3(0,1,0)),
			new ConnectionPoint(49,new Vector3(0,1,0),new Vector3(-1,0,0)),
			new ConnectionPoint(50,new Vector3(0,0,1),new Vector3(0,-1,0))
		};
		
		public TRodPiece(Color color) : base("TRod", vertices, triangles, connectionPoints, color)
		{
		}

		public override Piece Copy()
		{
			return new TRodPiece(this.Color);
		}
	}
}