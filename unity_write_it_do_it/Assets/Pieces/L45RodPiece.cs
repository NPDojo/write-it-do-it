using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace WriteItDoIt
{
	public class L45RodPiece : MalePiece
	{
		private static float pieceLength = Constants.lengthUnit * Constants.rodLengthFactor;
		private static float pieceWidth = Constants.lengthUnit * Constants.rodWidthFactor;
		private static float jointX = pieceWidth;
		private static float jointY = pieceWidth * Mathf.Tan(Mathf.PI / 8);

		private static float ArmX(bool isTop)
		{
			float ans;
			if (isTop) {
				ans = 0.5f * Mathf.Sqrt(2) * (pieceLength - pieceWidth);
			} else {
				ans = 0.5f * Mathf.Sqrt(2) * (pieceLength + pieceWidth);
			}
			return ans;
		}

		private static float ArmY(bool isTop)
		{
			return ArmX(!isTop);
		}

		// front face
		private static Vector3 vxA = new Vector3(ArmX(true), ArmY(true), -pieceWidth);
		private static Vector3 vxB = new Vector3(ArmX(false), ArmY(false), -pieceWidth);
		private static Vector3 vxC = new Vector3(jointX, -jointY, -pieceWidth);
		private static Vector3 vxD = new Vector3(pieceWidth, -pieceLength, -pieceWidth);
		private static Vector3 vxE = new Vector3(-pieceWidth, -pieceLength, -pieceWidth);
		private static Vector3 vxF = new Vector3(-jointX, jointY, -pieceWidth);
		
		// back face
		private static Vector3 vxG = new Vector3(ArmX(false), ArmY(false), pieceWidth);
		private static Vector3 vxH = new Vector3(ArmX(true), ArmY(true), pieceWidth);
		private static Vector3 vxI = new Vector3(-jointX, jointY, pieceWidth);
		private static Vector3 vxJ = new Vector3(-pieceWidth, -pieceLength, pieceWidth);
		private static Vector3 vxK = new Vector3(pieceWidth, -pieceLength, pieceWidth);
		private static Vector3 vxL = new Vector3(jointX, -jointY, pieceWidth);
		
		// connection points
		private static Vector3 cpA = new Vector3(0.5f * Mathf.Sqrt(2) * pieceLength, 0.5f * Mathf.Sqrt(2) * pieceLength, 0);
		private static Vector3 cpB = new Vector3(0, -pieceLength, 0);
		static Vector3[] vertices = 
		{
			vxA, vxB, vxC, vxD, vxE, vxF, // front face
			vxG, vxH, vxI, vxJ, vxK, vxL, // back face
			vxA, vxH, vxG, vxB, // top face
			vxD, vxK, vxJ, vxE, // right face
			vxA, vxF, vxI, vxH, // left face
			vxF, vxE, vxJ, vxI, // bottom face
			vxB, vxG, vxL, vxC, // upper inner face
			vxL, vxK, vxD, vxC, // lower inner face
			
			cpA, cpB // connection points
		};
		static int[] triangles =
		{
		// front face
			0, 1, 2,
			0, 2, 5,
			2, 3, 4,
			2, 4, 5,
			
		// back face
			6, 7, 8,
			6, 8, 11,
			8, 9, 10,
			8, 10, 11,
			
		// top face
			12, 13, 14,
			12, 14, 15,
			
		// right face
			16, 17, 18,
			16, 18, 19,
			
		// left face
			20, 21, 22,
			20, 22, 23,
			
		// bottom face
			24, 25, 26,
			24, 26, 27,
			
		// upper inner face
			28, 29, 30,
			28, 30, 31,
			
		// lower inner face
			32, 33, 34,
			32, 34, 35
		};
		static List<ConnectionPoint> connectionPoints = new List<ConnectionPoint>()
		{
			new ConnectionPoint(36,new Vector3(-1,1,0), new Vector3(1,1,0)),
			new ConnectionPoint(37,new Vector3(0,0,1), new Vector3(0,-1,0)),
		};
		
		public L45RodPiece(Color color) : base("L45Rod", vertices, triangles, connectionPoints, color)
		{	
		}

		public override Piece Copy()
		{
			return new L45RodPiece(this.Color);
		}
	}
}
