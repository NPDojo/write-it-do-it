﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


namespace WriteItDoIt
{
public class ConnectionPointObject {

		public GameObject GameObject
		{
			get;
			private set;
		}

		public ConnectionPointObject(Piece p, ConnectionPoint c)
		{
			this.GameObject = new GameObject("ConnectionPoint");

			this.GameObject.tag = Constants.ConnectionPointTag;
			
			this.GameObject.AddComponent<MeshFilter>();
			this.GameObject.AddComponent<MeshRenderer>();
			this.GameObject.GetComponent<MeshRenderer>().material.SetColor("_Color", Constants.ConnectionPointColorDefault);
//			this.GameObject.GetComponent<MeshRenderer>().material.shader = Shader.Find("Transparent/Diffuse");


			ConnectionPointBehavior cpb = this.GameObject.AddComponent<ConnectionPointBehavior>();
			cpb.Piece = p;
			cpb.ConnectionPoint = c;

			Mesh mesh = new Mesh();

			if (p.IsMale) {
				mesh.vertices = OctagonalPrismConstants.maleVertices;
				mesh.triangles = OctagonalPrismConstants.maleTriangles;
			}
			else {
				mesh.vertices = OctagonalPrismConstants.femaleVertices;
				mesh.triangles = OctagonalPrismConstants.femaleTriangles;
			}
			
			Vector2[] uvs = new Vector2[mesh.vertices.Length];
			int i = 0;
			while (i < uvs.Length) {
				uvs[i] = new Vector2(mesh.vertices[i].x, mesh.vertices[i].z);
				i++;
			}
			mesh.uv = uvs;
			mesh.RecalculateNormals();
			
			this.GameObject.GetComponent<MeshFilter>().mesh = mesh;

			this.GameObject.AddComponent<Rigidbody>().isKinematic = true;

			//Collider needed for raycasting for click detection
			//Must be added after mesh
			/*
			this.GameObject.AddComponent<CapsuleCollider>().isTrigger = true;
			this.GameObject.GetComponent<CapsuleCollider> ().radius = .4f * Constants.lengthUnit;
			this.GameObject.GetComponent<CapsuleCollider> ().height = .1f * Constants.lengthUnit;
			this.GameObject.GetComponent<CapsuleCollider> ().direction = 2; // height on z-axis
			*/
			// The collider should be big enough to be easy to click but not so big that they collide each other internally
			this.GameObject.AddComponent<SphereCollider> ().isTrigger = true;
			this.GameObject.GetComponent<SphereCollider> ().radius = .3f * Constants.lengthUnit;

		}

	}
}