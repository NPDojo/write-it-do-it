﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace WriteItDoIt
{
	public class X90RodPiece : MalePiece
	{
		
		private static float pieceLength = Constants.lengthUnit * Constants.rodLengthFactor;
		private static float pieceWidth = Constants.lengthUnit * Constants.rodWidthFactor;
		
		// front face
		private static Vector3 vxA = new Vector3(-pieceWidth, pieceLength, -pieceWidth);
		private static Vector3 vxB = new Vector3(pieceWidth, pieceLength, -pieceWidth);
		private static Vector3 vxC = new Vector3(pieceWidth, pieceWidth, -pieceWidth);
		private static Vector3 vxD = new Vector3(pieceLength, pieceWidth, -pieceWidth);
		private static Vector3 vxE = new Vector3(pieceLength, -pieceWidth, -pieceWidth);
		private static Vector3 vxF = new Vector3(-pieceLength, -pieceWidth, -pieceWidth);
		private static Vector3 vxG = new Vector3(-pieceLength, pieceWidth, -pieceWidth);
		private static Vector3 vxH = new Vector3(-pieceWidth, pieceWidth, -pieceWidth);
		
		// back face
		private static Vector3 vxI = new Vector3(pieceWidth, pieceLength, pieceWidth);
		private static Vector3 vxJ = new Vector3(-pieceWidth, pieceLength, pieceWidth);
		private static Vector3 vxK = new Vector3(-pieceWidth, pieceWidth, pieceWidth);
		private static Vector3 vxL = new Vector3(-pieceLength, pieceWidth, pieceWidth);
		private static Vector3 vxM = new Vector3(-pieceLength, -pieceWidth, pieceWidth);
		private static Vector3 vxN = new Vector3(pieceLength, -pieceWidth, pieceWidth);
		private static Vector3 vxO = new Vector3(pieceLength, pieceWidth, pieceWidth);
		private static Vector3 vxP = new Vector3(pieceWidth, pieceWidth, pieceWidth);
		
		// front arm
		private static Vector3 vxQ = new Vector3(-pieceWidth, pieceWidth, -pieceLength);
		private static Vector3 vxR = new Vector3(pieceWidth, pieceWidth, -pieceLength);
		private static Vector3 vxS = new Vector3(pieceWidth, -pieceWidth, -pieceLength);
		private static Vector3 vxT = new Vector3(-pieceWidth, -pieceWidth, -pieceLength);
		private static Vector3 vxU = new Vector3(pieceWidth, -pieceWidth, -pieceWidth);
		private static Vector3 vxV = new Vector3(-pieceWidth, -pieceWidth, -pieceWidth);
		
		// connection points
		private static Vector3 cpA = new Vector3(0, pieceLength, 0); // top
		private static Vector3 cpB = new Vector3(pieceLength, 0, 0); // right
		private static Vector3 cpC = new Vector3(-pieceLength, 0, 0); // left
		private static Vector3 cpD = new Vector3(0, 0, -pieceLength); // front
		
		static Vector3[] vertices = 
		{
			vxI, vxJ, vxK, vxL, vxM, vxN, vxO, vxP, // back face; 0-7
			vxT, vxS, vxU, vxE, vxN, vxM, vxF, vxV, // bottom face; 8-15
			vxA, vxB, vxC, vxH, // front top face; 16-19
			vxC, vxD, vxE, vxU, // front right face; 20-23
			vxV, vxF, vxG, vxH, // front left face; 24-27
			vxB, vxI, vxP, vxC, // right top face; 28-31
			vxJ, vxA, vxH, vxK, // left top face; 32-35
			vxG, vxL, vxK, vxH, // top left face; 36-39
			vxC, vxP, vxO, vxD, // top right face; 40-43
			vxH, vxC, vxR, vxQ, // top front face; 44-47
			vxR, vxC, vxU, vxS, // right front face; 48-51
			vxH, vxQ, vxT, vxV, // left front face; 52-55
			vxA, vxJ, vxI, vxB, // top conn face; 56-59
			vxD, vxO, vxN, vxE, // right conn face; 60-63
			vxQ, vxR, vxS, vxT, // front conn face; 64-67
			vxL, vxG, vxF, vxM, // left conn face; 68-71
			
			cpA, cpB, cpC, cpD // connection points
		};
		static int[] triangles =
		{
			// back face
			0, 1, 2,
			0, 2, 7,
			2, 3, 4,
			7, 2, 4,
			5, 7, 4,
			5, 6, 7,
			
			// bottom face
			8, 9, 10,
			8, 10, 15,
			10, 11, 12,
			15, 10, 12,
			13, 15, 12,
			13, 14, 15,
			
			// front top face
			16, 17, 18,
			16, 18, 19,
			
			// front right face
			20, 21, 22,
			20, 22, 23,
			
			// front left face
			24, 25, 26,
			24, 26, 27,
			
			// right top face
			28, 29, 30,
			28, 30, 31,
			
			// left top face
			32, 33, 34,
			32, 34, 35,
			
			// top left face
			36, 37, 38,
			36, 38, 39,
			
			// top right face
			40, 41, 42,
			40, 42, 43,
			
			// top front face
			44, 45, 46,
			44, 46, 47,
			
			// right front face
			48, 49, 50,
			48, 50, 51,
			
			// left front face
			52, 53, 54,
			52, 54, 55,
			
			// top conn face
			56, 57, 58,
			56, 58, 59,
			
			// right conn face
			60, 61, 62,
			60, 62, 63,
			
			// front conn face
			64, 65, 66,
			64, 66, 67,
			
			// left conn face
			68, 69, 70,
			68, 70, 71
		};
		static List<ConnectionPoint> connectionPoints = new List<ConnectionPoint>()
		{
			new ConnectionPoint(72,new Vector3(0,0,1),new Vector3(0,1,0)),
			new ConnectionPoint(73,new Vector3(0,1,0),new Vector3(1,0,0)),
			new ConnectionPoint(74,new Vector3(0,1,0),new Vector3(-1,0,0)),
			new ConnectionPoint(75,new Vector3(0,1,0),new Vector3(0,0,-1))
		};
		
		public X90RodPiece(Color color) : base("X90Rod", vertices, triangles, connectionPoints, color)
		{
		}

		public override Piece Copy()
		{
			return new X90RodPiece(this.Color);
		}
	}
}