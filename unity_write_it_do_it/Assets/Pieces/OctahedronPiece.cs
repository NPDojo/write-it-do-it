﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace WriteItDoIt
{
	public class OctahedronPiece : FemalePiece {

		private static float pieceSizeFactor = 0.9f;
		private static float xCoord = Constants.lengthUnit * pieceSizeFactor;
		
		private static float cpxCoord = xCoord*(1.0f-(Mathf.Sqrt (3.0f) - 1.0f)/2.0f);
		private static float cpyCoord = xCoord*((Mathf.Sqrt (3.0f) - 1.0f)/2.0f);
		
		private static Vector3 vxA = new Vector3 (-xCoord, 0, -xCoord);
		private static Vector3 vxB = new Vector3 (xCoord, 0, -xCoord);
		private static Vector3 vxC = new Vector3 (xCoord, 0, xCoord);
		private static Vector3 vxD = new Vector3 (-xCoord, 0, xCoord);
		private static Vector3 vxE = new Vector3 (0, xCoord, 0);
		private static Vector3 vxF = new Vector3 (0, -xCoord, 0);
		
		private static Vector3 cpA = new Vector3 (0, cpyCoord, -cpxCoord); // front top
		private static Vector3 cpB = new Vector3 (cpxCoord, cpyCoord, 0); // right top
		private static Vector3 cpC = new Vector3 (0, cpyCoord, cpxCoord); // back top
		private static Vector3 cpD = new Vector3 (-cpxCoord, cpyCoord, 0); // left top
		private static Vector3 cpE = new Vector3 (0, -cpyCoord, -cpxCoord); // front bottom
		private static Vector3 cpF = new Vector3 (cpxCoord, -cpyCoord, 0); // right bottom
		private static Vector3 cpG = new Vector3 (0, -cpyCoord, cpxCoord); // back bottom
		private static Vector3 cpH = new Vector3 (-cpxCoord, -cpyCoord, 0); // left bottom

		static Vector3[] vertices = 
		{
			vxA, vxE, vxB, // front top
			vxA, vxB, vxF, // front bottom
			vxB, vxE, vxC, // right top
			vxB, vxC, vxF, // right bottom
			vxC, vxE, vxD, // back top
			vxC, vxD, vxF, // back bottom
			vxD, vxE, vxA, // left top
			vxD, vxA, vxF, // left bottom
			
			cpA, cpB, cpC, cpD, cpE, cpF, cpG, cpH
		};
		
		static int[] triangles =
		{
			0, 1, 2, // front top
			3, 4, 5, // front bottom
			6, 7, 8, // right top
			9, 10, 11, // right bottom
			12, 13, 14, // back top
			15, 16, 17, // back bottom
			18, 19, 20, // left top
			21, 22, 23 // left bottom
		};
		
		static List<ConnectionPoint> connectionPoints = new List<ConnectionPoint>()
		{
			// Up vectors point to the top tip for triangle sides, or towards +x for base

			new ConnectionPoint(24,new Vector3(0,1f/Mathf.Sqrt(2),1f/Mathf.Sqrt(2)),new Vector3(0,1f/Mathf.Sqrt(2),-1f/Mathf.Sqrt(2))),
			new ConnectionPoint(25,new Vector3(-1f/Mathf.Sqrt(2),1f/Mathf.Sqrt(2),0),new Vector3(1f/Mathf.Sqrt(2),1f/Mathf.Sqrt(2),0)),
			new ConnectionPoint(26,new Vector3(0,1f/Mathf.Sqrt(2),-1f/Mathf.Sqrt(2)),new Vector3(0,1f/Mathf.Sqrt(2),1f/Mathf.Sqrt(2))),
			new ConnectionPoint(27,new Vector3(1f/Mathf.Sqrt(2),1f/Mathf.Sqrt(2),0),new Vector3(-1f/Mathf.Sqrt(2),1f/Mathf.Sqrt(2),0)),
			new ConnectionPoint(28,new Vector3(0,-1f/Mathf.Sqrt(2),1f/Mathf.Sqrt(2)),new Vector3(0,-1f/Mathf.Sqrt(2),-1f/Mathf.Sqrt(2))),
			new ConnectionPoint(29,new Vector3(-1f/Mathf.Sqrt(2),-1f/Mathf.Sqrt(2),0),new Vector3(1f/Mathf.Sqrt(2),-1f/Mathf.Sqrt(2),0)),
			new ConnectionPoint(30,new Vector3(0,-1f/Mathf.Sqrt(2),-1f/Mathf.Sqrt(2)),new Vector3(0,-1f/Mathf.Sqrt(2),1f/Mathf.Sqrt(2))),
			new ConnectionPoint(31,new Vector3(1f/Mathf.Sqrt(2),-1f/Mathf.Sqrt(2),0),new Vector3(-1f/Mathf.Sqrt(2),-1f/Mathf.Sqrt(2),0)),
		};

		public OctahedronPiece(Color color) : base("Octahedron", vertices, triangles, connectionPoints, color)
		{
		}

		public override Piece Copy()
		{
			return new OctahedronPiece(this.Color);
		}
	}
}