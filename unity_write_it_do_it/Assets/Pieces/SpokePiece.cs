﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace WriteItDoIt
{
	public class SpokePiece : FemalePiece {
		
		private static float xySizeFactor = 1.0f;
		private static float zSizeFactor = 0.5f;
		private static float pieceWidth = Constants.lengthUnit * xySizeFactor;
		private static float pieceThickness = Constants.lengthUnit * zSizeFactor;

		private static float halfSideLength = pieceWidth * Mathf.Tan ((22.5f*Mathf.PI)/180f);

		// front face
		private static Vector3 vxA = new Vector3(halfSideLength, pieceWidth, -pieceThickness);
		private static Vector3 vxB = new Vector3(pieceWidth, halfSideLength, -pieceThickness);
		private static Vector3 vxC = new Vector3(pieceWidth, -halfSideLength, -pieceThickness);
		private static Vector3 vxD = new Vector3(halfSideLength, -pieceWidth, -pieceThickness);
		private static Vector3 vxE = new Vector3(-halfSideLength, -pieceWidth, -pieceThickness);
		private static Vector3 vxF = new Vector3(-pieceWidth, -halfSideLength, -pieceThickness);
		private static Vector3 vxG = new Vector3(-pieceWidth, halfSideLength, -pieceThickness);
		private static Vector3 vxH = new Vector3(-halfSideLength, pieceWidth, -pieceThickness);

		// back face
		private static Vector3 vxI = new Vector3(halfSideLength, pieceWidth, pieceThickness);
		private static Vector3 vxJ = new Vector3(pieceWidth, halfSideLength, pieceThickness);
		private static Vector3 vxK = new Vector3(pieceWidth, -halfSideLength, pieceThickness);
		private static Vector3 vxL = new Vector3(halfSideLength, -pieceWidth, pieceThickness);
		private static Vector3 vxM = new Vector3(-halfSideLength, -pieceWidth, pieceThickness);
		private static Vector3 vxN= new Vector3(-pieceWidth, -halfSideLength, pieceThickness);
		private static Vector3 vxO = new Vector3(-pieceWidth, halfSideLength, pieceThickness);
		private static Vector3 vxP = new Vector3(-halfSideLength, pieceWidth, pieceThickness);

		private static Vector3 cpA = new Vector3 (0, pieceWidth, 0); // 12:00
		private static Vector3 cpB = new Vector3 (pieceWidth/Mathf.Sqrt(2), pieceWidth/Mathf.Sqrt(2), 0); // 1:30
		private static Vector3 cpC = new Vector3 (pieceWidth, 0, 0); // 3:00
		private static Vector3 cpD = new Vector3 (pieceWidth/Mathf.Sqrt(2), -pieceWidth/Mathf.Sqrt(2), 0); // 4:30
		private static Vector3 cpE = new Vector3 (0, -pieceWidth, 0); // 6:00
		private static Vector3 cpF = new Vector3 (-pieceWidth/Mathf.Sqrt(2), -pieceWidth/Mathf.Sqrt(2), 0); // 7:30
		private static Vector3 cpG = new Vector3 (-pieceWidth, 0, 0); // 9:00
		private static Vector3 cpH = new Vector3 (-pieceWidth/Mathf.Sqrt(2), pieceWidth/Mathf.Sqrt(2), 0); // 10:30
		private static Vector3 cpI = new Vector3 (0,0,-pieceThickness);
		private static Vector3 cpJ = new Vector3 (0,0,pieceThickness);
		
		static Vector3[] vertices = 
		{
			vxA, vxB, vxC, vxD, vxE, vxF, vxG, vxH, // front face
			vxP, vxO, vxN, vxM, vxL, vxK, vxJ, vxI, // back face
			vxA, vxH, vxP, vxI, // 12:00
			vxB, vxA, vxI, vxJ, // 1:30
			vxC, vxB, vxJ, vxK, // 3:00
			vxD, vxC, vxK, vxL, // 4:30
			vxE, vxD, vxL, vxM, // 6:00
			vxF, vxE, vxM, vxN, // 7:30
			vxG, vxF, vxN, vxO, // 9:00
			vxH, vxG, vxO, vxP, // 10:30

			cpA, cpB, cpC, cpD, cpE, cpF, cpG, cpH, cpI, cpJ // connection points
		};
		
		static int[] triangles =
		{
			// front face
			0, 1, 2,
			0, 2, 3,
			0, 3, 4,
			0, 4, 5,
			0, 5, 6,
			0, 6, 7,

			// back face
			8, 9, 10,
			8, 10, 11,
			8, 11, 12,
			8, 12, 13,
			8, 13, 14,
			8, 14, 15,

			// 12:00
			16, 17, 18,
			16, 18, 19,

			// 1:30
			20, 21, 22,
			20, 22, 23,

			// 3:00
			24, 25, 26,
			24, 26, 27,

			// 4:30
			28, 29, 30,
			28, 30, 31,

			// 6:00
			32, 33, 34,
			32, 34, 35,

			// 7:30
			36, 37, 38,
			36, 38, 39,

			// 9:00
			40, 41, 42,
			40, 42, 43,

			// 10:30
			44, 45, 46,
			44, 46, 47
		};
		
		static List<ConnectionPoint> connectionPoints = new List<ConnectionPoint>()
		{
			new ConnectionPoint (48, new Vector3(0, 0, 1), new Vector3(0,1,0)),
			new ConnectionPoint (49, new Vector3(0, 0, 1), new Vector3 (1f/Mathf.Sqrt(2), 1f/Mathf.Sqrt(2), 0)),
			new ConnectionPoint (50, new Vector3(0, 0, 1), new Vector3(1,0,0)),
			new ConnectionPoint (51, new Vector3(0, 0, 1), new Vector3 (1f/Mathf.Sqrt(2), -1f/Mathf.Sqrt(2), 0)),
			new ConnectionPoint (52, new Vector3(0, 0, 1), new Vector3(0,-1,0)),
			new ConnectionPoint (53, new Vector3(0, 0, 1), new Vector3 (-1f/Mathf.Sqrt(2), -1f/Mathf.Sqrt(2), 0)),
			new ConnectionPoint (54, new Vector3(0, 0, 1), new Vector3(-1,0,0)),
			new ConnectionPoint (55, new Vector3(0, 0, 1), new Vector3 (-1f/Mathf.Sqrt(2), 1f/Mathf.Sqrt(2), 0)),
			new ConnectionPoint (56, new Vector3(1, 0, 0), new Vector3(0, 0, -1)),
			new ConnectionPoint (57, new Vector3(1, 0, 0), new Vector3(0, 0, 1)),
		};
		
		public SpokePiece(Color color) : base("Spoke", vertices, triangles, connectionPoints, color)
		{
		}

		public override Piece Copy()
		{
			return new SpokePiece(this.Color);
		}
	}
}