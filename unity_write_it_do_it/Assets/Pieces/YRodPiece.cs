using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace WriteItDoIt
{
	public class YRodPiece : MalePiece
	{
		
		private static float pieceLength = Constants.lengthUnit * Constants.rodLengthFactor;
		private static float pieceWidth = Constants.lengthUnit * Constants.rodWidthFactor;
		private static float jointX = pieceWidth;
		private static float jointY = pieceWidth * Mathf.Tan(Mathf.PI / 8);
		private static float centerY = pieceWidth + jointY;
		private static float cpXY = 0.5f * Mathf.Sqrt (2) * pieceLength;
		
		private static float ArmX(bool isTop)
		{
			float ans;
			if (isTop) {
				ans = 0.5f * Mathf.Sqrt(2) * (pieceLength - pieceWidth);
			} else {
				ans = 0.5f * Mathf.Sqrt(2) * (pieceLength + pieceWidth);
			}
			return ans;
		}
		
		private static float ArmY(bool isTop)
		{
			return ArmX(!isTop);
		}
		
		// front face
		private static Vector3 vxA = new Vector3 (ArmX(true), ArmY(true), -pieceWidth);
		private static Vector3 vxB = new Vector3 (ArmX(false), ArmY(false), -pieceWidth);
		private static Vector3 vxC = new Vector3 (jointX, -jointY, -pieceWidth);
		private static Vector3 vxD = new Vector3 (pieceWidth, -pieceLength, -pieceWidth);
		private static Vector3 vxE = new Vector3 (-pieceWidth, -pieceLength, -pieceWidth);
		private static Vector3 vxF = new Vector3 (-jointX, -jointY, -pieceWidth);
		private static Vector3 vxG = new Vector3 (-ArmX (false), ArmY (false), -pieceWidth);
		private static Vector3 vxH = new Vector3 (-ArmX (true), ArmY (true), -pieceWidth);
		private static Vector3 vxI = new Vector3 (0, centerY, -pieceWidth);
		
		// back face
		private static Vector3 vxJ = new Vector3 (ArmX(false), ArmY(false), pieceWidth);
		private static Vector3 vxK = new Vector3 (ArmX (true), ArmY (true), pieceWidth);
		private static Vector3 vxL = new Vector3 (0, centerY, pieceWidth);
		private static Vector3 vxM = new Vector3 (-ArmX(true), ArmY(true), pieceWidth);
		private static Vector3 vxN = new Vector3 (-ArmX(false), ArmY(false), pieceWidth);
		private static Vector3 vxO = new Vector3 (-jointX, -jointY, pieceWidth);
		private static Vector3 vxP = new Vector3 (-pieceWidth, -pieceLength, pieceWidth);
		private static Vector3 vxQ = new Vector3 (pieceWidth, -pieceLength, pieceWidth);
		private static Vector3 vxR = new Vector3 (jointX, -jointY, pieceWidth);
		
		// connection points
		private static Vector3 cpA = new Vector3 (cpXY, cpXY, 0);
		private static Vector3 cpB = new Vector3 (0, -pieceLength, 0);
		private static Vector3 cpC = new Vector3 (-cpXY, cpXY, 0);

		static Vector3[] vertices = 
		{
			vxA, vxB, vxC, vxD, vxE, vxF, vxG, vxH, vxI, // front face; 0-8
			vxJ, vxK, vxL, vxM, vxN, vxO, vxP, vxQ, vxR, // back face; 9-17
			vxA, vxK, vxJ, vxB, // top right face; 18-21
			vxE, vxD, vxQ, vxP, // bottom face; 22-25
			vxH, vxG, vxN, vxM, // top left face; 26-29
			vxC, vxB, vxJ, vxR, // right face top; 30-33
			vxD, vxC, vxR, vxQ, // right face bottom; 34-37
			vxG, vxF, vxO, vxN, // left face top; 38-41
			vxF, vxE, vxP, vxO, // left face bottom; 42-45
			vxA, vxI, vxL, vxK, // top face right; 46-49
			vxI, vxH, vxM, vxL, // top face left; 50-53
			
			cpA, cpB, cpC // connection points
		};
		static int[] triangles =
		{
			// front face
			0, 1, 2,
			0, 2, 8,
			2, 3, 4,
			2, 4, 5,
			5, 6, 7,
			5, 7, 8,
			2, 5, 8,
			
			// back face
			9, 10, 11,
			9, 11, 17,
			11, 12, 13,
			11, 13, 14,
			14, 15, 16,
			14, 16, 17,
			11, 14, 17,
			
			// top right face
			18, 19, 20,
			18, 20, 21,
			
			// bottom face
			22, 23, 24,
			22, 24, 25,
			
			// top left face
			26, 27, 28,
			26, 28, 29,
			
			// right face top
			30, 31, 32,
			30, 32, 33,
			
			// right face bottom
			34, 35, 36,
			34, 36, 37,
			
			// left face top
			38, 39, 40,
			38, 40, 41,

			// left face bottom
			42, 43, 44,
			42, 44, 45,

			// top face right
			46, 47, 48,
			46, 48, 49,

			// top face left
			50, 51, 52,
			50, 52, 53
		};
		static List<ConnectionPoint> connectionPoints = new List<ConnectionPoint>()
		{
			new ConnectionPoint(54,new Vector3(-1,1,0), new Vector3(1,1,0)),
			new ConnectionPoint(55,new Vector3(0,0,1), new Vector3(0,-1,0)),
			new ConnectionPoint(56,new Vector3(1,1,0), new Vector3(-1,1,0))
		};
		
		public YRodPiece(Color color) : base("YRod", vertices, triangles, connectionPoints, color)
		{
		}

		public override Piece Copy()
		{
			return new YRodPiece(this.Color);
		}
	}
}

