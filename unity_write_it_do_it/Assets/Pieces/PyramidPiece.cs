﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace WriteItDoIt
{
	public class PyramidPiece : FemalePiece {
		
		private static float pieceSizeFactor = 0.9f;
		private static float xCoord = Constants.lengthUnit * pieceSizeFactor;

		private static float cpxCoord = xCoord*(1.0f-(Mathf.Sqrt (3.0f) - 1.0f)/2.0f);
		private static float cpyCoord = xCoord*((Mathf.Sqrt (3.0f) - 1.0f)/2.0f);

		private static Vector3 vxA = new Vector3 (-xCoord, 0, -xCoord); // Front Left
		private static Vector3 vxB = new Vector3 (xCoord, 0, -xCoord); // Front Right
		private static Vector3 vxC = new Vector3 (xCoord, 0, xCoord); // Back Right
		private static Vector3 vxD = new Vector3 (-xCoord, 0, xCoord); // Back left
		private static Vector3 vxE = new Vector3 (0, xCoord, 0); // Apex

		private static Vector3 cpA = new Vector3 (0, cpyCoord, -cpxCoord); // front slope
		private static Vector3 cpB = new Vector3 (cpxCoord, cpyCoord, 0); // right slope
		private static Vector3 cpC = new Vector3 (0, cpyCoord, cpxCoord); // back slope
		private static Vector3 cpD = new Vector3 (-cpxCoord, cpyCoord, 0); // left slope
		private static Vector3 cpE = new Vector3 (0, 0, 0); // bottom face
		
		static Vector3[] vertices = 
		{
			vxA, vxB, vxC, vxD, // bottom face
			vxA, vxE, vxB, // front slope
			vxB, vxE, vxC, // right slope
			vxC, vxE, vxD, // back slope
			vxD, vxE, vxA, // left slope

			cpA, cpB, cpC, cpD, cpE
		};
		
		static int[] triangles =
		{
			// bottom face
			0, 1, 2,
			0, 2, 3,

			// front slope
			4, 5, 6,

			// right slope
			7, 8, 9,

			// back slope
			10, 11, 12,

			// left slope
			13, 14, 15
		};
		
		static List<ConnectionPoint> connectionPoints = new List<ConnectionPoint>()
		{
			// Up vectors point to the top tip for triangle sides, or towards +x for base
			new ConnectionPoint(16,new Vector3(0,1f/Mathf.Sqrt(2),1f/Mathf.Sqrt(2)),new Vector3(0,1f/Mathf.Sqrt(2),-1f/Mathf.Sqrt(2))),
			new ConnectionPoint(17,new Vector3(-1f/Mathf.Sqrt(2),1f/Mathf.Sqrt(2),0),new Vector3(1f/Mathf.Sqrt(2),1f/Mathf.Sqrt(2),0)),
			new ConnectionPoint(18,new Vector3(0,1f/Mathf.Sqrt(2),-1f/Mathf.Sqrt(2)),new Vector3(0,1f/Mathf.Sqrt(2),1f/Mathf.Sqrt(2))),
			new ConnectionPoint(19,new Vector3(1f/Mathf.Sqrt(2),1f/Mathf.Sqrt(2),0),new Vector3(-1f/Mathf.Sqrt(2),1f/Mathf.Sqrt(2),0)),
			new ConnectionPoint(20,new Vector3(1,0,0),new Vector3(0,-1,0))
		};
		
		public PyramidPiece(Color color) : base("Pyramid", vertices, triangles, connectionPoints, color)
		{
		}

		public override Piece Copy()
		{
			return new PyramidPiece(this.Color);
		}
	}
}