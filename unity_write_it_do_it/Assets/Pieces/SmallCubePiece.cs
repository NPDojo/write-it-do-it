﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace WriteItDoIt
{
public class SmallCubePiece : FemalePiece {

		private static float xSizeFactor = 0.5f;
		
		private static float pieceWidth = Constants.lengthUnit * xSizeFactor;
		
		// top face
		private static Vector3 vxA = new Vector3(-pieceWidth, pieceWidth, -pieceWidth);
		private static Vector3 vxB = new Vector3(-pieceWidth, pieceWidth, pieceWidth);
		private static Vector3 vxC = new Vector3(pieceWidth, pieceWidth, pieceWidth);
		private static Vector3 vxD = new Vector3(pieceWidth, pieceWidth, -pieceWidth);
		
		// bottom face
		private static Vector3 vxE = new Vector3(-pieceWidth, -pieceWidth, -pieceWidth);
		private static Vector3 vxF = new Vector3(pieceWidth, -pieceWidth, -pieceWidth);
		private static Vector3 vxG = new Vector3(pieceWidth, -pieceWidth, pieceWidth);
		private static Vector3 vxH = new Vector3(-pieceWidth, -pieceWidth, pieceWidth);
		
		private static Vector3 cpA = new Vector3 (0, pieceWidth, 0); // top
		private static Vector3 cpB = new Vector3 (0, -pieceWidth, 0); // bottom
		private static Vector3 cpC = new Vector3 (0, 0, pieceWidth); // back
		private static Vector3 cpD = new Vector3 (0, 0, -pieceWidth); // front
		private static Vector3 cpE = new Vector3 (pieceWidth, 0, 0); // right
		private static Vector3 cpF = new Vector3 (-pieceWidth, 0, 0); // left
		
		static Vector3[] vertices = 
		{
			vxA, vxB, vxC, vxD, // top face
			vxA, vxD, vxF, vxE, // front face
			vxB, vxA, vxE, vxH, // left face
			vxC, vxB, vxH, vxG, // back face
			vxE, vxF, vxG, vxH, // bottom face
			vxD, vxC, vxG, vxF, // right face

			cpA, cpB, cpC, cpD, cpE, cpF // connection points
		};
		
		static int[] triangles =
		{
			0, 1, 2, // top face
			0, 2, 3,

			4, 5, 6, // front face
			4, 6, 7,

			8, 9, 10, // left face
			8, 10, 11,

			12, 13, 14, // back face
			12, 14, 15,

			16, 17, 18, // bottom face
			16, 18, 19,

			20, 21, 22, // right face
			20, 22, 23
		};
		
		static List<ConnectionPoint> connectionPoints = new List<ConnectionPoint>()
		{
			new ConnectionPoint (24, new Vector3(0, 0, 1), new Vector3(0, 1, 0)),
			new ConnectionPoint (25, new Vector3(0, 0, 1), new Vector3(0, -1, 0)),
			new ConnectionPoint (26, new Vector3(0, 1, 0), new Vector3(0, 0, 1)),
			new ConnectionPoint (27, new Vector3(0, 1, 0), new Vector3(0, 0, -1)),
			new ConnectionPoint (28, new Vector3(0, 1, 0), new Vector3(1, 0, 0)),
			new ConnectionPoint (29, new Vector3(0, 1, 0), new Vector3(-1, 0, 0))
		};
		
		public SmallCubePiece(Color color) : base("SmallCube", vertices, triangles, connectionPoints, color)
		{	
		}

		public override Piece Copy()
		{
			return new SmallCubePiece(this.Color);
		}
	}
}