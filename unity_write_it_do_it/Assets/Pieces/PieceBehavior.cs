using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace WriteItDoIt
{
	public class PieceBehavior : MonoBehaviour
	{
//		float posLerpCoefficient = 0.07f;
//		float rotLerpCoefficient = 0.01f;
//		float maxLerpDuration = 0.7f;
	
		public Piece Piece
		{
			get;
			set;
		}

		public bool IsLerping {
			get;
			private set;
		}

		private int touches {
			get;
			set;
		}

		private HashSet<Collider> piecesTouching {
			get;
			set;
		}

		//Used to force user to fix "problem" pieces (intersecting/colliding)
		public bool IsProblem {
			get {
				return this.touches > 0;
			}
		}

		public PieceBehavior()
		{
			piecesTouching = new HashSet<Collider> ();
			touches = 0;
		}
		
		// Runs when the game object (a piece) touches another game object that has a collider (a piece or connection point)
		public void OnTriggerEnter(Collider c)
		{
			if (!Utilities.IsConnectionPoint(c.gameObject)) {
				this.gameObject.renderer.material.color = Constants.OverlappingPieceColor;

				touches++;
				piecesTouching.Add (c);
				if(touches == 1) GameManager.CurrentGame.OverlappingPieceCount++;
				Debug.Log("Piece triggered: " + Piece.GetType() + "; " + touches + " touches; "
				          + GameManager.CurrentGame.OverlappingPieceCount + " pieces overlapping");
			}
		}

		// Runs when another collider leaves this collider's space
		public void OnTriggerExit(Collider c)
		{
			if (piecesTouching.Contains (c)) {
				touches--;
				piecesTouching.Remove (c);

				if(touches == 0) { // If this piece is no longer touching anything
					GameManager.CurrentGame.OverlappingPieceCount--;

					// Revert color back to normal
					if (object.ReferenceEquals(GameManager.CurrentGame.CurrentSelected, this.gameObject))
					{
						// This piece is the currently selected one, so set it to the selected piece color
						this.gameObject.renderer.material.color = Constants.PieceColorSelected;
					} else {
						this.gameObject.renderer.material.color = this.Piece.Color;
					}
				}
				Debug.Log("Exit trigger: " + Piece.GetType() + "; " + touches + " touches; "
				          + GameManager.CurrentGame.OverlappingPieceCount + " pieces overlapping");
			}
		}
	
		public void LerpTo(Quaternion q, Vector3 pos, float lerpSpeed, Action callback = null)
		{
			if (!IsLerping) {
				IsLerping = true;
				StartCoroutine(LerpTo_(q, pos, lerpSpeed, callback));
			}
		}

		private IEnumerator LerpTo_(Quaternion q, Vector3 pos, float lerpTime, Action callback = null)
		{
			float start = Time.time;
			
			while (Time.time < start + lerpTime) {
				float perc = (Time.time - start)/lerpTime;
				transform.rotation = Quaternion.Lerp(transform.rotation, q, perc);
				transform.position = Vector3.Lerp(transform.position, pos, perc);
				yield return null;
			}
			transform.rotation = q;
			transform.position = pos;
			IsLerping = false;

			if (callback != null) 
			{
				callback();
			}
		}		
	}
}

