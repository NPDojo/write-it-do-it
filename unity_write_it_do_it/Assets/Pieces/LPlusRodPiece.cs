﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace WriteItDoIt
{
	public class LPlusRodPiece : MalePiece {
		
		private static float pieceLength = Constants.lengthUnit * Constants.rodLengthFactor;
		private static float pieceWidth = Constants.lengthUnit * Constants.rodWidthFactor;
		
		// front face
		private static Vector3 vxA = new Vector3 (-pieceWidth, pieceLength, -pieceWidth);
		private static Vector3 vxB = new Vector3 (pieceWidth, pieceLength, -pieceWidth);
		private static Vector3 vxC = new Vector3 (pieceWidth, pieceWidth, -pieceWidth);
		private static Vector3 vxD = new Vector3 (pieceLength, pieceWidth, -pieceWidth);
		private static Vector3 vxE = new Vector3 (pieceLength, -pieceWidth, -pieceWidth);
		private static Vector3 vxF = new Vector3 (-pieceWidth, -pieceWidth, -pieceWidth);
		
		// back face
		private static Vector3 vxG = new Vector3 (pieceWidth, pieceLength, pieceWidth);
		private static Vector3 vxH = new Vector3 (-pieceWidth, pieceLength, pieceWidth);
		private static Vector3 vxI = new Vector3 (-pieceWidth, -pieceWidth, pieceWidth);
		private static Vector3 vxJ = new Vector3 (pieceLength, -pieceWidth, pieceWidth);
		private static Vector3 vxK = new Vector3 (pieceLength, pieceWidth, pieceWidth);
		private static Vector3 vxL = new Vector3 (pieceWidth, pieceWidth, pieceWidth);
		
		// front arm
		private static Vector3 vxM = new Vector3 (-pieceWidth, pieceWidth, -pieceLength);
		private static Vector3 vxN = new Vector3 (pieceWidth, pieceWidth, -pieceLength);
		private static Vector3 vxO = new Vector3 (pieceWidth, -pieceWidth, -pieceLength);
		private static Vector3 vxP = new Vector3 (-pieceWidth, -pieceWidth, -pieceLength);
		private static Vector3 vxQ = new Vector3 (-pieceWidth, pieceWidth, -pieceWidth);
		private static Vector3 vxR = new Vector3 (pieceWidth, -pieceWidth, -pieceWidth);
		
		// connection points
		private static Vector3 cpA = new Vector3 (0, pieceLength, 0);
		private static Vector3 cpB = new Vector3 (pieceLength, 0, 0);
		private static Vector3 cpC = new Vector3 (0, 0, -pieceLength);
		
		static Vector3[] vertices = 
		{
			vxA, vxB, vxC, vxD, vxE, vxF, // front face
			vxG, vxH, vxI, vxJ, vxK, vxL, // back face
			vxA, vxH, vxG, vxB, // top face
			vxD, vxK, vxJ, vxE, // right face
			vxA, vxF, vxI, vxH, // left face
			vxF, vxE, vxJ, vxI, // bottom face
			vxB, vxG, vxL, vxC, // upper inner face
			vxL, vxK, vxD, vxC, // lower inner face
			vxM, vxN, vxO, vxP, // front conn face
			vxM, vxQ, vxC, vxN, // front top face
			vxN, vxC, vxR, vxO, // front right face
			vxO, vxR, vxF, vxP, // front bottom face
			vxP, vxF, vxQ, vxM, // front left face
			
			cpA, cpB, cpC // connection points
		};
		
		static int[] triangles =
		{
			// front face
			0, 1, 2,
			0, 2, 5,
			2, 3, 4,
			2, 4, 5,
			
			// back face
			6, 7, 8,
			6, 8, 11,
			8, 9, 10,
			8, 10, 11,
			
			// top face
			12, 13, 14,
			12, 14, 15,
			
			// right face
			16, 17, 18,
			16, 18, 19,
			
			// left face
			20, 21, 22,
			20, 22, 23,
			
			// bottom face
			24, 25, 26,
			24, 26, 27,
			
			// upper inner face
			28, 29, 30,
			28, 30, 31,
			
			// lower inner face
			32, 33, 34,
			32, 34, 35,
			
			// front conn face
			36, 37, 38,
			36, 38, 39,
			
			// front top face
			40, 41, 42,
			40, 42, 43,
			
			// front right face
			44, 45, 46,
			44, 46, 47,
			
			// front bottom face
			48, 49, 50,
			48, 50, 51,
			
			// front left face
			52, 53, 54,
			52, 54, 55
		};
		
		static List<ConnectionPoint> connectionPoints = new List<ConnectionPoint>()
		{
			new ConnectionPoint(56,new Vector3(0,0,1), new Vector3(0,1,0)),
			new ConnectionPoint(57,new Vector3(0,1,0), new Vector3(1,0,0)),
			new ConnectionPoint(58,new Vector3(0,1,0), new Vector3(0,0,-1))
		};
		
		public LPlusRodPiece(Color color) : base("LPlusRod", vertices, triangles, connectionPoints, color)
		{
		}

		public override Piece Copy()
		{
			return new LPlusRodPiece(this.Color);
		}
	}
}  