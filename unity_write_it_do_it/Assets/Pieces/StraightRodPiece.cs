﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace WriteItDoIt
{
	public class StraightRodPiece : MalePiece {
		
		private static float pieceWidth = Constants.lengthUnit * Constants.rodWidthFactor;
		private static float pieceLength = Constants.lengthUnit * Constants.rodLengthFactor;
		
		private static Vector3 vxA = new Vector3(-pieceLength, pieceWidth, -pieceWidth);
		private static Vector3 vxB = new Vector3(pieceLength, pieceWidth, -pieceWidth);
		private static Vector3 vxC = new Vector3(pieceLength, pieceWidth, pieceWidth);
		private static Vector3 vxD = new Vector3(-pieceLength, pieceWidth, pieceWidth);
		private static Vector3 vxE = new Vector3(-pieceLength, -pieceWidth, -pieceWidth);
		private static Vector3 vxF = new Vector3(pieceLength, -pieceWidth, -pieceWidth);
		private static Vector3 vxG = new Vector3(pieceLength, -pieceWidth, pieceWidth);
		private static Vector3 vxH = new Vector3(-pieceLength, -pieceWidth, pieceWidth);

		private static Vector3 cpA = new Vector3 (pieceLength, 0, 0); // right
		private static Vector3 cpB = new Vector3 (-pieceLength, 0, 0); // left
		
		static Vector3[] vertices = 
		{
			vxA, vxD, vxC, vxB, // top face
			vxA, vxB, vxF, vxE, // front face
			vxD, vxA, vxE, vxH, // left face
			vxC, vxD, vxH, vxG, // back face
			vxE, vxF, vxG, vxH, // bottom face
			vxB, vxC, vxG, vxF, // right face
			
			cpA, cpB // connection points
		};
		
		static int[] triangles =
		{
			0, 1, 2, // top face
			0, 2, 3,
			
			4, 5, 6, // front face
			4, 6, 7,
			
			8, 9, 10, // left face
			8, 10, 11,
			
			12, 13, 14, // back face
			12, 14, 15,
			
			16, 17, 18, // bottom face
			16, 18, 19,
			
			20, 21, 22, // right face
			20, 22, 23
		};

		static List<ConnectionPoint> connectionPoints = new List<ConnectionPoint>()
		{
			new ConnectionPoint (24, new Vector3(0, 1, 0), new Vector3(1, 0, 0)),
			new ConnectionPoint (25, new Vector3(0, 1, 0), new Vector3(-1, 0, 0))
		};

		public StraightRodPiece(Color color) : base("StraightRod", vertices, triangles, connectionPoints, color)
		{
		}

		public override Piece Copy()
		{
			return new StraightRodPiece(this.Color);
		}
	}
}