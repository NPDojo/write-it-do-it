﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using System.Linq;
using UnityEngine.UI;

namespace WriteItDoIt
{
	public class Utilities : MonoBehaviour
	{
		//TODO find a better home
		//delegate the returns a delegate
		public delegate UnityEngine.Events.UnityAction DynamicOnlickAction(object item);
		public delegate string ItemToString(object item);
		
		public static void DynamicDropdown(Transform parentPanel, IEnumerable items, 
		                            ItemToString itemToString, DynamicOnlickAction onclickAction, bool asGrid = false)
		{
			int index = 0;
			// Create first page
			UnityEngine.Object buttonPrefab = Resources.Load("PieceButton");
			UnityEngine.Object panelPrefab;
			if (asGrid) panelPrefab = Resources.Load ("DynamicDropdownGridPagePanel");
			else panelPrefab = Resources.Load("DynamicDropdownPagePanel");

			GameObject panel = (GameObject)Instantiate(panelPrefab);
			panel.transform.SetParent(parentPanel, false);
			
			GameObject firstPage = panel;
			
			foreach (object item in items) {
				// if it's time for a new page
				if (index >= Constants.DropDownPageSize && index % Constants.DropDownPageSize == 0 && items.Cast<object>().Count<object>() > Constants.DropDownPageSize + 1) {
					GameObject oldPanel = panel;
					// Create new page panel
					panel = (GameObject)Instantiate(panelPrefab);
					panel.transform.SetParent(parentPanel, false);
					panel.SetActive(false); // Every panel but the first should start out inactive
					
					// Create next-page button for previous panel
					GameObject nextPanel = panel;
					GameObject pageButton = (GameObject)Instantiate(buttonPrefab);
					pageButton.GetComponentInChildren<Text>().text = "Page " + index / Constants.DropDownPageSize + " (Next)";
					pageButton.GetComponent<Button>().onClick.AddListener(
						() => {
						oldPanel.SetActive(false);
						nextPanel.SetActive(true);}
					);
					pageButton.transform.SetParent(oldPanel.transform, false);
				}
				// Make the new button
				GameObject butt = (GameObject)Instantiate(buttonPrefab);
				butt.GetComponentInChildren<Text>().text = itemToString(item);
				butt.GetComponent<Button>().onClick.AddListener(onclickAction(item));
				butt.transform.SetParent(panel.transform, false);
				index++;
			}
			if (panel != firstPage) { // if there is more than one page
				// Make a button in the last page leading back to the first
				GameObject pageButton = (GameObject)Instantiate(buttonPrefab);
				pageButton.GetComponentInChildren<Text>().text = "Page " + (index / Constants.DropDownPageSize + 1) + " (First)";
				pageButton.GetComponent<Button>().onClick.AddListener(
					() => {
					panel.SetActive(false);
					firstPage.SetActive(true);}
				);
				pageButton.transform.SetParent(panel.transform, false);
			}
		}

		public static void DynamicRecordDropdown(Transform parentPanel, List<ScorePersistenceData> records)
		{
			int index = 0, pageSize;
			UnityEngine.Object recordPrefab = Resources.Load("Record");
			UnityEngine.Object panelPrefab = Resources.Load("DynamicDropdownRecordPanel");

			// Determine ideal pageSize based on parentPanel size
			float scale = GameObject.Find ("Canvas").GetComponent<Canvas> ().scaleFactor;
			float parentHeight = parentPanel.GetComponent<RectTransform> ().rect.height * scale;
			GameObject temp = (GameObject)Instantiate (recordPrefab);
			float heightPerRecord = 30f * scale; //temp.GetComponent<RectTransform> ().rect.height * scale;
			GameObject.Destroy (temp);
			pageSize = Mathf.FloorToInt(parentHeight / heightPerRecord);

			// Create first page
			GameObject panel = (GameObject)Instantiate(panelPrefab);
			panel.transform.SetParent(parentPanel, false);

			for (index = 0; index < records.Count; index++) {
				// Grab records starting from the end so that it's ordered newest-first
				ScorePersistenceData record = records[records.Count - 1 - index];
				if (index >= pageSize && index % pageSize == 0) { // if it's time for a new page
					// Create new page panel
					panel = (GameObject)Instantiate(panelPrefab);
					panel.transform.SetParent(parentPanel, false);
					panel.SetActive(false); // Every panel but the first should start out inactive
				}
				// Make the new record
				GameObject rec = (GameObject)Instantiate(recordPrefab);
				rec.transform.GetChild (0).GetComponent<Text>().text = record.name;
				rec.transform.GetChild (1).GetComponent<Text>().text = record.dateTime.ToString();
				rec.transform.GetChild (2).GetComponent<Text>().text = record.score.ToString ();
				rec.transform.GetChild (3).GetComponent<Text>().text = record.possibleScore.ToString ();

				rec.transform.SetParent(panel.transform, false);
			}
		}

		public static Model DeserializeModelFromString(string s)
		{
			byte[] data = Convert.FromBase64String(s);
			MemoryStream memstr = new MemoryStream(data);
			ModelStream ms = new ModelStream(memstr);

			Model m = ms.ReadModel();

			ms.Close();

			return m;
		}

		public static Piece DeserializePieceFromString(string s)
		{
			byte[] data = Convert.FromBase64String(s);
			MemoryStream memstr = new MemoryStream(data);
			PieceStream ps = new PieceStream(memstr);

			Piece p = ps.ReadPiece();

			ps.Close();

			return p;
		}

		public static string SerializeModelToString(Model m)
		{
			string result = null;
			MemoryStream memstr = new MemoryStream();
			ModelStream ms = new ModelStream(memstr);

			ms.WriteModel(m);
			result = Convert.ToBase64String(memstr.ToArray());
			ms.Close();

			return result;
		}

		public static string SerializePieceToString(Piece p)
		{
			string result = null;
			MemoryStream memstr = new MemoryStream();
			PieceStream pstream = new PieceStream(memstr);

			pstream.WritePiece(p);
			result = Convert.ToBase64String(memstr.ToArray());
			pstream.Close();

			return result;
		}

		public static byte IndexForPieceType(Type pt)
		{
			for (int i = 0; i < Constants.PieceTypeMap.Length; i++)
				if (Constants.PieceTypeMap [i] == pt)
					return (byte)i;
			
			return (byte)Constants.PieceTypeMap.Length;
			//If this ever happens we forgot to add new piece to map
		}
		
		public static byte IndexForPieceType(Piece p)
		{
			return IndexForPieceType(p.GetType());
		}
		
		public static byte IndexForPieceColor(Color c)
		{
			for (int i = 0; i < Constants.pieceColors.Length; i++)
				if (Constants.pieceColors[i] == c)
					return (byte)i;
			
			return (byte)Constants.pieceColors.Length;
		}

		public static void debugConnections(Piece p) {
			Debug.Log (p.Name + " has " + p.Connections.Count + " connections");
			foreach (Connection c in p.Connections) {
				Debug.Log(p.Name + " vertex: " + c.ParentConnectionPoint.VertexIndex + " and "
				          + c.ConnectionPoint.Piece.Name + " vertex: " + c.ConnectionPoint.VertexIndex);
			}
		}

		public static bool IsConnectedTo(Piece p1, Piece p2, out Connection con)
		{
			con = null;
			
			foreach (Connection c in p1.Connections) 
			{
				if (c.ParentPiece == p2 || c.OtherPiece == p2)
				{
					con = c;
					return true;
				}
			}
			
			return false;
		}

		public static List<Piece> adjacentPieces(Piece p) {
			List<Piece> result = new List<Piece> ();
			foreach (Connection c in p.Connections) result.Add (c.OtherPiece);
			return result;
		}

		// Does a simple breadth first search to determine pieces reachable from the given piece
		// The optional "ignore" argument will not be traversed, and will not be included in output
		public static List<Piece> reachablePieces(Piece p, Piece ignore = null) {
			List<Piece> visited = new List<Piece> ();
			List<Piece> unvisited = new List<Piece> ();
			if (ignore != null) visited.Add (ignore);
			unvisited.Add (p);
			while (unvisited.Count > 0) {
				Piece current = unvisited[0];
				unvisited.RemoveAt(0);
				visited.Add (current);
				foreach (Connection c in current.Connections) {
					Piece o = c.OtherPiece;
					if(!visited.Contains (o) && !unvisited.Contains (o)) unvisited.Add (o);
				}
			}
			if (ignore != null) visited.Remove (ignore);
			return visited;
		}

		// Merges the parent structures of two pieces
		public static Transform mergeStructures(Piece p1, Piece p2) {
			if (p1.GameObject.transform.parent.childCount > p2.GameObject.transform.parent.childCount)
				return Utilities.mergeStructures(p2, p1);
			else if (p1.GameObject.transform.parent == p2.GameObject.transform.parent)
				return p1.GameObject.transform;
			else {
				Transform str1 = p1.GameObject.transform.parent;
				Transform str2 = p2.GameObject.transform.parent;
				List<Transform> children = new List<Transform> ();
				foreach (Transform child in str1) {children.Add (child);}
				foreach (Transform child in children) {child.SetParent (str2, true);}
				GameObject.Destroy (str1.gameObject); // Destroy the empty old structure
				return str2;
			}
		}

		public static bool IsConnectionPoint(GameObject go)
		{
			return go != null && go.CompareTag(Constants.ConnectionPointTag);
		}

		public static void Swap<T>(ref T a, ref T b)
		{
			T tmp;
			tmp = a;
			a = b;
			b = tmp;
		}

		public static GameObject ClickedGameObject()
		{
			if (Input.GetMouseButtonDown(0)) {
				Vector3 pos = Input.mousePosition;
				return GameObjectAt(new Vector2(pos.x, pos.y));
			}
			return null;
		}

		public static GameObject TappedGameObject()
		{
			if (Input.touchCount == 1) {
				Touch touch = Input.GetTouch(0);
				//make sure this is a tap, not a drag
				return touch.phase == TouchPhase.Began ? 
				GameObjectAt(Input.GetTouch(0).position) : null;
			}
			return null;
		}
	
		// Uses Main Camera
		public static GameObject GameObjectAt(Vector2 screenPos)
		{
			Ray r = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;
			if (Physics.Raycast(r, out hit, Mathf.Infinity)) {
				return hit.transform.gameObject;
			}
			return null;
		}

		// Useful is using a camera other than main
		public static GameObject GameObjectAt(Camera cam, Vector2 screenPos)
		{
			Ray r = cam.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;
			if (Physics.Raycast(r, out hit, Mathf.Infinity)) {
				return hit.transform.gameObject;
			}
			return null;
		}


		public static GameObject GameObjectAtDisplayCase(Vector2 screenPos)
		{
			Camera cam = null;
			foreach (Camera c in Camera.allCameras)
			{
				if (c.name == "Auxiliary Camera")
				{
					cam = c;
				}
			}
			if (cam != null) {
				return GameObjectAt (cam, new Vector2 (screenPos.x, screenPos.y));
			}
			return null;
		}

		// Displays error message window (relies on the error prefab being in the scene)
		public static void Alert(string message)
		{
			GameObject e = GameObject.FindGameObjectWithTag (Constants.ErrorTag);
			e.GetComponent<Error> ().SetMessage (message);
			e.transform.GetChild(0).gameObject.SetActive (true);
		}
	}
}