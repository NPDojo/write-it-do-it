﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.IO;
using System;

namespace WriteItDoIt {

	public class PlayModelPanel : MonoBehaviour {

		public Transform userModelButton;
		public Transform builtinModelButton;
		public Transform randomButton;
		public Transform userModelPanel;
		public Transform builtinModelPanel;
		public Transform randomPanel;

		private Transform activePanel;
		private string[] userModelPaths;

		public string SelectedText 
		{
			get
			{
				return activePanel.GetChild(0).GetComponent<Text>().text;
			}
			set
			{
				activePanel.GetChild (0).GetComponent<Text>().text = value;
			}
		}

		// Use this for initialization
		void Start () {
			userModelPaths = Directory.GetFiles(Application.persistentDataPath, "*.widi");

			GenerateUserModelDropDown();
			GenerateBuiltinModelDropDown();
		}
		
		// Update is called once per frame
		void Update () {
		}

		public void SelectRandomModel()
		{
			//We treat the usermodels + builtins as one array
			//We set stream from file or memory depending on whether
			//we get a valid index for the user models or not

			int userModelsCount = userModelPaths.Length;
			int totalModels = userModelsCount + Constants.builtinModels.Length;

			int index = new System.Random().Next(0, totalModels);

			Stream stream = null;
			string name = null;

			//If we're on the built-in side of the index range
			if (index >= userModelsCount) {
				int newindex = index - userModelsCount;
				TextAsset ta = (TextAsset) Constants.builtinModels[newindex];
				stream = new MemoryStream(ta.bytes);
				name = ta.name;
			//Otherwise, use user models
			} else {
				stream = new FileStream(userModelPaths[index], FileMode.Open);
				name = Path.GetFileNameWithoutExtension(userModelPaths[index]);
			}

			SetModelStream(stream, name);
			SelectedText = "Selected Model: Random";
		}

		public void SetModelStream(Stream s, string name)
		{
			resetCurrentModelStream ();
			GameManager.CurrentGame.LoadedModelStream = new ModelStream(s);
			GameManager.CurrentGame.ModelName = name;
		}

		public void panelButton(Transform button) {
			resetCurrentModelStream ();
			foreach (Transform t in button.parent) { // Grey out every button
				t.GetComponent<CanvasRenderer>().SetAlpha (0.5f);
			}
			button.GetComponent<CanvasRenderer> ().SetAlpha (1f); // Make this button solid again

			// Determine the active panel based on the button that was pressed
			if (button.Equals(userModelButton)) {
				activePanel = userModelPanel;
			} else if (button.Equals (builtinModelButton)) {
				activePanel = builtinModelPanel;
			} else if (button.Equals (randomButton)) {
				activePanel = randomPanel;
			}

			foreach (Transform t in activePanel.parent) { // Deactivate every panel
				t.gameObject.SetActive (false);
			}
			activePanel.gameObject.SetActive (true); // Make the selected panel active

			// Set the text in the active panel
			if (activePanel == userModelPanel && userModelPaths.Length == 0)
				SelectedText = "You don't have any models! You can save a new model in Sandbox mode";
			else if (activePanel == randomPanel)
				SelectRandomModel();
			else
				SelectedText = "Select a Model:";
		}

		private void resetCurrentModelStream() {
			if (GameManager.CurrentGame.LoadedModelStream != null) {
				GameManager.CurrentGame.LoadedModelStream.Close ();
				GameManager.CurrentGame.LoadedModelStream = null;
				GameManager.CurrentGame.ModelName = null;
			}
		}

		private int GetNumberOfPieces(string modelPath)
		{
			ModelStream ms = new ModelStream(new FileStream(modelPath, FileMode.Open));
			int count = ms.ReadPiecesCount();
			ms.Close();

			return count;
		}

		private void GenerateUserModelDropDown()
		{
			Utilities.DynamicDropdown(userModelPanel, userModelPaths,
                (object item) =>
					{
						string path = (string)item;
						return Path.GetFileNameWithoutExtension((string)item) + 
						" (" + GetNumberOfPieces(path) + "pc)";	
					},
					(object item) =>
					{
						return () =>
						{
							string name = Path.GetFileNameWithoutExtension ((string)item);
							SetModelStream(new FileStream ((string)item, FileMode.Open), name);
							SelectedText = "Selected Model: " + name;
						};
					}, true);
		}

		private void GenerateBuiltinModelDropDown()
		{
			Utilities.DynamicDropdown(builtinModelPanel, Constants.builtinModels,
	          (object item) =>
	          {
				TextAsset t = (TextAsset) item;
				ModelStream ms = new ModelStream(t.bytes);
				string result = t.name + " (" + ms.ReadPiecesCount() + "pc)";
				ms.Close ();
				return result;
			},
			(object item) =>
			{
				return () =>
				{
					TextAsset t = (TextAsset) item;
					resetCurrentModelStream ();
					GameManager.CurrentGame.LoadedModelStream = new ModelStream(t.bytes);
					GameManager.CurrentGame.ModelName = t.name;
					SelectedText = "Selected Model: " + t.name;
				};
			}, true);
		}
	}
}
